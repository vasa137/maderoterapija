let $selectableTree;
let categoryTree;
let changingNode;

let Category = function(text, tags, nodes) {
    this.text = text;
    this.tags = tags;

    if(nodes != null){
        this.nodes = nodes;
    }

    this.selectable = false;
    this.href = '/admin/kategorija/' + tags;

    this.state = {
        'expanded': true
    };
};


var initSelectableTree = function() {
    return $('#treeview-selectable').treeview({
        data: categoryTree,
        multiSelect: false,
        enableLinks: true
    });
};

// kopira dobijeno stablo kategorija u strukturu pogodnu za bootstrap treeview
function buildCategoryTree(categories){
    let branch = [];

    for(let i = 0; i < categories.length; i++){
        let category = categories[i];
        let children;

        if(category['children'] !== null && category['children'].length > 0){
            children  = buildCategoryTree(category['children']);
        } else{
            children = null;
        }

        let newCat = new Category(category['naziv'], category['id'], children);

        branch.push(newCat);
    }

    return branch;
}

// proizvodKategorije nije null kada se menja proizvod, a jeste kada se dodaje novi
function inicijalizujKategorije(stabloKategorija){
    stabloKategorija = JSON.parse(stabloKategorija);

    categoryTree = buildCategoryTree(stabloKategorija);

    $selectableTree = initSelectableTree();
}

