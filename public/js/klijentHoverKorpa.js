(function(){
        $("#cart-button").on("click", function () {
            // NE PRIKAZUJ POPUP NA MOBILNOM UREDJAJU ili AKO SMO VEC NA STRANICI KORPA
            if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)
                || window.location.href.indexOf("/korpa") > -1) {
                window.location.href = '/korpa';
            } else {
                $(".shopping-cart").fadeToggle("fast");
            }
        });

})();