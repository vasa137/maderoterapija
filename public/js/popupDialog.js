function otvoriDialog(){
    $('.cd-popup').addClass('is-visible');
}

function zatvoriDialog(){
    $('.cd-popup').removeClass('is-visible');
}

function otvoriDialogSaId(id){
    $('#dialog-' + id).addClass('is-visible');
}

function zatvoriDialogSaId(id){
    $('#dialog-' + id).removeClass('is-visible');
}