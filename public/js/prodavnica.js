let page = 1;
let kraj = 0;
$forma = $('#filterForma');
$formaSort = $('#sortirajForma');
$proizvodiInclude = $('#proizvodiInclude');

$(window).scroll(function() {
    if($(window).scrollTop() >= ($(document).height() - $(window).height())*0.7) {
        if(!kraj) {
            page++;
            loadMoreData(page);
        }
    }
});

function loadMoreData(page){
    $.ajax(
        {
            url: '?page=' + page + "&" + $forma.serialize() + "&" + $formaSort.serialize(),
            type: "get",

            beforeSend: function()
            {
                $('.ajax-load').show();
            }

        })
        .done(function(data)
        {
            $('.ajax-load').hide();

            if(data.html == ""){
                kraj = 1;
                //$('.ajax-load').html("Nema više proizvoda");
                return;
            }

            $proizvodiInclude.append(data.html);

        })
        .fail(function(jqXHR, ajaxOptions, thrownError)
        {
            console.log(jqXHR, ajaxOptions, thrownError);
        });
}

function filtriraj(){
    page = 1;
    kraj = 0;

    $("html, body").animate({ scrollTop: 200 }, "slow");

    $proizvodiInclude.empty();
    $('.ajax-load').show();

    $.get( "/prodavnica?" + $forma.serialize() + "&"  + $formaSort.serialize() + "&page="+ page , function( data ) {
        $proizvodiInclude.empty(); // OVDE DODAT EMPTY DA SE NE BI DUPLIRALI POZIVI I SAMO NADOVEZIVALI
        $proizvodiInclude.append(data.html);
        $('.ajax-load').hide();
    }).fail(function(jqXHR, ajaxOptions, thrownError)
    {
        $('.ajax-load').hide();
        alert('server nije dostupan...');
    });
}
