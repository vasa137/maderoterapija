let izmena = false;
let ogranicen;
let max = null, iskoriscen;

function showView(id){
    $('#' + id).css('display', 'block');
}

function hideView(id){
    $('#' +id).css('display','none');
}

function kategorijeChanged(zaSveKategorije){
    if(zaSveKategorije == 0){
        showView('kategorije-tree');
        showView('proizvodi-blok');
    } else{
        hideView('kategorije-tree');
        hideView('proizvodi-blok');
        document.getElementById('jos_proizvodaCB').checked = false;
    }
}

function josProizvodaChanged(josProizvodaCB){
    if(josProizvodaCB.checked){
        showView('proizvodi-kupon');
    } else {
        hideView('proizvodi-kupon');
    }
}

function ogranicenChanged(ogranicenCB){
    $max_koriscenja = $('#max-koriscenja');
    if(ogranicenCB.checked){
        $max_koriscenja.attr('readonly', false);
    } else {
        $('#max-koriscenja').attr('readonly', true);
    }

    $max_koriscenja.val(1);
}

function zaSveKupceChanged(zaSveKupce){
    if(zaSveKupce == 0){
        showView('kupci-kupon');
    } else {
        hideView('kupci-kupon');
    }
}

function postaviKorisnike(kupon){
    $('#korisnici').val(kupon.korisnici).change();
}

function postaviProizvode(kupon){
    $('#proizvodi').val(kupon.proizvodi).change();
}

function postaviPolja(kupon){
    kupon = JSON.parse(kupon);

    if(!kupon.za_sve_kategorije){
       showView('kategorije-tree');
       showView('proizvodi-blok');
    }

    if(!kupon.za_sve_korisnike){
        postaviKorisnike(kupon);
        showView('kupci-kupon');
    }

    if(!kupon.za_sve_kategorije && kupon.jos_proizvoda){
        postaviProizvode(kupon);
        showView('proizvodi-kupon');
        document.getElementById('jos_proizvodaCB').checked = true;
    }

    ogranicen = kupon.ogranicen;

    if(ogranicen){
        max = kupon.max_koriscenja;
    }

    iskoriscen = kupon.broj_koriscenja;
}

//mora ovde da se sakriju da bi se prvo iscrtali selectovi u potpunosti
function sakrijSelectove(){
    hideView('kupci-kupon');
    hideView('proizvodi-kupon');
}

// dodaj inpute za kategorije iz treeview-a
function kuponFormaSubmit(){
    let selectedNodes = $selectableTree.treeview('getSelected');

    $forma = $('#forma-kupon');

    for(let i = 0; i < selectedNodes.length; i++){
        $forma.append('<input type="hidden" name="kategorije[]" value="' +  selectedNodes[i].tags +'" />\n');
    }

    return true;
}