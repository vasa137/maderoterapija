function prikaziDostupne(){
    $('#tabela-specifikacije-obrisane').css('display', 'none');
    $('#tabela-specifikacije-aktivne').css('display', 'block');
    $('#tabela-specifikacije-obrisane_wrapper').css('display', 'none');
    $('#tabela-specifikacije-aktivne_wrapper').css('display', 'block');

    $('#specifikacije-title').html('Dostupne specifikacije');
}

function prikaziNedostupne(){
    $('#tabela-specifikacije-obrisane').css('display', 'block');
    $('#tabela-specifikacije-aktivne').css('display', 'none');
    $('#tabela-specifikacije-obrisane_wrapper').css('display', 'block');
    $('#tabela-specifikacije-aktivne_wrapper').css('display', 'none');

    $('#specifikacije-title').html('Obrisane specifikacije');
}