<?php

namespace App\Http\Controllers;

use App\Brend;
use App\Kategorija;
use App\Proizvod;
use App\ProizvodKategorija;
use App\ProizvodSpecifikacija;
use App\Specifikacija;
use App\Utility\Util;
use Illuminate\Http\Request;

define("PAGINACIJA", 12);

class klijentProizvodiController extends Controller
{
    private function popuniProizvodeNazivimaGlavnihSlika($proizvodi){
        foreach($proizvodi as $proizvod){
            $proizvod->nazivGlavneSlike = Util::getInstance()->nazivGlavneSlike($proizvod);
        }
    }

    public function prodavnica(Request $request){
        $kategorije = Kategorija::dohvatiAktivneKategorijeSortiranePoPrioritetu();
        $id = -1;
        if ($request->ajax()) {
            $queryKategorije = $request->query('kategorije');
            $queryBrendovi = $request->query('brendovi');
            $querySort = $request->query('sort');

            if($querySort == "null"){
                $querySort = null;
            }

            $kategorijeZaFilter = [];

            if($queryKategorije != null) {
                foreach ($queryKategorije as $nizKategorija) {
                    $kategorijeZaFilter[] = $nizKategorija;
                }
            }

            $brendoviZaFilter = [];

            if($queryBrendovi != null){
                $brendoviZaFilter = $queryBrendovi;
            }

            $sortBy = "created_at";
            $redosled = "desc";

            if($querySort != null){
                $explodedSort = explode('-', $querySort);

                $sortBy = $explodedSort[0];
                $redosled = $explodedSort[1];
            }

            $proizvodi = Proizvod::filtriraj($kategorijeZaFilter, $brendoviZaFilter, $sortBy, $redosled, PAGINACIJA);

            $this->popuniProizvodeNazivimaGlavnihSlika($proizvodi);

            $view = view('include.listaProizvoda',compact('proizvodi'))->render();

            return response()->json(['html'=>$view]);
        } else {
            $proizvodi = Proizvod::filtriraj([], [], 'created_at', 'desc',PAGINACIJA);

            $stabloKategorija = Util::getInstance()->buildCategoryTree($kategorije, null);

            $brendovi = Brend::dohvatiSveAktivne();

            $this->popuniProizvodeNazivimaGlavnihSlika($proizvodi);

            return view('prodavnica', compact('stabloKategorija', 'brendovi', 'proizvodi','id'));
        }
    }

     public function kategorijaProdavnica(Request $request ,$id){
        $kategorije = Kategorija::dohvatiAktivneKategorijeSortiranePoPrioritetu();

        if ($request->ajax()) {
            $queryKategorije = $request->query('kategorije');
            $queryBrendovi = $request->query('brendovi');
            $querySort = $request->query('sort');

            if($querySort == "null"){
                $querySort = null;
            }

            $kategorijeZaFilter = [[$id]];

            if($queryKategorije != null) {
                $kategorijeZaFilter = [];
                foreach ($queryKategorije as $nizKategorija) {
                    $kategorijeZaFilter[] = $nizKategorija;
                }
            }


            $brendoviZaFilter = [];

            if($queryBrendovi != null){
                $brendoviZaFilter = $queryBrendovi;
            }

            $sortBy = "created_at";
            $redosled = "desc";

            if($querySort != null){
                $explodedSort = explode('-', $querySort);

                $sortBy = $explodedSort[0];
                $redosled = $explodedSort[1];
            }

            $proizvodi = Proizvod::filtriraj($kategorijeZaFilter, $brendoviZaFilter, $sortBy, $redosled, PAGINACIJA);

            $this->popuniProizvodeNazivimaGlavnihSlika($proizvodi);

            $view = view('include.listaProizvoda',compact('proizvodi'))->render();

            return response()->json(['html'=>$view]);
        } else {
            $proizvodi = Proizvod::filtriraj([[$id]], [], 'created_at', 'desc',PAGINACIJA);

            $stabloKategorija = Util::getInstance()->buildCategoryTree($kategorije, null);

            $brendovi = Brend::dohvatiSveAktivne();

            $this->popuniProizvodeNazivimaGlavnihSlika($proizvodi);

            return view('prodavnica', compact('stabloKategorija', 'brendovi', 'proizvodi','id'));
        }
    }

    public function proizvod($link, $id){
        $proizvod = Proizvod::dohvatiSaId($id);

        if($proizvod == null || $proizvod->sakriven){
            abort(404);
        }

        $proizvodKategorije = ProizvodKategorija::dohvatiKategorijeZaProizvod($id);

        $kategorije = [];

        foreach($proizvodKategorije as $proizvodKategorija){
            $kategorije [] = Kategorija::dohvatiSaId($proizvodKategorija->id_kategorija);
        }

        $proizvod->kategorije = $kategorije;

        if($proizvod->id_brend != null){
            $proizvod->brend = Brend::dohvatiSaId($proizvod->id_brend);
        }

        if($proizvod->ima_specifikacije){
            $proizvodNizSpecifikacija = [];
            $proizvodNizSpecifikacijaTekst = [];
            $proizvodSpecifikacije = ProizvodSpecifikacija::dohvatiSpecifikacijeZaProizvod($proizvod->id);

            foreach ($proizvodSpecifikacije as $proizvodSpecifikacija) {
                $proizvodNizSpecifikacija[] = Specifikacija::dohvatiSaId($proizvodSpecifikacija->id_specifikacija);
                $proizvodNizSpecifikacijaTekst[] = $proizvodSpecifikacija->tekst;
            }

            $proizvod->specifikacije = $proizvodNizSpecifikacija;
            $proizvod->specifikacije_tekst = $proizvodNizSpecifikacijaTekst;
        }

        $proizvodDirectory =  public_path('images/proizvodi/' . $proizvod->id);

        $proizvod->nazivGlavneSlike = Util::getInstance()->nazivGlavneSlike($proizvod);

        $proizvod->sveSlike = Util::getInstance()->pokupiNizFajlova($proizvodDirectory . '/sveSlike');

        return view('proizvod', compact('proizvod'));
    }
}
