<?php

namespace App\Http\Controllers;

use App\Brend;
use App\Proizvod;
use Illuminate\Http\Request;
use File;
use Redirect;
class adminBrendoviController extends Controller
{


    private function obrisi_temp(){
        $directoryPath = public_path('images/brendovi/temp');
        File::deleteDirectory($directoryPath);

        File::makeDirectory($directoryPath,0755,true);
    }

    private function popuniBrendInfo($brend){
        $brend->broj_proizvoda = Proizvod::dohvatiBrojRazlicitihProizvodaZaBrend($brend->id);
        $brend->broj_kategorija = Proizvod::dohvatiBrojRazlicitihKategorijaZaBrend($brend->id);
    }

   public function brend($id){
        $this->obrisi_temp();

        $izmena = false;

        if($id > 0){
            $izmena = true;
        }

        if(!$izmena){
            return view('admin.adminBrend', compact('izmena'));
        } else{
            $brend = Brend::dohvatiSaId($id);

            if($brend == null){
                abort(404);
            }

            $this->popuniBrendInfo($brend);

            $brend_directory = public_path('images/brendovi/' . $brend->id);
            $temp_directory = public_path('images/brendovi/temp');

            if(!File::exists($brend_directory)){
                File::makeDirectory($brend_directory);
            }

            File::copyDirectory($brend_directory, $temp_directory);

            // ZA ADMINA SLIKU VRACAMO KAO GLAVNA.JPG
            if(File::exists($temp_directory . '/' . $brend->naziv . '.jpg')) {
                File::move($temp_directory . '/' . $brend->naziv . '.jpg', $temp_directory . '/glavna.jpg');
            }

            return view('admin.adminBrend', compact('izmena', 'brend'));
        }
   }

   public function brendovi(){
        $aktivniBrendovi = Brend::dohvatiSveAktivne();
        $obrisaniBrendovi = Brend::dohvatiSveObrisane();

        foreach($aktivniBrendovi as $brend){
            $this->popuniBrendInfo($brend);
        }

        foreach($obrisaniBrendovi as $brend){
            $this->popuniBrendInfo($brend);
        }

        return view('admin.adminBrendovi', compact('aktivniBrendovi', 'obrisaniBrendovi'));
   }

   public function sacuvaj_brend($id){
        $izmena = false;

        if($id > 0){
            $izmena = true;
        }

        $naziv = $_POST['naziv'];
        $opis = $_POST['opis'];

        $zaPunjenje = true;

        if($izmena){
            $brend = Brend::dohvatiSaId($id);

            if($brend->naziv == $naziv && $brend->opis == $opis){
               $zaPunjenje = false;
            }

        } else{
            $brend = new Brend();
        }

        if($zaPunjenje) {
            $brend->napuni($naziv, $opis);
        }

        $brend_directory = public_path('images/brendovi/' . $brend->id);
        $temp_directory = public_path('images/brendovi/temp');


        if(File::exists($brend_directory)){
            File::deleteDirectory($brend_directory);
        }

        File::makeDirectory($brend_directory,0755,true);

        File::copyDirectory($temp_directory, $brend_directory);

       // PROMENA NAZIVA GLAVNE SLIKE ZBOG OPTIMIZACIJE
       if(File::exists($brend_directory . '/glavna.jpg')) {
           File::move($brend_directory . '/glavna.jpg', $brend_directory . '/' . $brend->naziv . '.jpg');
       }

       return redirect('/admin/brend/' . $brend->id);
   }

   public function obrisi_brend($id){
        $brend = Brend::dohvatiSaId($id);

        $brend->obrisi();

        return Redirect::back();
   }

   public function restauriraj_brend($id){
       $brend = Brend::dohvatiSaId($id);

       $brend->restauriraj();

       return Redirect::back();
   }

   public function upload_slike(){
       $image = $_FILES['file'];

       $directoryPath = public_path('images/brendovi/temp');
       $image_name = 'glavna.jpg';

       //$filename = time() . '.' . pathinfo('' . $image['name'][0], PATHINFO_EXTENSION);

       File::move($image['tmp_name'][0], $directoryPath . '/' . $image_name);

       chmod($directoryPath . '/' .$image_name, 0644);
   }

   public function obrisi_upload_slike(){
       $image_name = $_POST['filename'];

       $directoryPath = public_path('images/brendovi/temp');

       File::delete($directoryPath . '/' . $image_name);
   }
}
