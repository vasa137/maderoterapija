<?php

namespace App\Http\Controllers;

use App\GrupaOpcija;
use App\Opcija;
use App\ProizvodOpcija;
use Illuminate\Http\Request;
use Redirect;
class adminOpcijeController extends Controller
{
    private function popuniOpcijaInfo($opcija){
        $opcija->broj_proizvoda = ProizvodOpcija::dohvatiBrojProizvodaZaOpciju($opcija->id);
    }

    public function opcija($id){
        $izmena = false;

        if($id > 0){
            $izmena = true;
        }

        $grupeOpcija = GrupaOpcija::dohvatiSve();

        if(!$izmena){
            return view('admin.adminOpcija', compact('izmena', 'grupeOpcija'));
        } else{
            $opcija = Opcija::dohvatiSaId($id);

            if($opcija == null){
                abort(404);
            }

            $this->popuniOpcijaInfo($opcija);

            return view('admin.adminOpcija', compact('izmena', 'opcija', 'grupeOpcija'));
        }
    }

    public function opcije(){
        $opcije = Opcija::dohvatiSve();

        $grupeOpcija = GrupaOpcija::dohvatiSve();

        $opcijePoGrupama = [];

        $brojAktivnihOpcija = 0;

        foreach($grupeOpcija as $grupaOpcija){
           $opcijePoGrupama[$grupaOpcija->id] = [];
        }

        foreach($opcije as $opcija){
            $this->popuniOpcijaInfo($opcija);
            $opcijePoGrupama[$opcija->id_grupa_opcija][] = $opcija;

            if(!$opcija->sakriven){
                $brojAktivnihOpcija++;
            }
        }

        foreach($grupeOpcija as $grupaOpcija){
            $grupaOpcija->opcije = $opcijePoGrupama[$grupaOpcija->id];
        }

        return view('admin.adminOpcije', compact('opcije', 'grupeOpcija', 'brojAktivnihOpcija'));
    }

    public function sacuvaj_opciju($id){
        $izmena = false;

        if($id > 0){
            $izmena = true;
        }

        $naziv = $_POST['naziv'];
        $opis = $_POST['opis'];
        $id_grupa_opcija = $_POST['grupaOpcija'];

        $zaPunjenje = true;

        if($izmena){
            $opcija = Opcija::dohvatiSaId($id);

            if($opcija->naziv == $naziv && $opcija->opis == $opis && $opcija->id_grupa_opcija == $id_grupa_opcija){
                $zaPunjenje = false;
            }

        } else{
            $opcija = new Opcija();
        }

        if($zaPunjenje) {
            $opcija->napuni($naziv, $opis, $id_grupa_opcija);
        }

        return redirect('/admin/opcija/' . $opcija->id);
    }

    public function obrisi_opciju($id){
        $opcija = Opcija::dohvatiSaId($id);

        $opcija->obrisi();

        return Redirect::back();
    }

    public function restauriraj_opciju($id){
        $opcija = Opcija::dohvatiSaId($id);

        $opcija->restauriraj();

        return Redirect::back();
    }
}
