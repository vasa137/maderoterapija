<?php

namespace App;

use Image;
use Laravel\Socialite\Contracts\Provider as Provider;
use Socialite;

// ubaci ovo
class SocialAccountService
{
    public function createOrGetUser(Provider $provider) // ovo zameni
    {
        $providerName = class_basename($provider);
        //$providerUser = $provider->user(); // i ove dve linije stavi

        // PUNO IME PROVAJDERA PROMENJENO KAKO BI driver funkcija radila, ona zahteva google ili facebook za parametre
        if ($providerName == 'GoogleProvider') {
            $providerDriverName = 'google';
        } else if ($providerName == 'FacebookProvider') {
            $providerDriverName = 'facebook';
        }

        //DA SE IZBEGNE VERIFIKACIJA cURL 60 SERTIFIKATA, ako se ne stavi ovo stateless nece raditi
        $providerUser = \Socialite::driver($providerDriverName)
            ->setHttpClient(new \GuzzleHttp\Client(['verify' => false]))
            ->stateless()->user();

        $account = SocialAccount::whereProvider($providerName)
            ->whereProviderUserId($providerUser->getId())
            ->first();

        if($account){
            $user = User::dohvatiSaId($account->user_id);
            return $user;
        } else {
            $account = new SocialAccount([
                'provider_user_id' => $providerUser->getId(),
                'provider' => $providerName
            ]);

            $user = User::dohvatiSaEmailom($providerUser->getEmail());

            if (!$user) {
                $ime_prezime = $providerUser->getName();
                $user = User::create([
                    'email' => $providerUser->getEmail(),
                    'ime_prezime' => $ime_prezime
                ]);
                $user->save();
            }

            $account->user()->associate($user);
            $account->save();

            return $user;
        }

    }
}
