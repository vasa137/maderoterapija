<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class ProizvodTag extends Model
{
    protected $table = 'proizvod_tag';

    protected $fillable = ['id_proizvod', 'id_tag'];

    public function napuni($id_proizvod, $id_tag){
        $this->id_proizvod = $id_proizvod;
        $this->id_tag = $id_tag;

        $this->save();
    }

    public static function dohvatiTagoveZaProizvod($id){
        return ProizvodTag::where('id_proizvod', $id)->get();
    }

    public static function obrisiTagoveZaProizvod($id){
        ProizvodTag::where('id_proizvod', $id)->delete();
    }

    public static function dohvatiBrojProizvodaZaTag($id){
        return DB::select("
            select IFNULL(COUNT(p.id), 0) as broj_proizvoda
            FROM proizvod_tag pt, proizvod p
            WHERE pt.id_tag = $id
            AND pt.id_proizvod = p.id
            AND p.sakriven = 0
        ")[0]->broj_proizvoda;
    }

}
