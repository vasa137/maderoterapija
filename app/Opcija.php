<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Opcija extends Model
{
    protected $table = 'opcija';

    protected $fillable = ['naziv', 'opis', 'id_grupa_opcija', 'sakriven'];

    protected $appends = ['broj_proizvoda'];

    private $broj_proizvoda;

    public function setBrojProizvodaAttribute($broj_proizvoda){
        $this->broj_proizvoda = $broj_proizvoda;
    }

    public function getBrojProizvodaAttribute(){
        return $this->broj_proizvoda;
    }

    public static function dohvatiSveAktivne(){
        return Opcija::where('sakriven', 0)->get();
    }

    public static function dohvatiSveObrisane(){
        return Opcija::where('sakriven', 1)->get();
    }

    public static function dohvatiSve(){
        return Opcija::all();
    }

    public static function dohvatiSaId($id){
        return Opcija::where('id', $id)->first();
    }


    public function napuni($naziv, $opis, $id_grupa_opcija){
        $this->naziv = $naziv;
        $this->opis = $opis;
        $this->id_grupa_opcija = $id_grupa_opcija;

        $this->save();
    }

    public function obrisi(){
        $this->sakriven = 1;

        $this->save();
    }

    public function restauriraj(){
        $this->sakriven = 0;

        $this->save();
    }
}
