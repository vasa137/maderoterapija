@extends('layout')

@section('scriptsTop')
    <link rel="stylesheet" href="{{asset('css/sliderPocetna.css')}}"/>
@endsection

@section('scriptsBottom')
    <script src="{{asset('js/jssor.slider-27.5.0.min.js')}}"></script>
    <script src="{{asset('js/sliderPocetna.js')}}"></script>
    <script>jssor_1_slider_init();</script>
@endsection

@section('sekcije')

    <div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:980px;height:440px;overflow:hidden;visibility:hidden;">
        <!-- Loading Screen -->
        <div data-u="loading" class="jssorl-004-double-tail-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
            <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="img/double-tail-spin.svg" />
        </div>
        <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:980px;height:500px;overflow:hidden;">
            <div>
                <!-- #endregion Jssor Slider End -->
                <section id="banner" class="banner" style=" background: url(/img/maderoterapija-bb-glow-salon-slider/ampule-za-bb-glow-prodaja-maderoterapija-bb-glow-1.jpg) no-repeat;">
                    <div class="bannerOverlay"></div>
                    <div class="container" style="position:absolute;">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="content">
                                    <p class="tagline">
                                        Dobrodošli na sajt kompanije
                                    </p>
                                    <h1>
                                        Essence Of Beauty
                                    </h1>
                                    <div class="links">
                                        <a class="link1" href="/prodavnica">
                                            Prodavnica
                                        </a>
                                        <a class="link2" href="/edukacija">
                                            Edukacija
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </section>
            </div>

            <div>
                <section id="banner" class="banner" style="background: url(/img/maderoterapija-bb-glow-salon-slider/ampule-za-bb-glow-prodaja-maderoterapija-bb-glow-2.jpg) no-repeat;">
                    <div class="bannerOverlay"></div>
                    <div class="container" style="position:absolute;">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="content">
                                    <p class="tagline">
                                       Dobrodošli na sajt kompanije
                                    </p>
                                    <h1>
                                        Essence Of Beauty
                                    </h1>
                                    <div class="links">
                                        <a class="link1" href="/prodavnica">
                                            Prodavnica
                                        </a>
                                        <a class="link2" href="/edukacija">
                                            Edukacija
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div>
                <section id="banner" class="banner" style="background: url(/img/maderoterapija-bb-glow-salon-slider/ampule-za-bb-glow-prodaja-maderoterapija-bb-glow-3.jpg) no-repeat;">
                    <div class="bannerOverlay"></div>
                    <div class="container" style="position:absolute;">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="content">
                                    <p class="tagline">
                                        Dobrodošli na sajt kompanije
                                    </p>
                                    <h1>
                                        Essence Of Beauty
                                    </h1>
                                    <div class="links">
                                        <a class="link1" href="/prodavnica">
                                            Prodavnica
                                        </a>
                                        <a class="link2" href="/edukacija">
                                            Edukacija
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- Bullet Navigator -->
        <div data-u="navigator" class="jssorb031" style="position:absolute;bottom:12px;right:12px;" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
            <div data-u="prototype" class="i" style="width:16px;height:16px;">
                <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                    <circle class="b" cx="8000" cy="8000" r="5800"></circle>
                </svg>
            </div>
        </div>
        <!-- Arrow Navigator -->
        <div data-u="arrowleft" class="jssora051" style="width:55px;height:55px;top:0px;left:25px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
            <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
            </svg>
        </div>
        <div data-u="arrowright" class="jssora051" style="width:55px;height:55px;top:0px;right:25px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
            <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
            </svg>
        </div>
    </div>


<!-- 
<section id="services" class="services">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-lg-3">
                <div class="sBox">
                    <div class="img">
                        <img class="align-self-center" src="img/icon/icon1.png" alt="">
                    </div>
                    <h3>
                        BESPLATNA DOSTAVA
                    </h3>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="sBox box2">
                    <div class="img">
                        <img class="align-self-center" src="img/icon/icon2.png" alt="">
                    </div>
                    <h3>
                        EDUKACIJA
                    </h3>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="sBox">
                    <div class="img">
                        <img class="align-self-center" src="img/icon/icon3.png" alt="">
                    </div>
                    <h3>
                        INFO 3
                    </h3>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="sBox box2">
                    <div class="img">
                        <img class="align-self-center" src="img/icon/icon4.png" alt="">
                    </div>
                    <h3>
                        INFO 4
                    </h3>
                </div>
            </div>
        </div>
    </div>
</section>
 -->

<section id="giftCard" class="giftCard">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10 col-lg-9">
                <div class="sectionTheading">
                    <h2>
                        Izdvajamo iz ponude
                        <br><br>
                    </h2>
                   
                </div>
            </div>
        </div>
        
        <div class="row">

            <div class="col-md-6 col-lg-4">
                <div class="mycard">
                    <a href="http://essenceofbeauty.rs/prodavnica">
                    <div class="cardImage">
                        <img class="img-fluid" src="img/ws/1.jpg" alt="">
                    </div>
                    
                    <div class="cardContent">
                         <h3>
                             
                        </h3>
                    </div>
                
                    </a>
                </div>
            </div>

            <div class="col-md-6 col-lg-4">
                <div class="mycard">
                    <a href="http://essenceofbeauty.rs/prodavnica">
                    <div class="cardImage">
                        <img class="img-fluid" src="img/ws/2.jpg" alt="">
                    </div>
                    
                    <div class="cardContent">
                         <h3>
                             
                        </h3>
                    </div>
                
                    </a>
                </div>
            </div>

            <div class="col-md-6 col-lg-4">
                <div class="mycard">
                    <a href="http://essenceofbeauty.rs/prodavnica">
                    <div class="cardImage">
                        <img class="img-fluid" src="img/ws/3.jpg" alt="">
                    </div>
                    
                    <div class="cardContent">
                         <h3>
                             
                        </h3>
                    </div>
                
                    </a>
                </div>
            </div>

            <div class="col-md-6 col-lg-4">
                <div class="mycard">
                    <a href="http://essenceofbeauty.rs/prodavnica">
                    <div class="cardImage">
                        <img class="img-fluid" src="img/ws/4.jpg" alt="">
                    </div>
                    
                    <div class="cardContent">
                         <h3>
                             
                        </h3>
                    </div>
                
                    </a>
                </div>
            </div>

            <div class="col-md-6 col-lg-4">
                <div class="mycard">
                    <a href="http://essenceofbeauty.rs/prodavnica">
                    <div class="cardImage">
                        <img class="img-fluid" src="img/ws/5.jpg" alt="">
                    </div>
                    
                    <div class="cardContent">
                         <h3>
                             
                        </h3>
                    </div>
                
                    </a>
                </div>
            </div>

            <div class="col-md-6 col-lg-4">
                <div class="mycard">
                    <a href="http://essenceofbeauty.rs/prodavnica">
                    <div class="cardImage">
                        <img class="img-fluid" src="img/ws/6.jpg" alt="">
                    </div>
                    
                    <div class="cardContent">
                         <h3>
                             
                        </h3>
                    </div>
                
                    </a>
                </div>
            </div>

            <div class="col-md-6 col-lg-4">
                <div class="mycard">
                    <a href="http://essenceofbeauty.rs/prodavnica">
                    <div class="cardImage">
                        <img class="img-fluid" src="img/ws/7.jpg" alt="">
                    </div>
                    
                    <div class="cardContent">
                         <h3>
                             
                        </h3>
                    </div>
                
                    </a>
                </div>
            </div>

            <div class="col-md-6 col-lg-4">
                <div class="mycard">
                    <a href="http://essenceofbeauty.rs/prodavnica">
                    <div class="cardImage">
                        <img class="img-fluid" src="img/ws/8.jpg" alt="">
                    </div>
                    
                    <div class="cardContent">
                         <h3>
                             
                        </h3>
                    </div>
                
                    </a>
                </div>
            </div>

            <div class="col-md-6 col-lg-4">
                <div class="mycard">
                    <a href="http://essenceofbeauty.rs/prodavnica">
                    <div class="cardImage">
                        <img class="img-fluid" src="img/ws/9.jpg" alt="">
                    </div>
                    
                    <div class="cardContent">
                         <h3>
                             
                        </h3>
                    </div>
                
                    </a>
                </div>
            </div>

           
            
            
        </div>
    </div>
</section>

<!-- Gifts & Cards Section End -->


<!-- News Feeds Area CSS start -->
<section id="news" class="news">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10 col-lg-9">
                <div class="sectionTheading">
                    <h2>
                        Edukacije
                    </h2>
                    <img src="img/sectionSeparatorw.png" alt="">
                    <p>
                        Naredni termini naših najtraženijih edukacija su:
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
            <!--
            

		
            <div class="col-md-6 col-lg-4">
                <div class="blog-box">
                    <div class="topImg">
                        <a href="/edukacija/plazma-pen">
                            <img class="img-fluid" src="img/edukacije-maderoterapija-tela-lica-bb-glow-srbija/plazma-pen.jpg" alt="">
                        </a>
                    </div>
                    <div class="text">
                        <a href="/edukacija/plazma-pen">
                            <h3>
                               Plazma pen<br>25.1.2020.
                            </h3>
                        </a>
                    </div>
                </div>
            </div>

    
        
            
            <div class="col-md-6 col-lg-4">
                <div class="blog-box">
                    <div class="topImg">
                        <a href="/edukacija/plazma-pen">
                            <img class="img-fluid" src="img/edukacije-maderoterapija-tela-lica-bb-glow-srbija/plazma-pen.jpg" alt="">
                        </a>
                    </div>
                    <div class="text">
                        <a href="/edukacija/plazma-pen">
                            <h3>
                               Plazma pen<br>22.2.2020.
                            </h3>
                        </a>
                    </div>
                </div>
            </div>


           
            -->
             <div class="col-md-6 col-lg-4">
                <div class="blog-box">
                    <div class="topImg">
                        <a href="/edukacija/maderoterapija">
                            <img class="img-fluid" src="img/edukacije-maderoterapija-tela-lica-bb-glow-srbija/maderoterapija-tela-obuka-naslovna.jpg" alt="">
                        </a>
                    </div>
                    <div class="text">
                        <a href="/edukacija/maderoterapija">
                            <h3>
                               Maderoterapija tela <br><a href="/kontakt">PRIJAVE NA 062 455 200</a>
                            </h3>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-4">
                <div class="blog-box">
                    <div class="topImg">
                        <a href="/edukacija/maderoterapija-lica">
                            <img class="img-fluid" src="img/edukacije-maderoterapija-tela-lica-bb-glow-srbija/maderoterapija-lica-kurs-naslovna.jpg" alt="">
                        </a>
                    </div>
                    <div class="text">
                        <a href="/edukacija/maderoterapija-lica">
                            <h3>
                               Maderoterapija lica <br><a href="/kontakt">PRIJAVE NA 062 455 200</a>
                            </h3>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-4">
                <div class="blog-box">
                    <div class="topImg">
                        <a href="/edukacija/depilacija">
                            <img class="img-fluid" src="img/edukacije-maderoterapija-tela-lica-bb-glow-srbija/depilacija-naslovna.jpg" alt="">
                        </a>
                    </div>
                    <div class="text">
                        <a href="/edukacija/depilacija">
                            <h3>
                               Depilacija<br><a href="/kontakt">PRIJAVE NA 062 455 200</a>
                            </h3>
                        </a>
                    </div>
                </div>
            </div>
             <div class="col-md-6 col-lg-4">
                <div class="blog-box">
                    <div class="topImg">
                        <a href="/edukacija/relaks-masaza">
                            <img class="img-fluid" src="img/edukacije-maderoterapija-tela-lica-bb-glow-srbija/relaks-masaza-naslovna.jpg" alt="">
                        </a>
                    </div>
                    <div class="text">
                        <a href="/edukacija/relaks-masaza">
                            <h3>
                               Relaks masaža <br><a href="/kontakt">PRIJAVE NA 062 455 200</a>
                            </h3>
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-lg-4">
                <div class="blog-box">
                    <div class="topImg">
                        <a href="/edukacija/anticelulit-masaza">
                            <img class="img-fluid" src="img/edukacije-maderoterapija-tela-lica-bb-glow-srbija/anticelulit-masaza-naslovna.jpg" alt="">
                        </a>
                    </div>
                    <div class="text">
                        <a href="/edukacija/anticelulit-masaza">
                            <h3>
                               Anticelulit masaža <br><a href="/kontakt">PRIJAVE NA 062 455 200</a>
                            </h3>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-4">
                <div class="blog-box">
                    <div class="topImg">
                        <a href="/edukacija/maderoterapija">
                            <img class="img-fluid" src="img/edukacije-maderoterapija-tela-lica-bb-glow-srbija/maderoterapija-tela-obuka-naslovna.jpg" alt="">
                        </a>
                    </div>
                    <div class="text">
                        <a href="/edukacija/maderoterapija">
                            <h3>
                               Maderoterapija tela <br><a href="/kontakt">PRIJAVE NA 062 455 200</a>
                            </h3>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-4">
                <div class="blog-box">
                    <div class="topImg">
                        <a href="/edukacija/maderoterapija-lica">
                            <img class="img-fluid" src="img/edukacije-maderoterapija-tela-lica-bb-glow-srbija/maderoterapija-lica-kurs-naslovna.jpg" alt="">
                        </a>
                    </div>
                    <div class="text">
                        <a href="/edukacija/maderoterapija-lica">
                            <h3>
                               Maderoterapija lica <br><a href="/kontakt">PRIJAVE NA 062 455 200</a>
                            </h3>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-4">
                <div class="blog-box">
                    <div class="topImg">
                        <a href="/edukacija/refleksoloska-masaza">
                            <img class="img-fluid" src="img/edukacije-maderoterapija-tela-lica-bb-glow-srbija/refleksoloska-masaza.jpg" alt="">
                        </a>
                    </div>
                    <div class="text">
                        <a href="/edukacija/refleksoloska-masaza">
                            <h3>
                               Refleksološka masaža<br><a href="/kontakt">PRIJAVE NA 062 455 200</a>
                            </h3>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-4">
                <div class="blog-box">
                    <div class="topImg">
                        <a href="/edukacija/masaza-svecom">
                            <img class="img-fluid" src="img/edukacije-maderoterapija-tela-lica-bb-glow-srbija/masaza-svecom.jpg" alt="">
                        </a>
                    </div>
                    <div class="text">
                        <a href="/edukacija/masaza-svecom">
                            <h3>
                               Masaža svećom<br><a href="/kontakt">PRIJAVE NA 062 455 200</a>
                            </h3>
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-lg-4">
                <div class="blog-box">
                    <div class="topImg">
                        <a href="/edukacija/svedska-masaza">
                            <img class="img-fluid" src="img/edukacije-maderoterapija-tela-lica-bb-glow-srbija/svedska-masaza.jpg" alt="">
                        </a>
                    </div>
                    <div class="text">
                        <a href="/edukacija/svedska-masaza">
                            <h3>
                               Švedska masaža<br><a href="/kontakt">PRIJAVE NA 062 455 200</a>
                            </h3>
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-lg-4">
                <div class="blog-box">
                    <div class="topImg">
                        <a href="/edukacija/hijaluron-pen">
                            <img class="img-fluid" src="img/edukacije-maderoterapija-tela-lica-bb-glow-srbija/hijaluron-pen2.jpg" alt="">
                        </a>
                    </div>
                    <div class="text">
                        <a href="/edukacija/hijaluron-pen">
                            <h3>
                               Hijaluron pen<br><a href="/kontakt">PRIJAVE NA 062 455 200</a>
                            </h3>
                        </a>
                    </div>
                </div>
            </div>


            <div class="col-md-6 col-lg-4">
                <div class="blog-box">
                    <div class="topImg">
                        <a href="/edukacija/bb-glow">
                            <img class="img-fluid" src="img/edukacije-maderoterapija-tela-lica-bb-glow-srbija/bb-glow-srbija-edukacija-naslovna.jpg" alt="">
                        </a>
                    </div>
                    <div class="text">
                        <a href="/edukacija/bb-glow">
                            <h3>
                               BB Glow + Dermapen<br><a href="/kontakt">PRIJAVE NA 062 455 200</a>
                            </h3>
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-lg-4">
                <div class="blog-box">
                    <div class="topImg">
                        <a href="/edukacija/depilacija">
                            <img class="img-fluid" src="img/edukacije-maderoterapija-tela-lica-bb-glow-srbija/depilacija-naslovna.jpg" alt="">
                        </a>
                    </div>
                    <div class="text">
                        <a href="/edukacija/depilacija">
                            <h3>
                               Depilacija<br><a href="/kontakt">PRIJAVE NA 062 455 200</a>
                            </h3>
                        </a>
                    </div>
                </div>
            </div>

             <div class="col-md-6 col-lg-4">
                <div class="blog-box">
                    <div class="topImg">
                        <a href="/edukacija/maderoterapija">
                            <img class="img-fluid" src="img/edukacije-maderoterapija-tela-lica-bb-glow-srbija/maderoterapija-tela-obuka-naslovna.jpg" alt="">
                        </a>
                    </div>
                    <div class="text">
                        <a href="/edukacija/maderoterapija">
                            <h3>
                               Maderoterapija tela <br><a href="/kontakt">PRIJAVE NA 062 455 200</a>
                            </h3>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-4">
                <div class="blog-box">
                    <div class="topImg">
                        <a href="/edukacija/maderoterapija-lica">
                            <img class="img-fluid" src="img/edukacije-maderoterapija-tela-lica-bb-glow-srbija/maderoterapija-lica-kurs-naslovna.jpg" alt="">
                        </a>
                    </div>
                    <div class="text">
                        <a href="/edukacija/maderoterapija-lica">
                            <h3>
                               Maderoterapija lica <br><a href="/kontakt">PRIJAVE NA 062 455 200</a>
                            </h3>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-4">
                <div class="blog-box">
                    <div class="topImg">
                        <a href="/edukacija/depilacija">
                            <img class="img-fluid" src="img/edukacije-maderoterapija-tela-lica-bb-glow-srbija/depilacija-naslovna.jpg" alt="">
                        </a>
                    </div>
                    <div class="text">
                        <a href="/edukacija/depilacija">
                            <h3>
                               Depilacija<br><a href="/kontakt">PRIJAVE NA 062 455 200</a>
                            </h3>
                        </a>
                    </div>
                </div>
            </div>
            

            <div class="col-md-6 col-lg-4">
                <div class="blog-box">
                    <div class="topImg">
                        <a href="/edukacija/hijaluron-pen">
                            <img class="img-fluid" src="img/edukacije-maderoterapija-tela-lica-bb-glow-srbija/hijaluron-pen2.jpg" alt="">
                        </a>
                    </div>
                    <div class="text">
                        <a href="/edukacija/hijaluron-pen">
                            <h3>
                               Hijaluron pen<br><a href="/kontakt">PRIJAVE NA 062 455 200</a>
                            </h3>
                        </a>
                    </div>
                </div>
            </div>


            <div class="col-md-6 col-lg-4">
                <div class="blog-box">
                    <div class="topImg">
                        <a href="/edukacija/bb-glow">
                            <img class="img-fluid" src="img/edukacije-maderoterapija-tela-lica-bb-glow-srbija/bb-glow-srbija-edukacija-naslovna.jpg" alt="">
                        </a>
                    </div>
                    <div class="text">
                        <a href="/edukacija/bb-glow">
                            <h3>
                               BB Glow + Dermapen<br><a href="/kontakt">PRIJAVE NA 062 455 200</a>
                            </h3>
                        </a>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<!-- News Feeds Area CSS End -->

<!-- Video Section Start 
<section id="video" class="video">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="vImg">
                    <img class="img-fluid" src="img/videoSection.jpg" alt="">
                </div>
            </div>
            <div class="col-lg-6">
                <h2>
                    Pzicija za video
                </h2>
                <p>
                    Ovde bi bio video na kome se može prikazati obuka ili neka od usluga. Takođe možemo napraviti doodly video po potrebi gde mozemo raditi npr sta se dobija nekom od obuka sa štikliranim pozicijama...
                </p>
                <div class="videoContent">
                    <div class="icon">
                        <a class="venobox" data-vbtype="video" data-autoplay="true" href="http://vimeo.com/75976293">
                            <i class="fas fa-play"></i>
                        </a>
                    </div>
                    <div class="content d-flex">
                        <div class="text align-self-center">
                            <h5>obuka - Maderoterapija tela</h5>
                                <p>Pogledajte video</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
Video Section End -->


<!-- Appoinment Section Start -->
<section id="appoinmentSection" class="appoinmentSection">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="">
                    <div style="text-align: center; color: white;">
                        <h2>Za naše edukacije traži se mesto više! Zbog kvaliteta edukacije rad je isključivo u malim grupama!<br><br></h2>

                        <a href="tel:+381 62 455 200">
                            <h2>
                                +381 62 455 200
                            </h2>
                            
                        </a>
                        
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Appoinment Section End -->



<!-- Counter Area Start 
<section id="counter" class="counter">
    <div class="container">
        <div class="row">
            <div class=" col-md-6 col-lg-3">
                <div class="c-box">
                    <i class="pe-7s-diamond"></i>
                    <span class="count">1000</span><sup>+</sup>
                    <h3>Uspešnih obuka</h3>
                </div>
            </div>
            <div class=" col-md-6 col-lg-3">
                <div class="c-box">
                    <i class="pe-7s-leaf"></i>
                    <span class="count">9997</span><sup>+</sup>
                    <h3>Zadovoljnih klijenata</h3>
                </div>
            </div>
            <div class=" col-md-6 col-lg-3">
                <div class="c-box">
                    <i class="pe-7s-magic-wand"></i>
                    <span class="count">8656</span><sup>+</sup>
                    <h3>Proizvedenih oklagija</h3>
                </div>
            </div>
            <div class=" col-md-6 col-lg-3">
                <div class="c-box">
                    <i class="pe-7s-coffee"></i>
                    <span class="count">1000</span><sup>+</sup>
                    <h3>Neki info</h3>
                </div>
            </div>
        </div>
    </div>
</section>
 Counter Area End -->



<!-- Pricing Plan Start -->
<section id="pricePlan" class="pricePlan">
    <div class="container">
            <div class="row justify-content-center">
                    <div class="col-md-10 col-lg-8">
                <div class="sectionTheading">
                    <h2>
                    Cena edukacije 
                    </h2>
                    <img src="{{asset('img/sectionSeparatorw.png')}}" alt="">
                    <h5>
                            Izaberite svoj promo paket uz edukaciju za BB Glow tretman:
                    </h5>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2"></div>
            
            <div class="col-lg-4">
                <div class="p_box">
                    <div class="doller">
                    <span>start</span>
                    </div>
                    <div class="title">
                        <img class="img-fluid" src="{{asset('img/shape1.png')}}" alt="">
                        <i class="pe-7s-diamond"></i>
                    </div>
                    <div class="doller">
                        <!--<span><del>590 €</del></span>-->
                    <span>290 €</span>
                    </div>

                    <div class="list">
                        <ul>
                            Edukacija za BB Glow
                            
                            <li>
                                <strong>Edukacija za BB Glow</strong>
                            </li>
                            <li>
                                <strong>Skripta </strong>
                            </li>
                            <li>
                                <strong>Sertifikat</strong>
                            </li>
                            <li>
                                <strong>Starter set (10 ampula)</strong>
                            </li>
                            <li>
                                <strong>Podrška u radu</strong>
                            </li>
                            <li>
                                <strong>Trajni popust od 10% na proizvode</strong>
                            </li>
                            <li>
                                <strong><br></strong>
                            </li>
                        </ul>
                    </div>
                    <a class="phurchaseBtn" href="/kontakt">
                        Zakažite termin 
                    </a>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="p_box">
                    <div class="doller">
                    <span>smart</span>
                    </div>
                    <div class="title">
                        <img class="img-fluid" src="{{asset('img/shape1.png')}}" alt="">
                        <i class="pe-7s-magic-wand"></i>
                    </div>
                    <div class="doller">
                        <!--<span><del>690 €</del></span>-->
                    <span>390 €</span>
                    </div>

                    <div class="list">
                        <ul>
                             <li>
                                <strong>Edukacija za BB Glow</strong>
                            </li>
                            <li>
                                <strong>Skripta</strong>
                            </li>
                            <li>
                                <strong>Sertifikat</strong>
                            </li>
                            <li>
                                <strong>Starter set (10 ampula)</strong>
                            </li>
                            <li>
                                <strong>Dermapen aparat</strong>
                            </li>
                            <li>
                                <strong>Podrška u radu</strong>
                            </li>
                            
                            <li>
                                <strong>Trajni popust od 10% na proizvode</strong>
                            </li>
                        </ul>



                    </div>
                    <a class="phurchaseBtn" href="/kontakt">
                        Zakažite termin 
                    </a>
                </div>
            </div>
            
            
            
        </div>
    </div>
</section> 


<!-- Opening Time  Section Start -->
<section id="openingTime" class="openingTime">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-xl-8">
                <div class="openingLeft">
                    <div class="left">
                        <img class="img-fluid" src="img/openingTime.jpg" alt="">
                    </div>
                    <div class="right">
                        <h2>
                            BB GLOW
                            MADERO
                            ANTICELULIT
                            MASAŽA
                        </h2>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-xl-4">
                <div class="openingChart">
                    <div class="title">
                        <h2>
                            Radno vreme
                        </h2>
                    </div>
                    <div class="content">
                        <p>
                           Kozmetički salon Essence of Beauty nalazi se u Bulevaru Zorana Đinđića 6. Odiše toplinom i u njemu se klijenti tokom tretmana osećaju prijatno i relaksirano, a ljubaznost i profesionalnost osoblja je ključ našeg uspeha.
                        </p>
                        <ul class="time">
                            <li>
                                Pon-Pet: 13<sup>00</sup> – 20<sup>00</sup>
                            </li>
                            <li>
                                Subota: 9<sup>00</sup> – 17<sup>00</sup>
                            </li>
                            <li>
                                Nedelja: Ne radimo
                            </li>
                        </ul>
                        <a class="link" href="/kontakt">
                            Kontakt
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Opening Time  Section End -->

<!-- Team Section CSS Start -->
<section id="team" class="team">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10 col-lg-9">
                <div class="sectionTheading">
                    <h2>
                        UTISCI POLAZNIKA EDUKACIJA
                    </h2>
                    <img src="img/sectionSeparatorw.png" alt="">
                    <p>
                        Pogledajte utiske nekih od naših klijenata koji su svo stečeno znanje na Essence Of Beauty edukacijama odmah uspešno primenili i ostvarili trajne pogodnosti za kupovinu naših proizvoda.
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="t-box">
                    <div class="left">
                        <div class="img">
                            <img class=" img-fluid" src="img/edukacija-kurs-maderoterapija-bb-glow-anticelulit-masaza/kurs-maderoterapija-bb-glow-1.jpg" alt="">
                        </div>
                    </div>
                    <div class="right">
                        <div class="content">
                            <h4>
                                Svetlana Mosić
                            </h4>
                            <h6>
                                Edukacija  maderoterapija
                            </h6>
                            <p>
                                

Edukaciju maderoterapije završila sam u edukativnom centru Essence of Beauty.

Prezadovolja sam obukom, uz fenomenalnog predavača veoma lako sam stekla praktično znanje

tako da sada imam jako puno zadovoljnih klijentkinja
                            </p>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="t-box">
                    <div class="left">
                        <div class="img">
                            <img class=" img-fluid" src="img/edukacija-kurs-maderoterapija-bb-glow-anticelulit-masaza/kurs-maderoterapija-bb-glow-2.jpg" alt="">
                        </div>
                    </div>
                    <div class="right">
                        <div class="content">
                            <h4>
                                Nikoleta Vasiljević
                            </h4>
                            <h6>
                               Edukacija BB Glow, Hijaluron pen i Maderoterapija lica
                            </h6>
                            <p>
                                Stekla sam potrebna kako teorijska tako i praktična znanja
                                koja sam odmah počela da primenjujem u praksi
                            </p>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="t-box">
                    <div class="left">
                        <div class="img">
                            <img class=" img-fluid" src="img/edukacija-kurs-maderoterapija-bb-glow-anticelulit-masaza/kurs-maderoterapija-bb-glow-3.jpg" alt="">
                        </div>
                    </div>
                    <div class="right">
                        <div class="content">
                            <h4>
                                Sanja Stošić
                            </h4>
                            <h6>
                                Edukacije maderoterapija lica, tela, BB Glow i depilacija
                            </h6>
                            <p>
                                Prezadovoljna sam edukacijama koje sam završila u edukativnom centru Essence of Beauty,
                                predavačima i stečenim znanjem. Savršeno iskustvo.
                            </p>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="t-box">
                    <div class="left">
                        <div class="img">
                            <img class=" img-fluid" src="img/edukacija-kurs-maderoterapija-bb-glow-anticelulit-masaza/kurs-maderoterapija-bb-glow-4.jpg" alt="">
                        </div>
                    </div>
                    <div class="right">
                        <div class="content">
                            <h4>
                                Anaira Džanković 
                            </h4>
                            <h6>
                                BB Glow i Hijaluron pen edukacija
                            </h6>
                            <p>
                                Veoma sam zadovoljna edukacijom za BB Glow i Hijaluron pen. Svo stečeno znanje je odmah praktično primenljivo i kvalitetno.
                            </p>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Team Section CSS End -->
@stop