@extends('layout')

@section('title')
Epilacija - Salon Lepote - 
@stop

@section('scriptsTop')
<script>
    	fbq('track', 'SALON - stranica');
</script>
@stop

@section('sekcije')
<!-- banner Area Start -->
<section id="spabreadcrumb" class="spabreadcrumb">
	<div class="bcoverlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="Content">
					<h2>
						Epilacija
					</h2>
					<div class="links">
						<ul>
							<li>
								<a href="/">Naslovna</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a href="/salon">Salon Lepote</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a class="active" href="/salon/epilacija">Epilacija</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Welcome Area End -->



<!-- Video Section Start -->
<section id="video" class="video">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="vImg">
					
					<img class="img-fluid" src="{{asset('img/salon/epilacija.jpg')}}" alt="">
				</div>
			</div>
			<div class="col-lg-6">
				<h2>
					EPILACIJA DIODNIM LASEROM
				</h2>
				<h5>
					Laserska epilacija je brz, nežan i efikasan način za uklanjanje neželjenih dlačica sa trajnim rezultatima.
					Epilacija laserom predstavlja jedinu svetski priznatu i bezbednu metodu trajnog uklanjanja dlaka sa lica i tela.
					Lasersku epilaciju mogu raditi i muškarci i žene, na svim delovima tela, osim predela oko očiju.
					Laser usmerava koncentrisano svetlo u folikul dlake, koje se zatim pretvara u toplotu i na taj način sprečava sposobnost dlake da raste, bez ometanja ili oštećenja površine kože.		
				</h5>
			</div>
		</div>
	</div>
</section>
<!-- Video Section End -->

<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-10">
				<div class="sectionTheading">
					
					<h2>KAKO DA SE PRIPREMITE  ZA LASERSKU EPILACIJU?</h2>
					<img src="{{asset('img/sectionSeparator.png')}}" alt="">
					
					<h5>
					U cilju dobijanja brzih i dobrih rezultata, kada je trajno uklanjanje dlaka u pitanju, kao i apsolutnog izbegavanja i najmanjih neželjenih efekata, neophodna je adekvatna priprema koja podrazumeva sledeće:
					<br><br>
					· Mesec dana pre početka sa tretmanima epilacije, treba prestati sa depilacijom voskom i treba započeti brijanje regije koju želite tretirati
					<br><br>
					·  Nedelju dana pre početka sa tretmanima epilacije treba prekinuti izlaganje regije koju želite epilirati UV zracima (sunce, solarijum)
					<br><br>
					·          Mesec dana pre epilacije i u toku trajanja tretmana nikako ne treba koristiti proizvode za samopotamnjivanje kože
					<br><br>
					·          U pauzi izmedju tretmana, dlačice sa tretirane regije  isključivo treba brijati

					· 24 sata pre tretmana epilacije treba obrijati dlačice sa regije koja se epilira
					<br><br><br><br>
						</h5>			

					<h2>KOLIKO JE TRETMANA LASERSKE EPILACIJE POTREBNO I KOJI JE RAZMAK IZMEĐU TRETMANA?</h2>
 					<img src="{{asset('img/sectionSeparator.png')}}" alt="">
 					<h5>
						Prosečan broj potrebnih tretmana za regije na telu je oko 6<br>
						Prosečan broj tretmana za regije na licu, zavisi od razloga prekomerne maljavosti i može biti do 10<br>
						Preporučeni razmak između tretmana za regije na telu je 4-6 nedelja<br>
						Preporučeni razmak između tretmana za regije na licu je 3-5 nedelja<br>
						Napomena: Usled hormonalne neravnoteže bilo koje etiologije, broj tretmana, pogotovo u regiji lica, stomaka, grudi, zadnjice i natkolenica kod žena, može biti veći.<br>
					</h5>
					<br><br><br><br>

 					<h2>
 						ŠTA MOŽETE DA OČEKUJETE NAKON TRETMANA LASERSKE EPILACIJE?
 					</h2>
 					<img src="{{asset('img/sectionSeparator.png')}}" alt="">
 					<h5>
 						·          Nakon tretmana laserske epilacije, dlačice izrastaju iz kože u periodu od 15 do 30 dana i tada možete primetiti da na nekim delovima kože dlačice više ne rastu
						<br><br>
						·          Najmanje 7 dana posle tretmana treba izbegavati izlaganje tretirane regije UV zracima. Takođe treba izbegavati i upotrebu agresivnih preparata kao što su pilinzi i proizvodi koji sadrže voćne kiseline i retinol
						<br><br>
						·          Neposredno nakon epilacije moguća je pojava crvenila kože koje se povlači u roku od 48h

 					</h5>
					
				</div>
			</div>
		</div>
	</div>
</section>


<!-- Pricing Plan Start -->
<section id="counter" class="counter">
	<div class="container">
			<div class="row justify-content-center">
					<div class="col-md-12 col-lg-12">
				<div class="sectionTheading">
					<h2 style="color: white;">
					Pre  epilacije potrebno je da:
					</h2>
    				<h5 style="color: white;">
				    	Izbegavate čupanje dlačica najmanje 4 nedelje pre tretmana<br>
						Dlačice treba da budu veće od 1-2 milimetra<br>
						Izbegavate izlaganje suncu ili sunčanje u solarijumu<br>
						Izbegavati korišćenje krema za samopotamljivanje<br>
						Izbegavati korišćenje antikoagulanasa<br>

						
					</h5>
					<br><br>
					<h2 style="color: white;">
					 epilacija se ne preporučuje u sledećim slučajevima:
					</h2>
    				<h5 style="color: white;">
				    	Osobama mlađim od 16 godina<br>
						Trudnoće i periodu dojenja<br>
						Epilepsija<br>
						Dijabetes<br>
						Multa skleroza<br>
						Otvorene rane<br>
						Bolesti kože<br>
						Kancer<br>
						Srčana oboljenja ili pejsmejker<br>
						Stentovi<br>
					</h5>
				</div>
			</div>
		</div>
		
	</div>
</section> 
<!-- Pricing Plan End -->
<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-12">
				<div class="sectionTheading">
					<!--
					<img src="{{asset('/img/in.png')}}" alt="">
					-->
						<h4>
							Željene tretmane možete zakazati na broj: <br><strong><a href="tel:069/455-90-99">069/455-90-99</a></strong> 
						</h4>
				

				</div>
			</div>
		</div>
	</div>
</section>
@stop



