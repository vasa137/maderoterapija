@extends('layout')

@section('title')
BB Glow  - Salon Lepote - Ampule Za BB Glow - 
@stop

@section('scriptsTop')
<script>
    	fbq('track', 'SALON - stranica');
</script>
@stop

@section('sekcije')
<!-- banner Area Start -->
<section id="spabreadcrumb" class="spabreadcrumb">
	<div class="bcoverlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="Content">
					<h2>
						BB Glow
					</h2>
					<div class="links">
						<ul>
							<li>
								<a href="/">Naslovna</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a href="/salon">Salon Lepote</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a class="active" href="/salon/bb-glow">BB Glow</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Welcome Area End -->



<!-- Video Section Start -->
<section id="video" class="video">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="vImg">
					<img class="img-fluid" src="{{asset('img/edukacije-maderoterapija-tela-lica-bb-glow-srbija/bb-glow-srbija-edukacija-naslovna.jpg')}}" alt="">
				</div>
			</div>
			<div class="col-lg-6">
				<h2>
					BB Glow 
				</h2>
				<h5>
					Probajte da zamislite svoje lice koje izgleda besprekorno i blistavo, bez svakodnevnog šminkanja! Mislite da to nije moguće?
						Lepa vest je da ne morate da zamišljate jer je planetarno najpopularniji tretman BB Glow stigao u Srbiju! Dugo i pažljivo čuvana tajna savršenog tena žena iz Koreje je konačno otkrivena i dostupna damama u našoj zemlji. Istražite novi revolucionarni tretman kože sa visoko kvalitetnim proizvodima.<br>
						
						
				</h5>
				<!--
				<div class="videoContent">
					<div class="icon">
						<a class="venobox" data-vbtype="video" data-autoplay="true" href="http://vimeo.com/75976293">
							<i class="fas fa-play"></i>
						</a>
					</div>
					
					<div class="content d-flex">
						<div class="text align-self-center">
							<h5>intro</h5>
								<h5>Play Video</h5>
						</div>
					</div>
					
				</div>
				-->
			</div>
		</div>
	</div>
</section>
<!-- Video Section End -->


<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-10">
				<div class="sectionTheading">
					
						<h2>
							Besprekoran ten bez šminke?<br> Da, moguće je!
						</h2>
					
					<h2>
						Šta je to BB Glow?
					</h2>
					<img src="{{asset('img/sectionSeparator.png')}}" alt="">
					
					<h5>
						BB Glow je prijatan i bezbedan tretman kojim postižemo ujednečen ten, kao i poboljšanje tonusa kože lica. Najnoviji trend u tehnologiji za negu kože. Pruža trenutnu pokrivenost i dugotrajnu lepotu bez preterane stimulacije kože.  Ovim tretmanom postižemo prirodni izgled besprekorne i blistave kože lica, lagano posvetljujemo i toniziramo kožu, a sve to uz prikrivanje nedostataka kao što su flekice, pegice, crvenilo ili tamni podočnjaci.  Povrh svega navedenog, BB Glow ima i hidrirajući antiage efekat!
					</h5>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Counter Area Start -->
<section id="counter" class="counter">

<div class="container">
	<h2 style="color: white; text-align:center;">
	REZULTATI BB GLOW TRETMANA
	<br><br>
	</h2>
	<div class="row">
		<div class=" col-md-6 col-lg-3">
			<div class="c-box">
				<span class="">1</span>
				<h5 style="color: white;">Kože lica dobija trenutni sjaj , hidrirana je i jedra</h5>
			</div>
		</div>
		<div class=" col-md-6 col-lg-3">
			<div class="c-box">
				<span class="">2</span>
				<h5 style="color: white;">Ublaženi su podočnjaci, pege, fleke i ožiljci od akni</h5>
			</div>
		</div>
		<div class=" col-md-6 col-lg-3">
			<div class="c-box">
				<span class="">3</span>
				<h5 style="color: white;">Problem hiperpigmentacija i crvenila je rešen</h5>
			</div>
		</div>
		<div class=" col-md-6 col-lg-3">
			<div class="c-box">
				<span class="">4</span>
				<h5 style="color: white;">Sitne bore postaju nevidljive, BB Cream efekat</h5>
			</div>
		</div>
	</div>
</div>
</section>
<!-- Counter Area End -->


<!-- Video Section Start -->
<section id="video" class="video">
	<div class="container">
		<div class="row">

			<div class="col-12">
				<h2>
					Šta je BB Glow tretman?
				</h2>
				<h5>
					BB Glow je prijatan i bezbedan tretman kojim postižemo ujednačen ten, kao i poboljšanje opšteg stanja i tonusa kože lica. Pruža trenutnu pokrivenost i dugotrajnu lepotu bez preterane stimulacije kože.  Ovim tretmanom postižemo prirodni izgled besprekorne i blistave kože lica, lagano posvetljujemo i toniziramo kožu, a sve to uz prikrivanje nedostataka kao što su flekice, pegice, crvenilo ili tamni podočnjaci.  Povrh svega navedenog, BB Glow ima i hidrirajući antiage efekat!			
				</h5>
			</div>
		</div>
	</div>
</section>
<!-- Video Section End -->

<section id="serviceSection" class="video" style="background-color: #f8f8f8;">
	<div class="container">
		<div class="row">
			
			<div class="col-12">
				<h2>
					Kako se odvija BB Glow tretman? 
				</h2>
				<h5>
					Prvi korak tretmana podrazumeva pripremu kože i proces eksfoliacije, gde je moguće kombinovati različite tretmane u zavisnosti od tipa i stanja kože. Nakon toga sledi drugi korak tretmana, a to je unos boostera sa anti-ageing efektom, kao i efektom vlaženja i posvetljavanja kože lica. Boosteri se nanose needling tehnikom upotrebljavajući aparat dermapen. Nakon boostera sledi unos BB Glow seruma dostupnih u nekoliko nijansi koji se u toku tretmana prilagođavaju boji kože. BB Glow serum se sastoji od kompleksa visokokoncentrovanih aktivnih sastojaka biljnog porekla i pigmenata koji se takođe unose needling tehnikom.  Na kraju  treći korak obuhvata završnu negu i proces umirivanja kože, što činimo vlažnim i okluzivnim maskama.		
				</h5>
			</div>
		</div>
	</div>
</section>

<!-- Video Section Start -->
<section id="video" class="video">
	<div class="container">
		<div class="row">

			<div class="col-12">
				<h2>
					Da li je BB Glow tretman bolan?
				</h2>
				<h5>
					Samo izvođenje tretmana je prijatno za klijenta, bez obzira što se u procesu rada upotrebljava needling tehnika. Naša preporuka je upotreba nano iglica koje stvaraju površinske (epidermalne) perforacije.
				</h5>
			</div>
		</div>
	</div>
</section>
<!-- Video Section End -->

<section id="serviceSection" class="video" style="background-color: #f8f8f8;">
	<div class="container">
		<div class="row">
			
			<div class="col-12">
				<h2>
					Koliko BB Glow tretmana je potrebno uraditi da bi se dobio pravi efekat?
				</h2>
				<h5>
					Broj tretmana zavisi od tipa kože. Mi predlažemo da se nakon urađenog prvog tretmana, tretman ponovi za 14 dana, a nakon toga za dugotrajni efekat  je potrebno ponavljati tretmane na 4 do 6 nedelja.
				</h5>
			</div>
		</div>
	</div>
</section>

<!-- Video Section Start -->
<section id="video" class="video">
	<div class="container">
		<div class="row">

			<div class="col-12">
				<h2>
					Može li BB Glow tretman da se radi na bilo kom tipu kože? Postoje li ograničenja?
				</h2>
				<h5>				
					BB Glow tretman namenjen je svim tipovima kože u svrhu ujednačavanja tena i poboljšanja tonusa kože. Treba biti jako oprezan kod reaktivne kože i upalnih procesa, gde je prvo potrebno očistiti i umiriti kožu, a tek nakon toga sprovoditi tretman BB Glow.
				</h5>
			</div>
		</div>
	</div>
</section>
<!-- Video Section End -->

<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-12">
				<div class="sectionTheading">
					<!--
					<img src="{{asset('/img/in.png')}}" alt="">
					-->
						<h4>
							Željene tretmane možete zakazati na broj: <br><strong><a href="tel:069/455-90-99">069/455-90-99</a></strong> 
						</h4>
				

				</div>
			</div>
		</div>
	</div>
</section>
@stop



