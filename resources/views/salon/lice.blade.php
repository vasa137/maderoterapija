@extends('layout')

@section('title')
Tretmani Lica - Salon Lepote - 
@stop

@section('scriptsTop')
<script>
    	fbq('track', 'SALON - stranica');
</script>
@stop

@section('sekcije')
<!-- banner Area Start -->
<section id="spabreadcrumb" class="spabreadcrumb">
	<div class="bcoverlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="Content">
					<h2>
						Tretmani Lica
					</h2>
					<div class="links">
						<ul>
							<li>
								<a href="/">Naslovna</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a href="/salon">Salon Lepote</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a class="active" href="/salon/tretmani-lica">Tretmani Lica</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Welcome Area End -->



<!-- Video Section Start -->
<section id="video" class="video">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="vImg">
					
					<img class="img-fluid" src="{{asset('img/salon/tretmani-lica.jpg')}}" alt="">
				</div>
			</div>
			<div class="col-lg-6">
				<h2>
					Tretmani lica
				</h2>
				<h5>
					Briga o licu je važna za zdravlje vaše kože. Korišćenjem pravih tretmana prilagođenih tipu kože je odličan način da osigurate da vaše lice privuče pažnju koju zaslužuje. U Essence of beauty sve tretmane rade profesionalni kozmetičari.			
				</h5>
			</div>
		</div>
	</div>
</section>
<!-- Video Section End -->

<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-10">
				<div class="sectionTheading">
					
					<img src="{{asset('img/sectionSeparator.png')}}" alt="">

					<h2>
						<br>
							Higijenski tretman lica
						</h2>
					
					<h5>
						Higijenski tretman lica dubinski čisti kožu, uklanja sloj mrtvih ćelija na licu i nečistoće iz pora, vraća svežinu koži. Ovaj tretman pomaže da vaša koža bude zdrava i hidrirana sa ujednačenim tenom. Preporučuje se i ženama i muškarcima bez obzira na starost.<br>
						<strong>Higijenski tretman obuhvata:</strong><br>
						Mehanički piling<br>
						Dubinsko čišćenje lica<br>
						Nanošenje maske prema tipu kože<br>

					</h5>
				</div>
			</div>
		</div>
	</div>
</section>


<!-- Pricing Plan Start -->
<section id="counter" class="counter">
	<div class="container">
			<div class="row justify-content-center">
					<div class="col-md-10 col-lg-8">
				<div class="sectionTheading">
					<h2 style="color: white;">
					Ostali tretmani lica:
					</h2>
    				<h5 style="color: white;">
				    	Hydralift tretman lica <br>
						Antiage tretman lica <br>
						Tretman lica voćnim kiselinama<br>
						Ultrazvučno čišćenje lica<br>
						Masaža lica<br>
						Maderoterapija lica<br>
						
					</h5>
					
				</div>
			</div>
		</div>
		
	</div>
</section> 
<!-- Pricing Plan End -->
<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-12">
				<div class="sectionTheading">
					<!--
					<img src="{{asset('/img/in.png')}}" alt="">
					-->
						<h4>
							Željene tretmane možete zakazati na broj: <br><strong><a href="tel:069/455-90-99">069/455-90-99</a></strong> 
						</h4>
				

				</div>
			</div>
		</div>
	</div>
</section>
@stop



