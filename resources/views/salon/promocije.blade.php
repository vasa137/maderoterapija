@extends('layout')

@section('title')
Promocije - Salon Lepote - 
@stop

@section('scriptsTop')
<script>
    	fbq('track', 'SALON - stranica');
</script>
@stop

@section('sekcije')
<!-- banner Area Start -->
<section id="spabreadcrumb" class="spabreadcrumb">
	<div class="bcoverlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="Content">
					<h2>
						Promocije
					</h2>
					<div class="links">
						<ul>
							<li>
								<a href="/">Naslovna</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a href="/salon">Salon Lepote</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a class="active" href="/salon/promocije">Promocije</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Welcome Area End -->



<!-- Video Section Start -->
<section id="video" class="video">
	<div class="container">
		<div class="row">
			<div class="col-lg-2"></div>
			<div class="col-lg-8">
				<div class="vImg">
					
					<img class="img-fluid" src="{{asset('img/jun212121.jpg')}}" alt="">


				</div>
			</div>
	</div>
</section>
<!-- Video Section End -->
<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-12">
				<div class="sectionTheading">
					<!--
					<img src="{{asset('/img/in.png')}}" alt="">
					-->
						<h4>
							<strong>20% popusta</strong> za sve trudnice na sve redovne cene tretmana u salonu! <br>Zakažite na 
							<strong><a href="tel:062/455200">062/455200</a></strong> ili putem maila: <strong>info@essenceofbeauty.rs</strong>
						</h4>
				

				</div>
			</div>
		</div>
	</div>
</section>

@stop



