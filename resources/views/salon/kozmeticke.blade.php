@extends('layout')

@section('title')
Kozmetički tretmani - Salon Lepote - 
@stop

@section('scriptsTop')
<script>
    	fbq('track', 'SALON - stranica');
</script>
@stop

@section('sekcije')
<!-- banner Area Start -->
<section id="spabreadcrumb" class="spabreadcrumb">
	<div class="bcoverlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="Content">
					<h2>
						Kozmetički tretmani
					</h2>
					<div class="links">
						<ul>
							<li>
								<a href="/">Naslovna</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a href="/salon">Salon Lepote</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a class="active" href="/salon/kozmeticke-usluge">Kozmetički tretmani</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Welcome Area End -->



<!-- Video Section Start -->
<section id="video" class="video">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="vImg">
					
					<img class="img-fluid" src="{{asset('img/salon/kozmeticke-usluge/depilacija-hladnim-voskom.jpg')}}" alt="">
				</div>
			</div>
			<div class="col-lg-6">
				<h2>
					Depilacija hladnim voskom
				</h2>
				<h5>
					Kada je u pitanju depilacija, akcenat stavljamo na higijenu. Koristimo kvalitetan vosak za jednokratnu upotrebu. U Essence of beauty kozmetičari vode računa i o vašoj koži, pa stoga koriste vosak za osetljivu kožu. Depilacijom se temeljno uklanjaju dlačice sa regije po vašem izboru. Vosak se tanko i ravnomerno nanosi  na površinu kože, na koji se potiskuje traka koja se brzim pokretima uklanja zajedno sa dlačicama. Na kraju tretmana se nanosi losion koji smanjuje iritaciju kože i hidrira je. Ovim tretmanom se mogu tretirati sledeće regije: lice (nausnice, obrve, brada), ruke, noge, pazuh, prepone i brazilka.			
				</h5>
			</div>
		</div>
	</div>
</section>
<!-- Video Section End -->

<section id="serviceSection" class="video" style="background-color: #f8f8f8;">
	<div class="container">
		<div class="row">
			
			<div class="col-lg-6">
				<h2>
					Manikir 
				</h2>
				<h5>
					Imati privlačne nokte i negovane ruke može povećati samopouzdanje svake žene. Da bi smo vam pomogli da to postignete svakog dana u nedelji, u Essence of beauty vam nudimo tretmane koji odgovaraju vašim potrebama. 		
				</h5>
			</div>
			<div class="col-lg-6">
				<div class="vImg">
					
					<img class="img-fluid" src="{{asset('img/salon/kozmeticke-usluge/manikir.jpg')}}" alt="">
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Video Section Start -->
<section id="video" class="video">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="vImg">
					
					<img class="img-fluid" src="{{asset('img/salon/kozmeticke-usluge/estetski-manikir.jpg')}}" alt="">
				</div>
			</div>
			<div class="col-lg-6">
				<h2>
					Klasičan manikir
				</h2>
				<h5>
					Klasičan manikir se preporučuje osobama koje neguju prirodan izgled noktiju. Tretman obuhvata oblikovanje i poliranje nokatne ploče, uklanjanje i hranjenje zanoktica i lakiranje. Za lakiranje noktiju možete birati izmenju Crisnail lakova ili IBD trajnih antialergijskih lakova.		
				</h5>
			</div>
		</div>
	</div>
</section>
<!-- Video Section End -->

<section id="serviceSection" class="video" style="background-color: #f8f8f8;">
	<div class="container">
		<div class="row">
			
			<div class="col-lg-6">
				<h2>
					Spa manikir
				</h2>
				<h5>
					Spa manikir obuhvata oblikovanje i poliranje nokatne ploče, uklanjanje zanoktica, piling i masažu ruku, hranjenje zanoktica uljem i lakiranje. Za lakiranje noktiju možete birati izmenju Crisnail lakova ili IBD trajnih antialergijskih lakova.		
				</h5>
			</div>
			<div class="col-lg-6">
				<div class="vImg">
					
					<img class="img-fluid" src="{{asset('img/salon/kozmeticke-usluge/spa-manikir.jpg')}}" alt="">
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Video Section Start -->
<section id="video" class="video">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="vImg">
					
					<img class="img-fluid" src="{{asset('img/salon/kozmeticke-usluge/japanski-manikir.jpg')}}" alt="">
				</div>
			</div>
			<div class="col-lg-6">
				<h2>
					Japanski manikir
				</h2>
				<h5>
					Japanski manikir predstavlja ritual nege, gde uz primenu prirodnih preparata sa ekstratom morske trave, bambusa, crvenog čaja, krastavca, pčelinjeg voska i praha bisera na noktima ostaje zaštitni film i visoki sjaj koji se zadržava do 10 dana.			
				</h5>
			</div>
		</div>
	</div>
</section>
<!-- Video Section End -->

<section id="serviceSection" class="video" style="background-color: #f8f8f8;">
	<div class="container">
		<div class="row">
			
			<div class="col-lg-6">
				<h2>
					Parafinsko pakovanje
				</h2>
				<h5>
					Parafinsko pakovanje ruku je idealan tretman za suvu i ispucalu kožu ruku. Uz dubinsku hidrataciju kože, parafinsko pakovanje zagreva tretirano područje, što dovodi do povoljnog dejstva na kosti i krvne sudove. Posebno se preporučuje osobama koje imaju problem sa cirkulacijom i reumom.			
				</h5>
			</div>
			<div class="col-lg-6">
				<div class="vImg">
					
					<img class="img-fluid" src="{{asset('img/salon/kozmeticke-usluge/parafinsko-pakovanje-ruku.jpg')}}" alt="">
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Pricing Plan Start -->
<section id="counter" class="counter">
	<div class="container">
			<div class="row justify-content-center">
					<div class="col-md-10 col-lg-8">
				<div class="sectionTheading">
					<h2 style="color: white;">
					Ostali tretmani:
					</h2>
    				<h5 style="color: white;">
				    	Za sve osobe koje imaju manjak vremena za negu svojih noktiju na nedeljnom nivou, preporučujemo jedan od ostalih tretmana uz koje se možete opustiti i do mesec dana.<br> 
						-	Ojačavanje prirodnih noktiju<br> 
						-	Trajni lak IBD antialergijskim lakovima <br> 
						-	Trajni french<br> 
						<br> 
						
					</h5>
					
				</div>
			</div>
		</div>
		
	</div>
</section> 


<!-- Video Section Start -->
<section id="video" class="video">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="vImg">
					
					<img class="img-fluid" src="{{asset('img/salon/kozmeticke-usluge/pedikir.jpg')}}" alt="">
				</div>
			</div>
			<div class="col-lg-6">
				<h2>
					Pedikir
				</h2>
				<h5>
					Pedikir je kozmetički tretman stopala i noktiju. On se vrši u terapeutske, estetske i medicinske svrhe i može pomoći u sprečavanju bolesti noktiju. Pedikir je odličan način da osigurate brigu o svojim stopalima. U Essence of beauty možete izabrati uslugu pedikira prema svojim potrebama.		
				</h5>
			</div>
		</div>
	</div>
</section>
<!-- Video Section End -->


<!-- Video Section Start -->
<section id="serviceSection" class="video" style="background-color: #f8f8f8;">
	<div class="container">
		<div class="row">
			
			<div class="col-lg-6">
				<h2>
					Estetski pedikir
				</h2>
				<h5>
					Estetski pedikir je osnovna nega stopala, koja se preporučuje bar jednom mesečno. On obuhvata oblikovanje i poliranje nokatne ploče, uklanjanje zanoktica, uklanjanje naslaga sa peta i lakiranje. Za lakiranje noktiju možete birati izmenju Crisnail lakova ili IBD trajnih antialergijskih lakova.	
				</h5>
			</div>
			<div class="col-lg-6">
				<div class="vImg">
					
					<img class="img-fluid" src="{{asset('img/salon/kozmeticke-usluge/estetski-pedikir.jpg')}}" alt="">
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Video Section End -->

<section id="video" class="video">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="vImg">
					
					<img class="img-fluid" src="{{asset('img/salon/kozmeticke-usluge/medicinski-pedikir.jpg')}}" alt="">
				</div>
			</div>
			<div class="col-lg-6">
				<h2>
					Medicinski pedikir
				</h2>
				<h5>
					Medicinski pedikir rešava svu problematiku vaših stopala. Možemo se pohvaliti visokom nivou stručnosti naših kozmetičara. Tretman obuhvata oblikovanje i poliranje nokatne ploče uz korekciju uraslih noktiju, uklanjanje zadebljanja sa stopala, rešavanje ostalih problema stopala i lakiranje. Za lakiranje noktiju možete birati izmenju Crisnail lakova ili IBD trajnih antialergijskih lakova. Svi alati za pružanje ove usluge su sterilisani u našem sterilizatoru.	
				</h5>
			</div>
			
		</div>
	</div>
</section>

<!-- Video Section Start -->
<section id="serviceSection" class="video" style="background-color: #f8f8f8;">

	<div class="container">
		<div class="row">
			
			<div class="col-lg-6">
				<h2>
					SPA pedikir
				</h2>
				<h5>
					SPA pedikir obuhvata oblikovanje i poliranje nokatne ploče, uklanjanje zanoktica, piling i masažu stopala, hranjenje zanoktica uljem i lakiranje. Za lakiranje noktiju možete birati izmenju Crisnail lakova ili IBD trajnih antialergijskih lakova.			
				</h5>
			</div>
			<div class="col-lg-6">
				<div class="vImg">
					
					<img class="img-fluid" src="{{asset('img/salon/kozmeticke-usluge/spa-pedikir.jpg')}}" alt="">
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Video Section End -->

<section id="video" class="video">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="vImg">
					
					<img class="img-fluid" src="{{asset('img/salon/kozmeticke-usluge/parafinsko-pakovanje-stopala.jpg')}}" alt="">
				</div>
			</div>
			<div class="col-lg-6">
				<h2>
					Parafinsko pakovanje stopala
				</h2>
				<h5>
					Parafinsko pakovanje stopala je idealan tretman za suvu i ispucalu kožu stopala. Uz dubinsku hidrataciju kože, parafinsko pakovanje zagreva tretirano područje, što dovodi do povoljnog dejstva na kosti i krvne sudove. Posebno se preporučuje osobama koje imaju problem sa cirkulacijom i reumom.			
				</h5>
			</div>
			
		</div>
	</div>
</section>

<!-- 
<section id="counter" class="counter">
	<div class="container">
			<div class="row justify-content-center">
					<div class="col-md-10 col-lg-8">
				<div class="sectionTheading">
					<h2 style="color: white;">
					Za sve koji vole izgled savršenih stopala, u ponudi imamo i:
					</h2>
    				<h5 style="color: white;">
						-	Trajni lak na stopalima<br> 
						-	Ojačavanje gelom<br> 
						-	Trajni french<br> 

						
						
					</h5>
					
				</div>
			</div>
		</div>
		
	</div>
</section> 
-->
<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-12">
				<div class="sectionTheading">
					<!--
					<img src="{{asset('/img/in.png')}}" alt="">
					-->
						<h4>
							Željene tretmane možete zakazati na broj: <br><strong><a href="tel:069/455-90-99">069/455-90-99</a></strong> 
						</h4>
				

				</div>
			</div>
		</div>
	</div>
</section>
@stop



