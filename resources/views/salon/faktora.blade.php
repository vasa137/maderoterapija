@extends('layout')

@section('title')
Fraktora lifting - Salon Lepote - 
@stop

@section('scriptsTop')
<script>
    	fbq('track', 'SALON - stranica');
</script>
@stop

@section('sekcije')
<!-- banner Area Start -->
<section id="spabreadcrumb" class="spabreadcrumb">
	<div class="bcoverlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="Content">
					<h2>
						Epilacija
					</h2>
					<div class="links">
						<ul>
							<li>
								<a href="/">Naslovna</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a href="/salon">Salon Lepote</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a class="active" href="/salon/fraktora-lifting">Fraktora lifting</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Welcome Area End -->



<!-- Video Section Start -->
<section id="video" class="video">
	<div class="container">

		<div class="row">
			<div class="col-lg-6">
				<div class="vImg">
					
					<img class="img-fluid" src="{{asset('img/salon/faktora-lifting.jpg')}}" alt="">
				</div>
			</div>
			<div class="col-lg-6">
				<h2>
					Ekskluzivno u salonu Essenceof Beauty: Fraktora lifting
					
				</h2>
				<h5>
					


	
				</h5>
			</div>
		</div>
	</div>
</section>
<!-- Video Section End -->

<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-10">
				<div class="sectionTheading">
					
					<h2>Šta je to Fraktora lifting?</h2>
					<img src="{{asset('img/sectionSeparator.png')}}" alt="">
					
					<h5>
					Prijatan, bezbedan i bezbolan tretman radiotalasima koji će probuditi vaš kolagen, zategnuti lice ili telo, ukloniti najsitnije bore sa vašeg lica, vrata ili dekoltea, smanjjiti pore, ublažiti ožiljake od akni, redukovati hiperpigmentacije i izjednačiti ten.
					<br><br>
					Fraktora je poslednja reč tehnike u primeni multipolarne radio-frekvencije za rejuvenaciju kože. Idealni razmak između elektroda omogućava da se bipolarna radiofrekventna energija veoma ravnomerno i precizno aplicira na tretiranu površinu kože.
					<br><br>
					RF energija generiše toplotu oko elektrode koja indukuje mikropovređivanje ćelija i na taj način podstiče proces regeneracije. Za razliku od standardnih bipolarnih RF aparata, ovde je novitet matrica gustih polova koja dozvoljava da se prenosi mnogo veća količina toplotne energije, a to je bezbolnije za klijenta i bez negativnih efekata kao što su opekotine i preterano crvenilo.
					<br><br>
					<br><br>
						</h5>			

					<h2>Koliko često treba raditi tretman?</h2>
 					<img src="{{asset('img/sectionSeparator.png')}}" alt="">
 					<h5>
						Tretmani zatezanja lica, vrata, dekoleta ili opuštene kože stomaka, kolena i ruku izvode se jednom nedeljno u seriji od 4 do 6 tretmana.
						<br><br>
						Nakon tretmana koža se blago crveni i klijent ima osećaj kao da mu tretirana regija malo bridi, ali je vreme oporavka kratko i moguće je odmah nastaviti radni dan.
						Trajanje tretmana je oko 30 min.
					</h5>
					<br><br><br><br>

				</div>
			</div>
		</div>
	</div>
</section>


<!-- Pricing Plan Start -->
<section id="counter" class="counter">
	<div class="container">
			<div class="row justify-content-center">
					<div class="col-md-12 col-lg-12">
				<div class="sectionTheading">
					<h3 style="color: white;">
					Kada su vidljivi rezultati tretmana?<br><br>
					</h3>
    				<h2 style="color: white;">
				    	Rezultati su vidljivi već sutradan nakon prvog tretmana.

						
					</h2>
					
				</div>
			</div>
		</div>
		
	</div>
</section> 
<!-- Pricing Plan End -->
<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-12">
				<div class="sectionTheading">
					<!--
					<img src="{{asset('/img/in.png')}}" alt="">
					-->
						<h4>
							Željene tretmane možete zakazati na broj: <br><strong><a href="tel:069/455-90-99">069/455-90-99</a></strong> 
						</h4>
				

				</div>
			</div>
		</div>
	</div>
</section>
@stop



