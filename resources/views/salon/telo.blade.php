@extends('layout')

@section('title')
Masaže i tretmani tela - Salon Lepote - 
@stop

@section('scriptsTop')
<script>
    	fbq('track', 'SALON - stranica');
</script>
@stop

@section('sekcije')
<!-- banner Area Start -->
<section id="spabreadcrumb" class="spabreadcrumb">
	<div class="bcoverlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="Content">
					<h2>
						Masaže i tretmani tela
					</h2>
					<div class="links">
						<ul>
							<li>
								<a href="/">Naslovna</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a href="/salon">Salon Lepote</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a class="active" href="/salon/tretmani-tela">Masaže i tretmani tela</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Welcome Area End -->



<!-- Video Section Start -->
<section id="video" class="video">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="vImg">
					
					<img class="img-fluid" src="{{asset('img/salon/tretmani-tela/masaza.jpg')}}" alt="">
				</div>
			</div>
			<div class="col-lg-6">
				<h2>
					Masaže i tretmani tela
				</h2>
				<h5>
					Potrebno je s’vremena na vreme priuštiti sebi beg od svakodnevnice uz masažu koja zadovoljava vaše potrebe. Svi naši fizioterapeuti su visoko kvalifikovani za različite vrste masaža, tako da vam možemo garantovati najbolje od svake izabrane masaže. Masaža vam pomaže da živite zdravije, što uključuje: bolju cirkulaciju, smanjenje krvnog pritiska, oslobađanje od stresa i napetosti, ojačavanje imunološkog sistema, poboljšavanje spavanja, poboljšavanje fleksibilnosti i uklanjanje bola u određenim regijama. U Essence of beauty sve masaže mogu koristiti i muškarci i žene.		
				</h5>
			</div>
		</div>
	</div>
</section>
<!-- Video Section End -->

<section id="serviceSection" class="video" style="background-color: #f8f8f8;">
	<div class="container">
		<div class="row">
			
			<div class="col-lg-6">
				<h2>
					Anticelulit masaža 
				</h2>
				<h5>
					Anticelulit masaža se fokusira na problematična područja za oslobađanje od celulita. Masaža ima za cilj da stimulišući krvnu i limfnu cirkulaciju omogući oslobađanje celulita iz kapsule i da ga putem organa za detoksifikaciju izbaci iz organizma. S obzirom da je lociran 3-5 milimetara ispod površine kože, u vezivnom tkivu, nema razloga za grubim pritiskanjima i oštećenjima krvnih sudova koji će se oglasiti modricama. Prilikom masaže se koriste kreme, emulzije i ulja. Efekat tretmana se poboljšava, ako se nakon tretmana stavi pakovanje na tretirane regije.		
				</h5>
			</div>
			<div class="col-lg-6">
				<div class="vImg">
					
					<img class="img-fluid" src="{{asset('img/salon/tretmani-tela/anticelulit-masaza.jpg')}}" alt="">
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Video Section Start -->
<section id="video" class="video">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="vImg">
					
					<img class="img-fluid" src="{{asset('img/salon/tretmani-tela/maderoterapija.jpg')}}" alt="">
				</div>
			</div>
			<div class="col-lg-6">
				<h2>
					Maderoterapija
				</h2>
				<h5>
					Maderoterapija je potpuno prirodan tretman protiv celulita pri kome se masaža izvodi pomoću specijalno dizajniranih oklagija različitog oblika, koje obezbeđuju intenzivan pritisak na celulit, poboljšava cirkulaciju i izbacivanje viška tečnosti i toksina iz organizma. Nakon samo nekoliko tretmana koža postaje blistavija, zategnutija i elastičnija, dok su krajnji rezultati smanjeni obimi na željenim mestima, preoblikovane noge i podignut gluteus. Drveni elementi koji se koriste za maderoterapiju su antialergijski, a oblik im dozvoljava lakšu pristupačnost svim delovima tela koje tretiramo.<br>
					<strong>
					Uz efikasno rešavanje celulita, maderoterapija ima još pozitivnih efekata na naše telo:<br>
					</strong>
					-	Ojačava imuni sistem<br>
					-	Uspostavlja balans između uma, tela i duha<br>
					-	Poboljšava mišićni tonus<br>
					-	Reguliše cirkulaciju krvi i limfnu cirkulaciju<br>
					-	Smanjuje lokalizovane masne naslage<br>
					-	Pospešuje produkciju elastina, vitamina E i kolagena, koji su značajni za kvalitet kože koji slabi tokom godina<br>
	
				</h5>
			</div>
		</div>
	</div>
</section>
<!-- Video Section End -->

<section id="serviceSection" class="video" style="background-color: #f8f8f8;">
	<div class="container">
		<div class="row">
			
			<div class="col-lg-6">
				<h2>
					Terapeutska masaža
				</h2>
				<h5>
					Terapeutska masaža ili parcijalna masaža se primenjuje na zahtev klijenta i odnosi se na tretiranje određenog dela tela (ruku, nogu, stopala, leđa ili glave). Prilikom ove masaže koriste se različite tehnike koje su nastale iz različitih stilova masaža. Naš fizioterapeut će prilagoditi tehniku  prema vašem stanju i potrebama. Cilj ove masaže je da se tretirani deo dodatno opusti  i omogući skladno funkcionisanje kompletnog organizma.Terapeutsku masažu mogu koristiti i osobe koje nemaju konkretan problem, već žele da održe dobru fizičku formu. 		
				</h5>
			</div>
			<div class="col-lg-6">
				<div class="vImg">
					
					<img class="img-fluid" src="{{asset('img/salon/tretmani-tela/terapeutska-masaza.jpg')}}" alt="">
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Video Section Start -->
<section id="video" class="video">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="vImg">
					
					<img class="img-fluid" src="{{asset('img/salon/tretmani-tela/sportska-masaza.jpg')}}" alt="">
				</div>
			</div>
			<div class="col-lg-6">
				<h2>
					Sportska masaža
				</h2>
				<h5>
					Sportska masaža je predviđena da spreči i ublaži bol i nelagodnost od povreda i stanja vezanih za vežbanje ili sport. Preporučuje se profesionalnim sportistima, rekreativcima i svima čije je telo izloženo većim naporima. Ova masaža pruža zagrevanje i opuštanje tkiva, pri čemu dolazi do poboljšanja cirkulacije, poravnanja mišićnih vlakana i izbacivanja mlečnih kiselina iz organizma. <br>
					<strong>Prednosti sportske masaže su:</strong><br>
					-	Oslobađanje otoka uzrokovanim sportskim povredama<br>
					-	Smanjenje napetosti mišića u toku trajanja vežbanja <br>
					-	Sprečavanje dalje povrede tokom vežbanja<br>
					-	Smanjenje iscrpljenosti nakon intezivnog treninga<br>
		
				</h5>
			</div>
		</div>
	</div>
</section>
<!-- Video Section End -->

<section id="serviceSection" class="video" style="background-color: #f8f8f8;">
	<div class="container">
		<div class="row">
			
			<div class="col-lg-6">
				<h2>
					Relaks masaža
				</h2>
				<h5>
					Relaks masaža predstavlja pravi izbor ako vam je potrebno da se oslobodite stresa i pobegnete od svakodnevnice. Ona uz negu vašeg tela, podiže opšte zdravstveno stanje, oslobađa od stresa i relaksira sve mišiće. Ova masaža počinje nežnim potezima da bi se pronašle regije napetosti na vašem telu. Uz pomoć mirisnih ulja, koriste se dugotrajni potezi za omekšavanje mišića, glatkog vezivnog tkiva i oslobađanje napetosti. Preporučuje se svima.<br>
					<strong>Prednosti relaks masaže su:</strong>

					-	Poboljšanje cirkulacije<br>
					-	Pospešivanje celokupne pokretljivosti tela<br>
					-	Oslobađanje niskog stepena boli u mišićima<br>
					-	Potpuno opuštanje i relaksacija<br>
		
				</h5>
			</div>
			<div class="col-lg-6">
				<div class="vImg">
					
					<img class="img-fluid" src="{{asset('img/salon/tretmani-tela/relaks-masaza.jpg')}}" alt="">
				</div>
			</div>
		</div>
	</div>
</section>


<!-- Video Section Start -->
<section id="video" class="video">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="vImg">
					
					<img class="img-fluid" src="{{asset('img/salon/tretmani-tela/kokos-masaza.jpg')}}" alt="">
				</div>
			</div>
			<div class="col-lg-6">
				<h2>
					Coconut magic treatment
				</h2>
				<h5>
					Coconut magic treatment smo osmislili specijalno za vas, koji osim potpunog opuštanja želite da pružite svojoj koži maksimalno kvalitetnu negu. Radi se o posebnoj vrsti antistres masaže organskim hladnoceđenim kokosovim uljem, koje će dubinski hidrirati i nahraniti vašu kožu, snabdeti je dovoljnom količinom vitamina E i zaštititi je od mikroba i infekcija.  Celokupan tretman traje 60 minuta.
				</h5>
			</div>
		</div>
	</div>
</section>
<!-- Video Section End -->

<section id="serviceSection" class="video" style="background-color: #f8f8f8;">
	<div class="container">
		<div class="row">
			
			<div class="col-lg-6">
				<h2>
					Čoko SPA tretman 
				</h2>
				<h5>
					Čoko SPA tretman je najlakši put za postizanje opuštanja. Kombinacija relaks masaže uljem od čokolade, piliga tela, nanošenje kreme od čokolade za negu tela je samo uvodni deo ovog spa tretmana. Mislili smo i na vaše lice, koje pripremamo čišenjem mlekom i tonikom. Zatim nanosimo masku od čokolade, posle koje dolazi krema i antirid prema tipu vaše kože. Da bi užitak bio potpun, tu je i parafinski tretman ruku. Celokupan tretman traje 60 minuta, od toga je 45 minuta relaks masaža.		
				</h5>
			</div>
			<div class="col-lg-6">
				<div class="vImg">
					
					<img class="img-fluid" src="{{asset('img/salon/tretmani-tela/cokolada-masaza.jpg')}}" alt="">
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Video Section Start -->
<section id="video" class="video">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="vImg">
					
					<img class="img-fluid" src="{{asset('img/salon/tretmani-tela/marocan.jpeg')}}" alt="">
				</div>
			</div>
			<div class="col-lg-6">
				<h2>
					Marokanski SPA tretman
				</h2>
				<h5>
					Marokanski SPA tretman se sastoji od relaks masaže arganovim uljem, koje u Maroku zbog svog fantastičnog dejstva nazivaju tečnim zlatom. Nakon masaže sledi piling tela, koie će očistiti, hidrirati i dati vašoj koži svežiji izgled čineći je mekšom i glatkom. Tretman  završavamo nanošenjem kreme od argana za negu tela i parafinskim pakovanjem ruku. Celokupan tretman traje 60 minuta, od toga je 45 minuta relaks masaža.	
				</h5>
			</div>
		</div>
	</div>
</section>
<!-- Video Section End -->

<section id="serviceSection" class="video" style="background-color: #f8f8f8;">
	<div class="container">
		<div class="row">
			
			<div class="col-lg-6">
				<h2>
					Duo relaks masaža
				</h2>
				<h5>
					Duo relaks masaža ili masaža u paru je sve traženija usluga koju pružamo svojim klijentima. Pri ovoj masaži dve osobe bivaju masirane u istoj prostoriji sa dva masažna stola, istovremeno od strane dva terapeuta. Ova masaža vam daje jedinstvenu priliku da provedete divne trenutke sa svojom bliskom osobom uz ćaskanje ili da se zajedno opustite u tišini. Izbor je na vama. 
				</h5>
			</div>
			<div class="col-lg-6">
				<div class="vImg">
					
					<img class="img-fluid" src="{{asset('img/salon/tretmani-tela/duo-masaza.jpg')}}" alt="">
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Video Section Start -->
<section id="video" class="video">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="vImg">
					
					<img class="img-fluid" src="{{asset('img/salon/tretmani-tela/elektrostimulacija.jpg')}}" alt="">
				</div>
			</div>
			<div class="col-lg-6">
				<h2>
					Elektrostimulacija mišića
				</h2>
				<h5>
					Elektrostimulacija mišića  je tretman kojim se putem aparata i struje niske frekvencije podiže tonus mišića tela. Stimulisanjem mišića određenih regija poboljšava se cirkulacija i vrši bolja drenaža tkiva i ubrzano sagorevanje masti. Prilikom kontrakcije mišići crpe energiju iz sopstvenog glikogena i okolnih masnih naslaga. Na taj način se podstiče gubitak masnih naslaga i može se smanjiti obim 2-3 cm. Mogu se tretirati sledeće regije: gluteus, noge, stomak i ruke. Tretman je namenjen kako damama tako i muškarcima.			
				</h5>
			</div>
		</div>
	</div>
</section>
<!-- Video Section End -->

<section id="serviceSection" class="video" style="background-color: #f8f8f8;">
	<div class="container">
		<div class="row">
			
			<div class="col-lg-6">
				<h2>
					IR detox tretman tela
				</h2>
				<h5>
					IR detox tretman tela je odličan za sagorevanje kalorija i oslobađanje tela od neželjenih toksina. Tretman pomaže da se opustite, ublažite bol, sagorite masti, povećate cirkulaciju, poboljšate teksturu kože, pojačate energiju, osnažite metabolizam i smanjite nivo stresa.<br>
					Infracrveno ćebe je sačinjeno od dve zone mršavljenja, gde prednji i zadnji deo tela gube ravnomerno težinu. Kada ležite pritisak na vašu kičmu je za upola manji, mišići slabe i celo telo se opušta. Krv cirkuliše bolje i napor na vaše srce se smanjuje. Šire se kapilari što omogućava kiseoniku da teče efikasnije kroz celo telo.<br>
					Infracrveno ćebe je rešenje za zdravo mršavljenje bez napornog vežbanja. U toku samo jednog tretmana od 30 minuta, možete sagoreti minimum 600 kalorija. Posle prvog tretmana se nepovratno skine 1-4 cm (250-300g).
					<strong>
					Kontraindikacije (potrebna je konsultacija lekara pre tretmana u navedenim slučajevima):<br>
					</strong>
					-	Epilepsija<br>
					-	Srčana oboljenja ili pejsmejker<br>
					-	Hipertenzija<br>
					-	Trudnoća ili laktacija<br>
			
				</h5>
			</div>
			<div class="col-lg-6">
				<div class="vImg">
					
					<img class="img-fluid" src="{{asset('img/salon/tretmani-tela/infra.jpg')}}" alt="">
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Video Section Start -->
<section id="video" class="video">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="vImg">
					
					<img class="img-fluid" src="{{asset('img/salon/tretmani-tela/piling-tela.jpg')}}" alt="">
				</div>
			</div>
			<div class="col-lg-6">
				<h2>
					Piling tela
				</h2>
				<h5>
					Piling tela uklanja mrtve ćelije sa kože, ostavljajući je mekom, glatkom i svežom. Prilikom tretmana piling se lagano utrljava i masira preko kože na potrebnim regijama, a zatim se ispira. On predstavlja dobru osnovu za druge tretmane tela. Takođe, poboljšava cirkulaciju krvi i limfe na površini kože, pomažući joj u borbi protiv celulita i utiče na poboljšanje tena kože.			
				</h5>
			</div>
		</div>
	</div>
</section>
<!-- Video Section End -->

<!-- Pricing Plan End -->
<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-12">
				<div class="sectionTheading">
					<!--
					<img src="{{asset('/img/in.png')}}" alt="">
					-->
						<h4>
							Željene tretmane možete zakazati na broj: <br><strong><a href="tel:069/455-90-99">069/455-90-99</a></strong> 
						</h4>
				

				</div>
			</div>
		</div>
	</div>
</section>
@stop



