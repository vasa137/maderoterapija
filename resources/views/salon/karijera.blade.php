@extends('layout')

@section('title')
Karijera - Salon Lepote - 
@stop

@section('scriptsTop')
<script>
    	fbq('track', 'SALON - stranica');
</script>
@stop

@section('sekcije')
<!-- banner Area Start -->
<section id="spabreadcrumb" class="spabreadcrumb">
	<div class="bcoverlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="Content">
					<h2>
						Karijera
					</h2>
					<div class="links">
						<ul>
							<li>
								<a href="/">Naslovna</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a href="/salon">Salon Lepote</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a class="active" href="/salon/karijera">Karijera</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Welcome Area End -->



<!-- Video Section Start -->
<section id="video" class="video">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="vImg">
					
					<img class="img-fluid" src="{{asset('img/posao-maderoterapija-bb-glow-srbija.jpg')}}" alt="">
				</div>
			</div>
			<div class="col-lg-6">
				<h2>
					Postanite deo Essence of Beauty tima!
				</h2>
				<h5>
					Ukoliko ste profesionalni kozmetičar ili fizioteraput pozivamo vas da nam se pridružite!			
				</h5>
			</div>
		</div>
	</div>
</section>
<!-- Video Section End -->

<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-10">
				<div class="sectionTheading">
					
					<img src="{{asset('img/sectionSeparator.png')}}" alt="">

					<h2>
						<br>
							Šta treba da posedujete ?
						</h2>
					
					<h5>
						Pozitivnu energiju, vedrinu, inovativnost i nadahnuće koje ste spremni da podelite sa nama<br>
					    Volju da se kontinuirano usavršavate<br>
					    Iskrenost i otvorenost u komunikaciji, fleksibilnost u postupanju<br>
					    Diplomu o završenoj edukaciji za kozmetičara ili fizioterapeuta<br>
					    Radno iskustvo od minimum godinu dana na poziciji kozmetičara ili fizioterapeuta<br>
					</h5>
				</div>
			</div>
		</div>
	</div>
</section>


<!-- Pricing Plan Start -->
<section id="counter" class="counter">
	<div class="container">
			<div class="row justify-content-center">
					<div class="col-md-10 col-lg-8">
				<div class="sectionTheading">
					<h2 style="color: white;">
					Zašto Essence of Beauty ?
					</h2>
    				<h5 style="color: white;">
				    Stalni radni odnos u prijatnoj i pozitivnoj radnoj atmosferi<br>
				    Permanentno stručno usavršavanje na poziciji kozmetičara ili fizioterapeuta.<br>
				    Mogućnost napredovanja i stimulativna finansijska nadoknada.<br>
				    Uplata penzijskog doprinosa u dobrovoljni penzioni fond DDOR Garant.<br>
					</h5>
					
				</div>
			</div>
		</div>
		
	</div>
</section> 
<!-- Pricing Plan End -->
<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-12">
				<div class="sectionTheading">
					<!--
					<img src="{{asset('/img/in.png')}}" alt="">
					-->
						<h4>
							Prijavu za radno mesto u našem salonu možete izvršiti  slanjem  vaše radne biografije na e-mail adresu: <strong>info@essenceofbeauty.rs</strong>
						</h4>
				

				</div>
			</div>
		</div>
	</div>
</section>
@stop



