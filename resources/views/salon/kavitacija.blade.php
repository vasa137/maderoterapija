@extends('layout')

@section('title')
Kavitacija  - Salon Lepote -  
@stop

@section('scriptsTop')
<script>
    	fbq('track', 'SALON - stranica');
</script>
@stop

@section('sekcije')
<!-- banner Area Start -->
<section id="spabreadcrumb" class="spabreadcrumb">
	<div class="bcoverlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="Content">
					<h2>
						Kavitacija
					</h2>
					<div class="links">
						<ul>
							<li>
								<a href="/">Naslovna</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a href="/salon">Salon Lepote</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a class="active" href="/salon/kavitacija">Kavitacija</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Welcome Area End -->



<!-- Video Section Start -->
<section id="video" class="video">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="vImg">
					<img class="img-fluid" src="{{asset('img/edukacije-maderoterapija-tela-lica-bb-glow-srbija/kavitacija-salon-beograd.png')}}" alt="">
				</div>
			</div>
			<div class="col-lg-6">
				<h2>
					Kavitacija
				</h2>
				<h5>
					Problem lokalizovanih masnih naslaga nije moguće uspešno rešiti klasičnim dijetama.
					Ultrazvučna lipoliza, poznata i pod terminom kavitacija je brza, neinvazivna i potpuno bezbolna metoda koja predstavlja revolucionarno otkriće za uspešnu eliminaciju masnih naslaga i celulita pomoću niskofrekventnih ultrazvučnih talasa. Ovim tertmanom postiže se redukcija masnih naslaga u predelu bokova, stomaka, kukova, nogu, nadlaktica. Kavitacija-ultrazvučna lipoliza se izvodi jednom nedeljno. Tretmani u našem estetskom centru podrazumevaju obaveznu limfnu drenažu (ručnu ili aparaturnu) kao i preporuku za povećan unos tečnosti-prvenstveno vode, režim ishrane i adekvatni neizostavni aerobni trening. Rezultati su vidljivi nakon prvog tretmana. 
					<br>
						
						
				</h5>
				<!--
				<div class="videoContent">
					<div class="icon">
						<a class="venobox" data-vbtype="video" data-autoplay="true" href="http://vimeo.com/75976293">
							<i class="fas fa-play"></i>
						</a>
					</div>
					
					<div class="content d-flex">
						<div class="text align-self-center">
							<h5>intro</h5>
								<h5>Play Video</h5>
						</div>
					</div>
					
				</div>
				-->
			</div>
		</div>
	</div>
</section>
<!-- Video Section End -->


<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-10">
				<div class="sectionTheading">
					
						<h2>
							KOLIKO JE TRETMANA POTREBNO?
						</h2>
					
					<img src="{{asset('img/sectionSeparator.png')}}" alt="">
					
					<h5>
						
Tretmani ultrazvučne lipolize rade se jednom nedeljno, sa obaveznom limfnom drenažom koju je neophodno uraditi odmah nakon tretmana. Broj tretmana je individualan i određuje ga naš terapeut na konsultaciji.

					</h5>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Counter Area Start -->
<section id="counter" class="counter">

<div class="container">
	<h2 style="color: white; text-align:center;">
	KADA SE MOGU VIDETI PRVI REZULTATI?


	<br><br>
	</h2 >
	<h4 style="color: white; text-align:center;">
	Prvi rezultati se vide neposredno nakon tretmana. Obim na tretiranom području je manji. 
	</h4>
</div>
</section>
<!-- Counter Area End -->


<!-- Video Section Start -->
<section id="video" class="video" style="background-color: #f8f8f8;">
	<div class="container">
		<div class="row">

			<div class="col-12">
				<h2>
					KOLIKO TRAJU POSTIGNUTI REZULTATI?
				</h2>
				<h5>
					

Postignuti rezultati su trajni, jer se uništene masne ćelije ne regenerišu, te tako ne postoji opasnost od „jo-jo“ efekta. Svakim narednim tretmanom telo se sve više i lepše reoblikuje, a takođe i još mesec dana nakon poslednjeg tretmana.
			
				</h5>
			</div>
		</div>
	</div>
</section>
<!-- Video Section End -->

<section id="serviceSection" class="video" >
	<div class="container">
		<div class="row">
			
			<div class="col-12">
				<h2>
					KO NE TREBA DA RADI TRETMAN ULTRAZVUČNE LIPOLIZE?
				</h2>
				<h5>
					
Tretmane ultrazvučne lipolize ne treba da rade trudnice, dojilje, osobe sa dijabetesom, osteroporozom,  bubrežnim obolenjima, tumorima i sl.
		
				</h5>
			</div>
		</div>
	</div>
</section>


<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-12">
				<div class="sectionTheading">
					<!--
					<img src="{{asset('/img/in.png')}}" alt="">
					-->
						<h4>
							Željene tretmane možete zakazati na broj: <br><strong><a href="tel:069/455-90-99">069/455-90-99</a></strong> 
						</h4>
				

				</div>
			</div>
		</div>
	</div>
</section>
@stop



