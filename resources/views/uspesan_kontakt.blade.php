@extends('layout')

@section('title')
Potvrda Kontakt - 
@stop

@section('scriptsTop')
<script>
        fbq('track', ' Izvrsili kontakt');
</script>
@stop

@section('sekcije')

<!-- SignUP Area Start -->
<section class="logRegForm">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12 col-md-12">
                <div class="contact_form_wrappre2">

                        <div class="inputArea">

                            
                            <div class="row justify-content-center">
                                <div class="col-12">
                                    <div class="sectionTheading">
                                        <h2>
                                            Uspešno ste nas kontaktrili
                                            <br>
                                        </h2>
                                        
                                        <img height="300" src="{{asset('img/uspesno.png')}}" alt="">
                                        
                                        <p>
                                            <strong>
                                            Potrudićemo se da Vam odgovorimo u najkraćem mogućem roku. <br>
                                            Essence of Beauty.
                                            </strong>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-row">
                                <div class="col-6">
                                    <a href="/" class="lostpass">Početna strana</a>
                                </div>
                                <div class="col-6">
                                    <a href="/prodavnica" class="loginnow">Prodavnica</a>
                                </div>
                            </div>
                            
                        
                            <div class="form-row">
                                
                            </div>
                            
                
                           
                        </div>
                        

                </div>
            </div>
        </div>
    </div>


</section>
<!-- SignUp Area End -->

@stop