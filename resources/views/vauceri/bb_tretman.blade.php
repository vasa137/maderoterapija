@extends('layout')

@section('title')
VAUČERI - BB Tretman - 
@stop

@section('scriptsTop')
<script>
    	fbq('track', 'VAUCER BB Tretman');
</script>
@stop

@section('sekcije')
<!-- banner Area Start -->
<section id="spabreadcrumb" class="spabreadcrumb">
	<div class="bcoverlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="Content">
					<h2>
						VAUČER - BB Glow Tretman
					</h2>
					<div class="links">
						<ul>
							<li>
								<a href="/">Naslovna</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a href="/vauceri">Vaučeri</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a class="active" href="/vaucer-bb-glow-tretman">BB Glow Tretman</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Welcome Area End -->



<!-- Video Section Start -->
<section id="video" class="video">
	<div class="container">
		<div class="row">
			<div class="col-lg-2"></div>
			<div class="col-lg-8">
				<div class="vImg">
					
					<img class="img-fluid" src="{{asset('img/vauceri/bb-glow-tretman-vaucer.jpg')}}" alt="">


				</div>
			</div>
	</div>
</section>
<!-- Video Section End -->


<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-10">
				<div class="sectionTheading">
					
						<h2>
							Kako da iskoristite vaučer?<br>
						</h2>
					<img src="{{asset('img/sectionSeparator.png')}}" alt="">
					
					<h5>
						Vaučer možete preuzeti <a onclick="fbq('track', 'VAUCER BB Tretman - PREUZET');" target="_blank" href="https://www.essenceofbeauty.rs/vaucer-bb-tretman.pdf" >OVDE</a> i doneti ga prilikom edukacije u štampanoj formi(na papiru) ili Vašem telefonu. Vaučer se odnosi na <strong>BB GLOW Tretmane do kraja novembra 2019.</strong>
						Ostale aktuelne vaučere možete pogledati <a href="/vauceri"> OVDE. </a>
					</h5>
					<BR><BR>
					<h2>
						<a onclick="fbq('track', 'VAUCER BB Tretman - PREUZET');" style="color: #bb6dac;" target="_blank" href="https://www.essenceofbeauty.rs/vaucer-bb-tretman.pdf">
							PREUZMITE VAUČER
						</a>
					</h2>
				</div>
			</div>
		</div>
	</div>
</section>

<section id="counter" class="counter">

<div class="container">
	<h2 style="color: white; text-align:center;">
	Do kada je moguće iskoristiti vaučer?
	<br><br>
	</h2>
	<div class="row">
		<div class=" col-md-12">
			<div class="c-box">
				<h5 style="color: white;">Vaučer važi do kraja novembra 2019. i odnosi se na BB GLOW Tretmane.</h5>
			</div>
		</div>
	</div>
</div>
</section>
<!-- Counter Area End -->

<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-12">
				<div class="sectionTheading">
					<!--
					<img src="{{asset('/img/in.png')}}" alt="">
					-->
						
						<br>
						<h4>
							Zakažite na 
							<strong><a href="tel:062/455200">062/455200</a></strong> ili putem maila: <strong><a href="info@essenceofbeauty.rs">info@essenceofbeauty.rs</a></strong>
						</h4>
				

				</div>
			</div>
		</div>
	</div>
</section>

@stop



