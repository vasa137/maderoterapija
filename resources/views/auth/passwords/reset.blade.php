@extends('layout')

@section('sekcije')
<!-- banner Area Start -->
<section id="spabreadcrumb" class="spabreadcrumb extrapadding">
    <div class="bcoverlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="Content">
                    <h2>
                        Zaboravljena lozinka
                    </h2>
                    <div class="links">
                        <ul>
                            <li>
                                <a href="/">Naslovna</a>
                            </li>
                            <li>
                                <span>/</span>
                            </li>
                            <li>
                                <a class="active" href="/password/reset">Zaboravljena lozinka</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Welcome Area End -->

<!-- SignIn Area Start -->
<section id="logRegForm" class="logRegForm">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-7 col-md-10">
                <div class="contact_form_wrappre2">
                    <div class="inputArea">
                        <form method="POST" action="{{ route('password.update') }}">
                            @csrf
                            <input type="hidden" name="token" value="{{ $token }}">
                            <div class="form-row">
                                <div class="col">
                                    <div class="input-group">
                                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" aria-describedby="Site" placeholder="Email" required autofocus>

                                        <div class="input-group-prepend">
                                                    <span class="input-group-text" id="Site">
                                                        <i class="fas fa-envelope"></i>
                                                    </span>
                                        </div>
                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>


                            </div>
                            <div class="form-row">
                                <div class="col">
                                    <div class="input-group">
                                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Lozinka" aria-describedby="url" required>
                                        <div class="input-group-prepend">
                                                    <span class="input-group-text" id="Site">
                                                        <i class="fas fa-key"></i>
                                                    </span>
                                        </div>
                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col">
                                    <div class="input-group">
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Potvrda nove lozinke" aria-describedby="url" required>
                                        <div class="input-group-prepend">
                                                <span class="input-group-text" id="url">
                                                    <i class="fas fa-key"></i>
                                                </span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="col-md-12">
                                    <button class="loginnow" type="submit">Resetuj lozinku</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
