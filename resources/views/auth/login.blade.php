@extends('layout')

@section('scriptsTop')
    <link href="{{asset('css/social-dugmici.css')}}" rel="stylesheet"/>
@endsection

@section('sekcije')
    <!-- banner Area Start -->
    <section id="spabreadcrumb" class="spabreadcrumb extrapadding">
        <div class="bcoverlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="Content">
                        <h2>
                            Prijavljivanje
                        </h2>
                        <div class="links">
                            <ul>
                                <li>
                                    <a href="/">Naslovna</a>
                                </li>
                                <li>
                                    <span>/</span>
                                </li>
                                <li>
                                    <a class="active" href="/login">Prijavljivanje</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Welcome Area End -->



    <!-- SignIn Area Start -->
    <section id="logRegForm" class="logRegForm">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-7 col-md-10">
                    <div class="contact_form_wrappre2">

                            <div class="inputArea">
                                <form method="POST" action="{{ route('login') }}">
                                    @csrf
                                    <div class="form-row">
                                        <div class="col-md-12">
                                            <a href="/register" class="lostpass">Nemate nalog?</a>
                                        </div>
                                    </div>
                                    <br/>
                                    <div class="form-row">
                                        <div class="col">
                                            <div class="input-group">
                                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" aria-describedby="Site" placeholder="Email" required autofocus>

                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="Site">
                                                        <i class="fas fa-envelope"></i>
                                                    </span>
                                                </div>
                                                @if ($errors->has('email'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                @endif
                                             </div>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col">
                                            <div class="input-group">
                                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Lozinka" aria-describedby="url" required>
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="Site">
                                                        <i class="fas fa-key"></i>
                                                    </span>
                                                </div>
                                                @if ($errors->has('password'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('password') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="col">
                                            <div class="input-group">
                                                <div class="form-check" style="padding-left:0px;">
                                                    <div class="checkbox-element account">
                                                        <div class="checkbox-wrapper">

                                                            <label class="checkboxContainer">
                                                                <span class="labelText">
                                                                        Zapamti me
                                                                </span>
                                                                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}/>
                                                                <span class="checkmark"></span>

                                                            </label>


                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-6">
                                            <button class="loginnow" type="submit">Prijava</button>
                                        </div>
                                        <div class="col-md-6">
                                            <a  href="{{ route('password.request') }}" class="lostpass" >Zaboravljena lozinka?</a>
                                        </div>
                                    </div>
                                </form>
                                    <div class="form-row">
                                        <div class="col text-center">
                                            <p class="loginwith" style="cursor:default;">Prijava putem društvenih mreža</p>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="col-md-6">
                                            <form action="/redirect/facebook" method="GET">
                                                <button type="submit" class="loginwithfb">
                                                    Prijavite se preko Facebook-a
                                                </button>
                                            </form>
                                        </div>
                                        <div class="col-md-6">
                                            <form action="/redirect/google" method="GET">
                                                <button type="submit" class="loginwithgoogle">
                                                    Prijavite se preko Gmail-a
                                                </button>
                                            </form>
                                        </div>
                                    </div>

                            </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- SignIn Area Start -->

@stop