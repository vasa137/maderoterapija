@extends('layout')

@section('title')
Masaža Svećom  Edukacija Srbija -
@stop


@section('scriptsTop')
<script>
    	fbq('track', 'EDUKACIJA - Masaža Svećom');
</script>
@stop

@section('sekcije')
<!-- banner Area Start -->
<section id="spabreadcrumb" class="spabreadcrumb">
	<div class="bcoverlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="Content">
					<h2>
						Masaža Svećom
					</h2>
					<div class="links">
						<ul>
							<li>
								<a href="/">Naslovna</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a href="/edukacija">Edukacija</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a class="active" href="/edukacija/masaza-svecom">Masaža Svećom</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Welcome Area End -->



<!-- Video Section Start -->
<section id="video" class="video">
	<div class="container">
		<div class="row">

			<div class="col-lg-6">
				<div class="vImg">
					<img class="img-fluid" src="{{asset('img/edukacije-maderoterapija-tela-lica-bb-glow-srbija/masaza-svecom.jpg')}}" alt="">
				</div>
			</div>
			
			<div class="col-lg-6">
				<h2>
					Masaža Svećom
				</h2>
				
				<h5>
					U masaži se primenjuju sveće od sojinog voska koje ne dovode do iritacije i pojave opekotina na koži. Masažom dolazi do zatezanja kože, poboljšanja cirkulacije, otklanjanja mrtvih ćelija i koža postaje glatka. Sveća se zapali i kada se delimično otopi, vosak se sipa po telu i primenjuje masaža.<br>
						
						
				</h5>
			<!--
				<div class="videoContent">
					<div class="icon">
						<a class="venobox" data-vbtype="video" data-autoplay="true" href="http://vimeo.com/75976293">
							<i class="fas fa-play"></i>
						</a>
					</div>
					<div class="content d-flex">
						<div class="text align-self-center">
							<h5>Video</h5>
								<p>Play</p>
						</div>
					</div>
				</div>
			-->
			</div>
			
		</div>
	</div>
</section>
<!-- Video Section End -->



<!--
<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-10">
				<div class="sectionTheading">
					
						<h2>
							
							Termin sledeće grupne edukacije u Beogradu:<br>
							20. april 2019. godine
							
							Anticelulit masaža
						</h2>
					
					<img src="{{asset('img/sectionSeparator.png')}}" alt="">
					
					<h5>
						Kod žena je utemeljeno mišljenje da je ručna anticelulit masaža najefikasniji način uklanjanja celulita. Cilj naše edukacije je da vas naučimo kako da pravilno sprovodite anticelulit masažu kako bi se telo klijenta potpuno oslobodilo tzv narandžine kore.
					</h5>
				</div>
			</div>
		</div>
	</div>
</section>
-->
<!-- Counter Area Start -->
<section id="counter" class="counter">

<div class="container">
	<h2 style="color: white; text-align:center;">
	Tematske jedinice koje obrađujemo:
	<br><br>
	</h2>
	<div class="row">
		<h5 style="color: white;">
			<strong>2 ČASA </strong>– teorijska predavanja – Uvod u masažu (pojam, indikacije i kontraindikacij, Termoterapija (opšti pojmovi, indikacije, kontraindikacije, sredstva u termoterapiji), Postupak izrade sveća
			<br>

			<strong>4 ČASA </strong>– praktična nastava –  Masažni hvatovi
		</h5>
		
	</div>
</div>
</section>
<!-- Counter Area End -->

<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-10">
				<div class="sectionTheading">
						<h2>
							Trajanje edukacije:

						</h2>
					
					<h5>
						Ukupan fond časova edukacije je 6. 
						<br><br>
					</h5>

					<img src="{{asset('img/sectionSeparator.png')}}" alt="">

					<h2>
						<br>
							Potrebna dokumentacija za upis:
						</h2>
					
					<h5>
						Prilikom upisa potrebno je da nam dostavite: fotokopiju prethodno stečene diplome redovnog obrazovanja i fotokopiju lične karte
					</h5>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Pricing Plan Start -->
<section id="pricePlan" class="pricePlan">
	<div class="container">
			<div class="row justify-content-center">
					<div class="col-md-10 col-lg-8">
				<div class="sectionTheading">
					<h2>
					Cena edukacije 
					</h2>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4">
			</div>
			<div class="col-lg-4">
				<div class="p_box">
					
					<div class="title">
						<img class="img-fluid" src="{{asset('img/shape1.png')}}" alt="">
						<i class="pe-7s-diamond"></i>
					</div>
					<div class="doller">
					<span>18.000rsd</span>
					</div>
					<div class="list">
						<ul>
							<li>
								<strong>Edukacija</strong>
							</li>
							<li>
								<strong>Priručnik za učenje</strong>
							</li>
							<li>
								<strong>Plaćanje u 2 rate</strong>
							</li>
							
						</ul>
					</div>
					<a class="phurchaseBtn" href="/kontakt">
						Zakažite termin	
					</a>
				</div>
			</div>
			
		</div>
	</div>
</section> 
<!-- Pricing Plan End -->
<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-12">
				<div class="sectionTheading">
					<!--
					<img src="{{asset('/img/in.png')}}" alt="">
					-->
						<h4>
							Prijavite se odmah na našu edukaciju koja je apsolutno jedinstvena u Srbiji po kvalitetu rada sa polaznicima i nivou stečenog znanja na broj: <strong><a href="tel:062/455200">062/455200</a></strong> ili putem maila: <strong>info@essenceofbeauty.rs</strong>
						</h4>
				

				</div>
			</div>
		</div>
	</div>
</section>
@stop



