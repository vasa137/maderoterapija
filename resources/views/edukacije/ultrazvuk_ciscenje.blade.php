@extends('layout')

@section('title')
Aparaturno čišćenje lica ultrazvučnom špatulom - 
@stop

@section('scriptsTop')
<script>
    	fbq('track', 'EDUKACIJA - Aparaturno čišćenje lica ultrazvučnom špatulom');
</script>
@stop

@section('sekcije')

<section id="spabreadcrumb" class="spabreadcrumb">
	<div class="bcoverlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="Content">
					<h2>
						Aparaturno čišćenje lica ultrazvučnom špatulom
					</h2>
					<div class="links">
						<ul>
							<li>
								<a href="/">Naslovna</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a href="/edukacija">Edukacija</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a class="active" href="/edukacija/edukacija/aparaturno-ciscenje-lica-ultrazvucnom-spatulom">Aparaturno čišćenje lica ultrazvučnom špatulom</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
 
<!--
<section id="" class="">
	<img width="100%" src="{{asset('img/bb-glow-srbija-promo.jpg')}}" alt="">

	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-12">
				<div class="sectionTheading">
					
						<h2><br>
							 POPUST 100€ ZA BB GLOW EDUKACIJE<br>
						</h2>
					
					<h3>
							ZA UPLATE DO 10.8.
					</h3>
					<img src="{{asset('img/sectionSeparator.png')}}" alt="">
					
					<h3>
						Jedinstveni koncept edukacije u Srbiji.<br>
						Svaki polaznik samostalno radi tretman uz nadzor.<br>
						<strong>Svaki polaznik dobija tretman na poklon!</strong>
					</h3>
					<br>
					<a class="phurchaseBtn" href="/kontakt">
                        Zakažite termin 
                    </a>
				</div>
			</div>
		</div>
	</div>
</section>
-->

<!-- Video Section Start -->
<section id="video" class="video">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="vImg">
					<img class="img-fluid" src="{{asset('img/edukacije-maderoterapija-tela-lica-bb-glow-srbija/ciscenje-lica-ultrazvucnom-spatulom.jpg')}}" alt="">
				</div>
			</div>
			<div class="col-lg-6">
				<h2>
					Aparaturno čišćenje lica ultrazvučnom špatulom 
				</h2>
				<h5>
					Edukacija za profesionalno aparaturno čišćenje lica ultrazvučnom špatulom već od 12.000rsd!
Ultrazvučno čišćenje lica je bezbolan, bezbedan,  i planetarno popularan tretman, koji se može uraditi kao poseban tretman ili kao uvodni tretman pre zahtevnijih kozmetičkih procedura. Ultrazvučnom špatulom se sa kože lica skidaju mrtve ćelije, višak masnoće i čiste pore, što je apsolutno osnovni korak svake nege. Dejstvom ultrazvuka poboljšava se mikrocirkulacija i oksigenacija kože nakon čega je lice čisto i rehidrirano.
<br><br>
Termine za individualne obuke možete dogovoriti pozivom na broj: 062 455 200
						
						
				</h5>
				<!--
				<div class="videoContent">
					<div class="icon">
						<a class="venobox" data-vbtype="video" data-autoplay="true" href="http://vimeo.com/75976293">
							<i class="fas fa-play"></i>
						</a>
					</div>
					
					<div class="content d-flex">
						<div class="text align-self-center">
							<h5>intro</h5>
								<h5>Play Video</h5>
						</div>
					</div>
					
				</div>
				-->
			</div>
		</div>
	</div>
</section>
<!-- Video Section End -->


<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-10">
				<div class="sectionTheading">
					
						<h2>
							EDUKUJTE SE ZA PROFESIONALNU UPOTREBU ULTRAZVUČNE ŠPATULE
						</h2>

					<img src="{{asset('img/sectionSeparator.png')}}" alt="">
					
					<h5>
						Ultrazvučna špatula je hidroabrazijski uređaj zasnovan je na fizičkom fenomenu brzog ultrazvučnog prskanja vodom. Dolazeći u kontakt sa vlažnom površinom kože, špatula emituje ultrazvučne talase kako bi kapljice vode mogle da odvoje mrtve ćelije kože od površine kože. Osim toga, <strong>ultrazvuk radi i finu mikromasažu kože, kojom se obezbedjuje optimalna prokrvljenost i saturacija kiseonikom te pobudjuju mnogi uspavani i usporeni metabolički i regenerativni procesi u koži.</strong>
					</h5>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Counter Area Start -->

<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-10">
				<div class="sectionTheading">
						<h2>
							Trajanje edukacije:
						</h2>
					
					<h5>
						Ukupan fond časova edukacije je 4. Nastava se odvija tokom jednog dana sa početkom od 10h. U pauzi je obezbedjeno osveženje.
						<br><br>
					</h5>

					<img src="{{asset('img/sectionSeparator.png')}}" alt="">

					<h2>
						<br>
							Potrebna dokumentacija za upis:
						</h2>
					
					<h5>
						Prilikom upisa potrebno je da nam dostavite: fotokopiju prethodno stečene diplome redovnog obrazovanja i fotokopiju lične karte.
					</h5>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Pricing Plan Start -->
<section id="pricePlan" class="pricePlan">
	<div class="container">
			<div class="row justify-content-center">
					<div class="col-md-10 col-lg-8">
				<div class="sectionTheading">
					<h2>
					Cena edukacije 
					</h2>
					<img src="{{asset('img/sectionSeparatorw.png')}}" alt="">
					
				</div>
			</div>
		</div>
		<div class="row">
			
        	<div class="col-lg-2"></div>
            <div class="col-lg-4">
                <div class="p_box">
                    <div class="doller">
                    <span>start</span>
                    </div>
                    <div class="title">
                        <img class="img-fluid" src="{{asset('img/shape1.png')}}" alt="">
                        <i class="pe-7s-diamond"></i>
                    </div>
                    <div class="doller">
                    	<!--<span><del>590 €</del></span>-->
                    <span>12 000 rsd</span>
                    </div>

                    <div class="list">
                        <ul>
                            
                            <li>
                                <strong>Edukacija</strong>
                            </li>
                            <li>
                                <strong>Priručnik za učenje </strong>
                            </li>
                            <li>
                                <strong>Sertifikat</strong>
                            </li>
                            
                            <li>
                                <strong><br></strong>
                            </li>
                        </ul>
                    </div>
                    <a class="phurchaseBtn" href="/kontakt">
                        Zakažite termin 
                    </a>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="p_box">
                    <div class="doller">
                    <span>smart</span>
                    </div>
                    <div class="title">
                        <img class="img-fluid" src="{{asset('img/shape1.png')}}" alt="">
                        <i class="pe-7s-magic-wand"></i>
                    </div>
                    <div class="doller">
                    	<!--<span><del>690 €</del></span>-->
                    <span>19 000 rsd</span>
                    </div>

                    <div class="list">
                        <ul>
                             <li>
                                <strong>Edukacija</strong>
                            </li>
                            <li>
                                <strong>Priručnik za učenje </strong>
                            </li>
                            <li>
                                <strong>Sertifikat</strong>
                            </li>
                            
                            <li>
                                <strong>Ultrazvučna špatula</strong>
                            </li>
                        </ul>



                    </div>
                    <a class="phurchaseBtn" href="/kontakt">
                        Zakažite termin 
                    </a>
                </div>
            </div>
            
        </div>

        <div class="row justify-content-center">	
			<h5>
				<br>
				* Za vreme trajanja epidemije Covid 19 virusa cena individualne edukacije je ista kao cena grupne.
			</h5>
		</div>
	</div>
</section> 
<!-- Pricing Plan End -->
<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-12">
				<div class="sectionTheading">
					<!--
					<img src="{{asset('/img/in.png')}}" alt="">
					-->
						<h4>
							Prijavite se odmah na našu edukaciju koja je apsolutno jedinstvena u Srbiji po kvalitetu rada sa polaznicima i nivou stečenog znanja na broj: <strong><a href="tel:062/455200">062/455200</a></strong> ili putem maila: <strong>info@essenceofbeauty.rs</strong>
						</h4>
				

				</div>
			</div>
		</div>
	</div>
</section>
@stop



