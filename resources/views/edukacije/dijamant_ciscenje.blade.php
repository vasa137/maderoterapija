@extends('layout')

@section('title')
Aparaturno čišćenje lica dijamantskom mikrodermoabrazijom - 
@stop

@section('scriptsTop')
<script>
    	fbq('track', 'EDUKACIJA - Aparaturno čišćenje lica dijamantskom mikrodermoabrazijom');
</script>
@stop

@section('sekcije')

<section id="spabreadcrumb" class="spabreadcrumb">
	<div class="bcoverlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="Content">
					<h2>
						Aparaturno čišćenje lica dijamantskom mikrodermoabrazijom
					</h2>
					<div class="links">
						<ul>
							<li>
								<a href="/">Naslovna</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a href="/edukacija">Edukacija</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a class="active" href="/edukacija/edukacija/aparaturno-ciscenje-lica-dijamantskom-mikrodermoabrazijom">Aparaturno čišćenje lica dijamantskom mikrodermoabrazijom</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
 
<!--
<section id="" class="">
	<img width="100%" src="{{asset('img/bb-glow-srbija-promo.jpg')}}" alt="">

	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-12">
				<div class="sectionTheading">
					
						<h2><br>
							 POPUST 100€ ZA BB GLOW EDUKACIJE<br>
						</h2>
					
					<h3>
							ZA UPLATE DO 10.8.
					</h3>
					<img src="{{asset('img/sectionSeparator.png')}}" alt="">
					
					<h3>
						Jedinstveni koncept edukacije u Srbiji.<br>
						Svaki polaznik samostalno radi tretman uz nadzor.<br>
						<strong>Svaki polaznik dobija tretman na poklon!</strong>
					</h3>
					<br>
					<a class="phurchaseBtn" href="/kontakt">
                        Zakažite termin 
                    </a>
				</div>
			</div>
		</div>
	</div>
</section>
-->

<!-- Video Section Start -->
<section id="video" class="video">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="vImg">
					<img class="img-fluid" src="{{asset('img/edukacije-maderoterapija-tela-lica-bb-glow-srbija/mikrodermoabrazija.jpg')}}" alt="">
				</div>
			</div>
			<div class="col-lg-6">
				<h2>
					Aparaturno čišćenje lica dijamantskom mikrodermoabrazijom
				</h2>
				<h5>
					Edukacija za profesionalno aparaturno čišćenje lica aparatom za dijamantsku mikrodermoabraziju već od 12.000rsd!



						
						
				</h5>
			
			</div>
			<div class="col-lg-12">
				<h5>
					<strong>Mikrodermoabrazija</strong> predstavlja piling kože dijamantskim kristalima, pri čemu se istovremeno izvodi i vakuum masaža što sveukupno doprinosi boljem čisćenju kože lica i poboljšanju cirkulacije. Blagim i kontrolisanim uklanjanjem povšinskog sloja, koža je stimulisana da proizvodi novi kolagen pa kao rezultat dobijamo obnavljanje tonusa i poboljšanje teksture kože.
					<br><br>
					Ovim tretmanom se podstiče mikrocirkulacija, čime se omogućava bolja ćelijska ishrana, brže i potpunije upijanje aktivnih materija koje treba da dopru do kolagena i elastina, i izazovu produkciju novih ćelija.
					<br><br>
Tokom tretmana izumrli sloj kože se otklanja, dok vakum istovremeno stimuliše fibroblaste na produkciju novih kolagenih i elastinskih vlakana.
<br><br>
Mikrodermoabrazija održava mladalački i svež izgled kože, ublažavaju se bore, staračke pege, ožiljke, pigmentacije uzrokovane suncem, akne i ožiljke od njih, smanjuje proširene pore, reguliše pojačano lučenje sebuma.
<br><br>

Termine za individualne obuke možete dogovoriti pozivom na broj: 062 455 200
				</h5>
			</div>
		</div>
	</div>
</section>
<!-- Video Section End -->


<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-10">
				<div class="sectionTheading">
					
						<h2>
							EDUKUJTE SE ZA PROFESIONALNU UPOTREBU MIKRODERMOABRAZIJE
						</h2>

					<img src="{{asset('img/sectionSeparator.png')}}" alt="">
					
					<h5>
						Mikrodermoabrazija predstavlja kontrolisano stanjivanje zadebljalog i izumrlog sloja kože dijamantskim mikrokristalima, koji se nalaze na kraju sonde aparata i  uklanjaju nedostatke kože, u vidu posledica starenja, sunčanja, akni. Ovim aparatom uspešno se tretiraju: proširene pore, hiperpigmentacije, pliće bore, ožiljci od akni i druge nepravilnosti u reljefu kože. Nakon tretmana koža je spremna da apsorbuje aktivne komponente i bolje je prokrvljena
					</h5>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Counter Area Start -->

<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-10">
				<div class="sectionTheading">
						<h2>
							Trajanje edukacije:
						</h2>
					
					<h5>
						Ukupan fond časova edukacije je 4. Nastava se odvija tokom jednog dana sa početkom od 10h. U pauzi je obezbedjeno osveženje.
						<br><br>
					</h5>

					<img src="{{asset('img/sectionSeparator.png')}}" alt="">

					<h2>
						<br>
							Potrebna dokumentacija za upis:
						</h2>
					
					<h5>
						Prilikom upisa potrebno je da nam dostavite: fotokopiju prethodno stečene diplome redovnog obrazovanja i fotokopiju lične karte.
					</h5>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Pricing Plan Start -->
<section id="pricePlan" class="pricePlan">
	<div class="container">
			<div class="row justify-content-center">
					<div class="col-md-10 col-lg-8">
				<div class="sectionTheading">
					<h2>
					Cena edukacije 
					</h2>
					<img src="{{asset('img/sectionSeparatorw.png')}}" alt="">
					
				</div>
			</div>
		</div>
		<div class="row">
			
        	<div class="col-lg-2"></div>
            <div class="col-lg-4">
                <div class="p_box">
                    <div class="doller">
                    <span>start</span>
                    </div>
                    <div class="title">
                        <img class="img-fluid" src="{{asset('img/shape1.png')}}" alt="">
                        <i class="pe-7s-diamond"></i>
                    </div>
                    <div class="doller">
                    	<!--<span><del>590 €</del></span>-->
                    <span>12 000 rsd</span>
                    </div>

                    <div class="list">
                        <ul>
                            
                            <li>
                                <strong>Edukacija</strong>
                            </li>
                            <li>
                                <strong>Priručnik za učenje </strong>
                            </li>
                            <li>
                                <strong>Sertifikat</strong>
                            </li>
                            
                            <li>
                                <strong><br></strong>
                            </li>
                        </ul>
                    </div>
                    <a class="phurchaseBtn" href="/kontakt">
                        Zakažite termin 
                    </a>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="p_box">
                    <div class="doller">
                    <span>smart</span>
                    </div>
                    <div class="title">
                        <img class="img-fluid" src="{{asset('img/shape1.png')}}" alt="">
                        <i class="pe-7s-magic-wand"></i>
                    </div>
                    <div class="doller">
                    	<!--<span><del>690 €</del></span>-->
                    <span>39 000 rsd</span>
                    </div>

                    <div class="list">
                        <ul>
                             <li>
                                <strong>Edukacija</strong>
                            </li>
                            <li>
                                <strong>Priručnik za učenje </strong>
                            </li>
                            <li>
                                <strong>Sertifikat</strong>
                            </li>
                            
                            <li>
                                <strong>Aparat dijamantska mikrodermoabrazija</strong>
                            </li>
                        </ul>



                    </div>
                    <a class="phurchaseBtn" href="/kontakt">
                        Zakažite termin 
                    </a>
                </div>
            </div>
            
        </div>

        <div class="row justify-content-center">	
			<h5>
				<br>
				* Za vreme trajanja epidemije Covid 19 virusa cena individualne edukacije je ista kao cena grupne.
			</h5>
		</div>
	</div>
</section> 
<!-- Pricing Plan End -->
<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-12">
				<div class="sectionTheading">
					<!--
					<img src="{{asset('/img/in.png')}}" alt="">
					-->
						<h4>
							Prijavite se odmah na našu edukaciju koja je apsolutno jedinstvena u Srbiji po kvalitetu rada sa polaznicima i nivou stečenog znanja na broj: <strong><a href="tel:062/455200">062/455200</a></strong> ili putem maila: <strong>info@essenceofbeauty.rs</strong>
						</h4>
				

				</div>
			</div>
		</div>
	</div>
</section>
@stop



