@extends('layout')

@section('title')
Brazilska maderoterapija Totally Fit by Essence of Beauty
@stop

@section('scriptsTop')
<script>
    	fbq('track', 'EDUKACIJA - Maderoterapija tela');
</script>
@stop

@section('sekcije')
<!-- banner Area Start -->
<section id="spabreadcrumb" class="spabreadcrumb">
	<div class="bcoverlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="Content">
					<h2>
						Brazilska maderoterapija Totally Fit by Essence of Beauty
					</h2>
					<div class="links">
						<ul>
							<li>
								<a href="/">Naslovna</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a href="/edukacija">Edukacija</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a class="active" href="/edukacija/brazilska-maderoterapija">Brazilska maderoterapija Totally Fit by Essence of Beauty</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Welcome Area End -->



<!-- Video Section Start -->
<section id="video" class="video">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="vImg">
					<img class="img-fluid" src="{{asset('img/edukacije-maderoterapija-tela-lica-bb-glow-srbija/maderoterapija-tela-obuka.jpg')}}" alt="">
				</div>
			</div>
			<div class="col-lg-6">
				<h2>
					Brazilska maderoterapija Totally Fit by Essence of Beauty
				</h2>
				<h5>
					Brazilska maderoterapija Totally Fit by Essence of Beauty na kojoj je tim naših strukovnih fizioterapeuta i iskusnih maderoterapeuta vredno radio poslednje 2 godine vrtoglavom brzinom stiče sve veću i veću popularnost.
<br>
Brazilska maderoterapija Totally Fit by Essence of Beauty je potpuno prirodan tretman i predstavlja kombinaciju posebne tehnike ručne masaže i masaže specijalno dizajniranim vakuum zvonima, koja obezbeđuju efikasnu limfnu drenažu.
<br>
Brazilska maderoterapija Totally Fit by Essence of Beauty daje potpuno fenomenalne rezultate koji se vrlo brzo primećuju na delovima tela napadnutim celulitom. 
				</h5>
				<!--
				<div class="videoContent">
					<div class="icon">
						<a class="venobox" data-vbtype="video" data-autoplay="true" href="http://vimeo.com/75976293">
							<i class="fas fa-play"></i>
						</a>
					</div>
					<div class="content d-flex">
						<div class="text align-self-center">
							<h5>Video</h5>
								<h5>Play</h5>
						</div>
					</div>
				</div>
				-->
			</div>
			<div class="col-lg-12">
				<h5>
					<br>
					
				</h5>
				
			</div>
		</div>
	</div>
</section>
<!-- Video Section End -->




<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-10">
				<div class="sectionTheading">
					
						<h2>
							NAUČITE I VI BRAZILSKU TOTALLY FIT MADEROTERAPIJU!
						</h2>
					
					<img src="{{asset('img/sectionSeparator.png')}}" alt="">
					
					<h5>
						<strong>Brazilska Totally Fit maderoterapija by Essence of Beauty je naša autorska metoda uklanjanja celulita koja kombinuje posebnu tehniku ručne masaže uz upotrebu specijalno dizajniranih vakuum zvona. Brazilska Totally Fit maderoterapija by Essence of Beauty </strong> daje fenomenalne rezultate koji se veoma brzo primećuju na delovima tela napadnutim celulitom. I ovde se kao i kod drugih metoda uklanjanja celulita tretmani rade u seriji od 10 tretmana.<br>
<strong>Brazilska maderoterapija Totally Fit je posebno efikasna za smanjenje obima stomaka, eliminaciju viška masnih naslaga i celulita na bokovima, definisanje i zatezanje nogu butina i nogu. 
<br><br>
Brazilska vakum maderoterapija polako dostiže sve veću popularnost u svetu zato što daje nekoliko puta brže rezultate u odnosu na klasičnu anticelulitnu maderoterapiju.</strong>
					</h5>
				</div>
			</div>
		</div>
	</div>
</section>





<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-10">
				<div class="sectionTheading">
						<h2>
							Trajanje edukacije:
						</h2>
					
					<h5>
						Ukupan fond časova edukacije je 10. Nastava se odvija tokom jednog dana sa početkom od 10h. U pauzi je obezbedjeno osveženje.
						<br><br>
					</h5>

					<img src="{{asset('img/sectionSeparator.png')}}" alt="">

					<h2>
						<br>
							Potrebna dokumentacija za upis:
						</h2>
					
					<h5>
						Prilikom upisa potrebno je da nam dostavite: fotokopiju prethodno stečene diplome redovnog obrazovanja i fotokopiju lične karte.
					</h5>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Pricing Plan Start -->
<section id="pricePlan" class="pricePlan">
	<div class="container">
			<div class="row justify-content-center">
					<div class="col-md-10 col-lg-8">
				<div class="sectionTheading">
					<h2>
					Cena edukacije 
					</h2>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-2">
			</div>
			<div class="col-lg-4">
				<div class="p_box">
					<div class="doller">
					<span>START</span>
					</div>
					<div class="title">
						<img class="img-fluid" src="{{asset('img/shape1.png')}}" alt="">
						<i class="pe-7s-diamond"></i>
					</div>
					<div class="doller">
						
						<span><del>48.000rsd</del></span>
					<span>36.000rsd</span>
					</div>
					<div class="list">
						<ul>
							<li>
								<strong>Edukacija</strong>
							</li>
							<li>
								<strong>Priručnik za učenje</strong>
							</li>
							<li>
								<strong>Sertifikat</strong>
							</li>
							<li>
								<strong><BR></strong>
							</li>
							<li>
								<strong><BR></strong>
							</li>
						</ul>
					</div>
					<a class="phurchaseBtn" href="/kontakt">
						Zakažite termin	
					</a>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="p_box">
					<div class="doller">
					<span>SMART</span>
					</div>
					<div class="title">
						<img class="img-fluid" src="{{asset('img/shape1.png')}}" alt="">
						<i class="pe-7s-magic-wand"></i>
					</div>
					<div class="doller">
						<span><del>60.000rsd</del></span>
					<span>48.000rsd</span>
					</div>
					<div class="list">
						<ul>
							<li>
								<strong>Edukacija</strong>
							</li>
							<li>
								<strong>Priručnik za učenje</strong>
							</li>
							<li>
								<strong>Sertifikat</strong>
							</li>
							<li>
								<strong>Profesionalni set od 6 oklagija</strong>
							</li>
							<li>
								<strong>500ml ulja za maderoterapiju</strong>
							</li>							


						</ul>
					</div>
					<a class="phurchaseBtn" href="/kontakt">
						Zakažite termin	
					</a>
				</div>
			</div>
			
		</div>
		<div class="row justify-content-center">	
			<h5>
				<br>
				*Na individualnu obuku je potrebno dovesti osobu koja će biti model za vežbu.<br>
				Cena za individualnu edukaciju je 30% veća.
			</h5>
		</div>

		


	</div>
</section> 
<!-- Pricing Plan End -->
<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-12">
				<div class="sectionTheading">
					<!--
					<img src="{{asset('/img/in.png')}}" alt="">
					-->
						<h4>
							Prijavite se odmah na našu edukaciju koja je apsolutno jedinstvena u Srbiji po kvalitetu rada sa polaznicima i nivou stečenog znanja na broj: <strong><a href="tel:062/455200">062/455200</a></strong> ili putem maila: <strong>info@essenceofbeauty.rs</strong>
						</h4>
				

				</div>
			</div>
		</div>
	</div>
</section>
@stop



