@extends('layout')

@section('title')
Hijaluron pen Edukacija Srbija Beograd - Hijaluron pen
@stop

@section('scriptsTop')
<script>
    	fbq('track', 'EDUKACIJA - Hijaluron pen');
</script>
@stop

@section('sekcije')
<!-- banner Area Start -->
<section id="spabreadcrumb" class="spabreadcrumb">
	<div class="bcoverlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="Content">
					<h2>
						Hijaluron pen
					</h2>
					<div class="links">
						<ul>
							<li>
								<a href="/">Naslovna</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a href="/edukacija">Edukacija</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a class="active" href="/edukacija/hijaluron-pen">Hijaluron pen</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Welcome Area End -->



<!-- Video Section Start -->
<section id="video" class="video">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="vImg">
					<img class="img-fluid" src="{{asset('img/edukacije-maderoterapija-tela-lica-bb-glow-srbija/hijaluron-pen.jpg')}}" alt="">
				</div>
			</div>
			<div class="col-lg-6">
				<h2>
					Broj polaznika u grupi je mali zbog kvaliteta rada!
				</h2>
				<h5>
					Praktični deo edukacije se radi na modelima. <br>
					Edukacija se sastoji iz teorijskog dela i praktičnog dela.<br>
					Teorijski deo: Koža, Anatomija lica, Područje primene hijaluron pena, indikacije,kontraindikacije<br>


				</h5>
				<!--
				<div class="videoContent">
					<div class="icon">
						<a class="venobox" data-vbtype="video" data-autoplay="true" href="http://vimeo.com/75976293">
							<i class="fas fa-play"></i>
						</a>
					</div>
					<div class="content d-flex">
						<div class="text align-self-center">
							<h5>Video</h5>
								<h5>Play</h5>
						</div>
					</div>
				</div>
				-->
			</div>
			<div class="col-lg-12">
				<h5>
					<br>
					
				</h5>
				
			</div>
		</div>
	</div>
</section>
<!-- Video Section End -->




<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-12">
				<div class="sectionTheading">
					
						<h2>
							Savladajte novu revolucionarnu tehniku apliciranja hijalurona bez igle pod pritiskom! 
						</h2>
					
					<img src="{{asset('img/sectionSeparator.png')}}" alt="">
					
					<h5>
						Hijaluron penom možete ubrizgati hijaluronsku kiselinu u kožu bez ikakvih iglica, pri čemu se hijaluronska kiselina ravnomerno raspoređuje bez grudvica.<br>
						Ova metoda je nežna i gotovo bezbolna i ponovo povezuje epidermalne slojeve međusobno te se može vrlo precizno pod pritiskom dozirati, štiti tkivo i hijaluronska kiselina je ravnomjernije raspoređena nego kod injekcija iglom.<br>
						Rezultat je vidljiv odmah nakon aplikacije te se hijaluron ravnomerno raspoređuje u usnicama ili popunjavanju bora. <br>
						Zadržava se vlaga i podstiče stvaranje novog kolagena. <br>

					</h5>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Counter Area Start -->
<section id="counter" class="counter">

<div class="container">
	<h2 style="color: white; text-align:center;">
	Tematske jedinice koje obrađujemo:
	<br><br>
	</h2>

	<div class="row">
		<h5 style="color: white;">
			<strong>3 ČASA </strong>- teorijska predavanja: Koža, Anatomija lica, Područje primene hijaluron pena, Indikacije, Kontraindikacije
			<br>

			<strong>7 ČASOVA </strong>- praktična nastava - Ovaj deo edukacije se radi na modelima.
		</h5>
		
	</div>
</div>
</section>
<!-- Counter Area End -->

<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-12">
				<div class="sectionTheading">
						<h2>
							Prednosti Hijaluron pena:
						</h2>
					
					<h5>
						- Bez uboda igle <br>
						- Neinvazivna tehnika<br>
						- Neoperativna tehnika<br>
						- Ne ostavlja ožiljke<br>
						- Ne boli<br>
						- Kratko vreme tretmana<br>
					</h5>

					<img src="{{asset('img/sectionSeparator.png')}}" alt="">

					<h2>
						<br>
							Ovaj tretman je za sve one koji žele:
						</h2>
					
					<h5>
						
						- Prirodan izgled uz neinvazivu tehniku<br>
						- Bezbolno pomlađivanje i osveženje<br>
						- Povećanje volumena usana<br>
						- Punjenje bora<br>

					</h5>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Pricing Plan Start -->
<section id="pricePlan" class="pricePlan">
	<div class="container">
			<div class="row justify-content-center">
					<div class="col-md-10 col-lg-8">
				<div class="sectionTheading">
					<h2>
					Cena edukacije 
					</h2>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-2">
			</div>
			<div class="col-lg-4">
				<div class="p_box">
					<div class="doller">
					<span>START</span>
					</div>
					<div class="title">
						<img class="img-fluid" src="{{asset('img/shape1.png')}}" alt="">
						<i class="pe-7s-diamond"></i>
					</div>
					<div class="doller">
					<span>490 €</span>
					</div>
					<div class="list">
						<ul>
							<li>
								<strong>Edukacija</strong>
							</li>
							<li>
								<strong>Priručnik za učenje</strong>
							</li>
							<li>
								<strong>Sertifikat</strong>
							</li>
							<li>
								<strong><BR></strong>
							</li>
							<li>
								<strong><BR></strong>
							</li>
							<li>
								<strong><BR><BR></strong>
							</li>
						</ul>
					</div>
					<a class="phurchaseBtn" href="/kontakt">
						Zakažite termin	
					</a>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="p_box">
					<div class="doller">
					<span>SMART</span>
					</div>
					<div class="title">
						<img class="img-fluid" src="{{asset('img/shape1.png')}}" alt="">
						<i class="pe-7s-magic-wand"></i>
					</div>
					<div class="doller">
						<span>590 €</span>
					</div>
					<div class="list">
						<ul>
							<li>
								<strong>Edukacija</strong>
							</li>
							<li>
								<strong>Priručnik za učenje</strong>
							</li>
							<li>
								<strong>Sertifikat</strong>
							</li>
							<li>
								<strong>Hijaluron pen aparat</strong>
							</li>
							<li>
								<strong>5 kertridža za hijaluron<br> pen aparat</strong>
							</li>							
							<li>
								<strong>10 ml hijalurona </strong>
							</li>	

						</ul>
					</div>
					<a class="phurchaseBtn" href="/kontakt">
						Zakažite termin	
					</a>
				</div>
			</div>
			
		</div>
		
		<div class="row justify-content-center">	
			<h5>
				<br>
				*
				Cena za individualnu edukaciju je za 100 eura veća u odnosu na redovnu cenu.
			</h5>
		</div>
		
		


	</div>
</section> 
<!-- Pricing Plan End -->
<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-12">
				<div class="sectionTheading">
					<!--
					<img src="{{asset('/img/in.png')}}" alt="">
					-->
						<h4>
							Prijavite se odmah na našu edukaciju koja je apsolutno jedinstvena u Srbiji po kvalitetu rada sa polaznicima i nivou stečenog znanja na broj: <strong><a href="tel:062/455200">062/455200</a></strong> ili putem maila: <strong>info@essenceofbeauty.rs</strong>
						</h4>
				

				</div>
			</div>
		</div>
	</div>
</section>
@stop



