@extends('layout')

@section('title')
Maderoterapija Tela Edukacija Srbija Beograd - Oklagije za maderoterapiju
@stop

@section('scriptsTop')
<script>
    	fbq('track', 'EDUKACIJA - Maderoterapija tela');
</script>
@stop

@section('sekcije')
<!-- banner Area Start -->
<section id="spabreadcrumb" class="spabreadcrumb">
	<div class="bcoverlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="Content">
					<h2>
						Maderoterapija
					</h2>
					<div class="links">
						<ul>
							<li>
								<a href="/">Naslovna</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a href="/edukacija">Edukacija</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a class="active" href="/edukacija/maderoterapija">Maderoterapija</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Welcome Area End -->



<!-- Video Section Start -->
<section id="video" class="video">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="vImg">
					<img class="img-fluid" src="{{asset('img/edukacije-maderoterapija-tela-lica-bb-glow-srbija/maderoterapija-tela.jpg')}}" alt="">
				</div>
			</div>
			<div class="col-lg-6">
				<h2>
					Maderoterapija
				</h2>
				<h5>
					Maderoterapija je potpuno prirodan tretman protiv celulita pri kome se masaža izvodi pomoću specijalno dizajniranih oklagija različitog oblika, koje obezbeđuju intenzivan pritisak na celulit, poboljšava cirkulaciju i izbacivanje viška tečnosti i toksina iz organizma.
				</h5>
				<!--
				<div class="videoContent">
					<div class="icon">
						<a class="venobox" data-vbtype="video" data-autoplay="true" href="http://vimeo.com/75976293">
							<i class="fas fa-play"></i>
						</a>
					</div>
					<div class="content d-flex">
						<div class="text align-self-center">
							<h5>Video</h5>
								<h5>Play</h5>
						</div>
					</div>
				</div>
				-->
			</div>
			<div class="col-lg-12">
				<h5>
					<br>
					
				</h5>
				
			</div>
		</div>
	</div>
</section>
<!-- Video Section End -->




<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-10">
				<div class="sectionTheading">
					
						<h2>
							Edukujte se za terapeuta maderoterapije tela
						</h2>
					
					<img src="{{asset('img/sectionSeparator.png')}}" alt="">
					
					<h5>
						Čuli ste da je anticelulit masaža oklagijom najdelotvorniji i najprirodniji način za eliminaciju celulita, a takodje i najtraženija usluga u kozmetičkim salonima? Jako želite da se bavite tim veoma profitabilnim zanimanjem ali nemate od koga da naučite? Kontaktirajte nas što pre jer danas u našem edukativnom centru počinje prijava na obuku za terapeuta maderoterapije.
					</h5>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Counter Area Start -->
<section id="counter" class="counter">

<div class="container">
	<h2 style="color: white; text-align:center;">
	Tematske jedinice koje obrađujemo:
	<br><br>
	</h2>
	<div class="row">
		<h5 style="color: white;">
			<strong>2 ČASA </strong>- teorijska predavanja –Koža, Uzroci nastanka celulita i mesta pojavljivanja, Vrste celulita, Oprema prostorije za maderoterapiju, Sredstva za maderoterapiju, Dejstvo maderoterapije, Doziranje maderoterapije, Indikacije i kontraindikacije za primenu maderoterapije, Postupak nakon maderoterapije.
			<br>

			<strong>8 ČASOVA </strong>- praktična nastava –Tehnike mederoterapije u lečenju celulita
			<br>
			Nakon obuke polaznici polažu teorijski i praktični ispit i stiču sertifikat o stručnoj osposobljenosti za rad na poslovima terapeuta maderoterapije.
		</h5>
		
	</div>
</div>
</section>
<!-- Counter Area End -->

<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-10">
				<div class="sectionTheading">
						<h2>
							Trajanje edukacije:
						</h2>
					
					<h5>
						Ukupan fond časova edukacije je 10. Nastava se odvija tokom jednog dana sa početkom od 10h. U pauzi je obezbedjeno osveženje.
						<br><br>
					</h5>

					<img src="{{asset('img/sectionSeparator.png')}}" alt="">

					<h2>
						<br>
							Potrebna dokumentacija za upis:
						</h2>
					
					<h5>
						Prilikom upisa potrebno je da nam dostavite: fotokopiju prethodno stečene diplome redovnog obrazovanja i fotokopiju lične karte.
					</h5>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Pricing Plan Start -->
<section id="pricePlan" class="pricePlan">
	<div class="container">
			<div class="row justify-content-center">
					<div class="col-md-10 col-lg-8">
				<div class="sectionTheading">
					<h2>
					Cena edukacije 
					</h2>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-2">
			</div>
			<div class="col-lg-4">
				<div class="p_box">
					<div class="doller">
					<span>START</span>
					</div>
					<div class="title">
						<img class="img-fluid" src="{{asset('img/shape1.png')}}" alt="">
						<i class="pe-7s-diamond"></i>
					</div>
					<div class="doller">
						<span><del>29.000rsd</del></span>
					<span>24.000rsd</span>
					</div>
					<div class="list">
						<ul>
							<li>
								<strong>Edukacija</strong>
							</li>
							<li>
								<strong>Priručnik za učenje</strong>
							</li>
							<li>
								<strong>Sertifikat</strong>
							</li>
							<li>
								<strong><BR></strong>
							</li>
							<li>
								<strong><BR></strong>
							</li>
						</ul>
					</div>
					<a class="phurchaseBtn" href="/kontakt">
						Zakažite termin	
					</a>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="p_box">
					<div class="doller">
					<span>SMART</span>
					</div>
					<div class="title">
						<img class="img-fluid" src="{{asset('img/shape1.png')}}" alt="">
						<i class="pe-7s-magic-wand"></i>
					</div>
					<div class="doller">
						<span><del>45.000rsd</del></span>
					<span>33.000rsd</span>
					</div>
					<div class="list">
						<ul>
							<li>
								<strong>Edukacija</strong>
							</li>
							<li>
								<strong>Priručnik za učenje</strong>
							</li>
							<li>
								<strong>Sertifikat</strong>
							</li>
							<li>
								<strong>Profesionalni set od 6 oklagija</strong>
							</li>
							<li>
								<strong>500ml ulja za maderoterapiju</strong>
							</li>							


						</ul>
					</div>
					<a class="phurchaseBtn" href="/kontakt">
						Zakažite termin	
					</a>
				</div>
			</div>
			
		</div>
		<div class="row justify-content-center">	
			<h5>
				<br>
				*Na individualnu obuku je potrebno dovesti osobu koja će biti model za vežbu.<br>
				Cena za individualnu edukaciju je 30% veća.
			</h5>
		</div>

		


	</div>
</section> 
<!-- Pricing Plan End -->
<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-12">
				<div class="sectionTheading">
					<!--
					<img src="{{asset('/img/in.png')}}" alt="">
					-->
						<h4>
							Prijavite se odmah na našu edukaciju koja je apsolutno jedinstvena u Srbiji po kvalitetu rada sa polaznicima i nivou stečenog znanja na broj: <strong><a href="tel:062/455200">062/455200</a></strong> ili putem maila: <strong>info@essenceofbeauty.rs</strong>
						</h4>
				

				</div>
			</div>
		</div>
	</div>
</section>
@stop



