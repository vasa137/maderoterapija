@extends('layout')

@section('title')
Depilacija Edukacija Srbija Beograd - Depilacija Hladnim Voskom
@stop

@section('scriptsTop')
<script>
    	fbq('track', 'EDUKACIJA - Hijaluron pen');
</script>
@stop

@section('sekcije')
<!-- banner Area Start -->
<section id="spabreadcrumb" class="spabreadcrumb">
	<div class="bcoverlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="Content">
					<h2>
						Depilacija
					</h2>
					<div class="links">
						<ul>
							<li>
								<a href="/">Naslovna</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a href="/edukacija">Edukacija</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a class="active" href="/edukacija/depilacija">Depilacija</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Welcome Area End -->



<!-- Video Section Start -->
<section id="video" class="video">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="vImg">
					<img class="img-fluid" src="{{asset('img/edukacije-maderoterapija-tela-lica-bb-glow-srbija/depilacija.jpg')}}" alt="">
				</div>
			</div>
			<div class="col-lg-6">
				<h2>
					Broj polaznika u grupi je mali zbog kvaliteta rada!
				</h2>
				<h5>
					Depilacija hladnim voskom je jedna od najtraženijih usluga u kozmetičkim salonima. To je postupak kojim se temeljno uklanjaju dlačice sa regije lica ili tela po izboru klijenta. Vosak se tanko i ravnomerno nanosi na površinu kože, na koji se zatim potiskuje traka, a onda se brzim pokretima traka uklanja zajedno sa dlačicama.


				</h5>
				<!--
				<div class="videoContent">
					<div class="icon">
						<a class="venobox" data-vbtype="video" data-autoplay="true" href="http://vimeo.com/75976293">
							<i class="fas fa-play"></i>
						</a>
					</div>
					<div class="content d-flex">
						<div class="text align-self-center">
							<h5>Video</h5>
								<h5>Play</h5>
						</div>
					</div>
				</div>
				-->
			</div>
			<div class="col-lg-12">
				<h5>
					<br>
					
				</h5>
				
			</div>
		</div>
	</div>
</section>
<!-- Video Section End -->




<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-12">
				<div class="sectionTheading">
					
						<h2>
							SAVLADAJTE TEHNIKU HLADNE I TOPLE DEPILACIJE!
						</h2>
					
					<img src="{{asset('img/sectionSeparator.png')}}" alt="">
					
					<h5>
						Cilj naše edukacije je da vas naučimo kako da pravilno i detaljno sprovodite tretane depilacije lica i tela .Tokom edukacije upoznaćemo vas sa postupcima tople i hladne depilacije, sa građom i tipovima kako kože tako i  dlačica, kao I detaljnom dezinfekcijom i zaštitiom od iritacija i infekcije. <br>

					</h5>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Counter Area Start -->
<section id="counter" class="counter">

<div class="container">
	<h2 style="color: white; text-align:center;">
	Tematske jedinice koje obrađujemo:
	<br><br>
	</h2>

	<div class="row">
		<h5 style="color: white;">
			<strong>2 ČASA </strong>– teorijska predavanja: Uvod, histologija kože i građa dlake, Tipovi dlake, Dezinfekcija i sterilizacija, Osnovne mere zaštite od infekcije, Pribor za rad, Prijem klijenta
			<br>

			<strong>8 ČASOVA </strong>- praktična nastava: Postupak rada toplim i hladnim voskom – depilacija nogu,depilacija ruku, depilacija intimne zone prepona (brazilska depilacija),depilacija obrva, depilacija nausnica, depilacija lica, depilacija grudi,depilacija stomaka i depilacija leđa
		</h5>
		
	</div>
</div>
</section>
<!-- Counter Area End -->

<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-12">
				<div class="sectionTheading">
						<h2>
							TRAJANJE EDUKACIJE:

						</h2>
					
					<h5>
						Ukupan fond časova je 10. Nastava se odvija tokom jednog dana sa početkom od 10h. U pauzi je obezbedjeno osveženje. 
					</h5>

					<img src="{{asset('img/sectionSeparator.png')}}" alt="">

					<h2>
						<br>
							POTREBNA DOKUMENTACIJA ZA UPIS:
						</h2>
					
					<h5>
								
						Prilikom upisa potrebno je da nam dostavite: fotokopiju prethodno stečene diplome redovnog obrazovanja i fotokopiju lične karte

					</h5>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Pricing Plan Start -->
<section id="pricePlan" class="pricePlan">
	<div class="container">
			<div class="row justify-content-center">
					<div class="col-md-10 col-lg-8">
				<div class="sectionTheading">
					<h2>
					Cena edukacije 
					</h2>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4">
			</div>
			<div class="col-lg-4">
				<div class="p_box">
					<div class="doller">
					<span>SMART</span>
					</div>
					<div class="title">
						<img class="img-fluid" src="{{asset('img/shape1.png')}}" alt="">
						<i class="pe-7s-magic-wand"></i>
					</div>
					<div class="doller">
					<span>17.000 RSD</span>
					</div>
					<div class="list">
						<ul>
							<li>
								<strong>Edukacija</strong>
							</li>
							<li>
								<strong>Priručnik za učenje</strong>
							</li>
							<li>
								<strong>Sertifikat</strong>
							</li>
							</li>	

						</ul>
					</div>
					<a class="phurchaseBtn" href="/kontakt">
						Zakažite termin	
					</a>
				</div>
			</div>
			
		</div>
		<!--
		<div class="row justify-content-center">	
			<h5>
				<br>
				*Na individualnu obuku je potrebno dovesti osobu koja će biti model za vežbu.<br>
				Cena za individualnu edukaciju je 30% veća.
			</h5>
		</div>
		-->
		


	</div>
</section> 
<!-- Pricing Plan End -->
<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-12">
				<div class="sectionTheading">
					<!--
					<img src="{{asset('/img/in.png')}}" alt="">
					-->
						<h4>
							Prijavite se odmah na našu edukaciju koja je apsolutno jedinstvena u Srbiji po kvalitetu rada sa polaznicima i nivou stečenog znanja na broj: <strong><a href="tel:062/455200">062/455200</a></strong> ili putem maila: <strong>info@essenceofbeauty.rs</strong>
						</h4>
				

				</div>
			</div>
		</div>
	</div>
</section>
@stop



