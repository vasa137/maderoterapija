<ul class="cancel-off-pngcat-list">
    @foreach($kategorije as $kategorija)
        <li>
            <input onchange="filtriraj();" style="cursor:pointer;" id="checkbox-kategorija-{{$kategorija->id}}" type="checkbox" name="kategorije[{{$nad_kategorija->id}}][]" value="{{$kategorija->id}}"/>



            <label style="cursor:pointer;" for="checkbox-kategorija-{{$kategorija->id}}">{{$kategorija->naziv}}</label>

            @if(!empty( $kategorija->children))
                @include('include.podkategorija', ['kategorije' => $kategorija->children, 'nad_kategorija' => $kategorija])
            @endif
        </li>
        
    @endforeach
    <li>
    	@if($id > -1)
			<input checked onchange="filtriraj();" style="cursor:pointer;" id="checkbox-kategorija-{{$id}}" type="checkbox" name="kategorije[{{$id}}][]" value="{{$id}}"/>

        	<label style="cursor:pointer;" for="checkbox-kategorija-{{$id}}">Svi proizvodi</label>
		@endif
    </li>
</ul>