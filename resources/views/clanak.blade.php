@extends('layout')

@section('title')
{{$clanak->naslov}} - Novosti - 
@stop

@section('scriptsTop')
<script>
    	fbq('track', 'Blog');
</script>
@stop

@section('sekcije')
<section id="spabreadcrumb" class="spabreadcrumb extrapadding">
	<div class="bcoverlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="Content">
					<h2>
						{{$clanak->naslov}}
					</h2>
					<div class="links">
					<ul>
						<li>
							<a href="/">Naslovna</a>
						</li>
						<li>
							<span>/</span>
						</li>
						<li>
							<a href="/blog">Novosti</a>
						</li>
						<li>
							<span>/</span>
						</li>
						<li>
							<a class="active" href="/clanak/<?=str_replace('?', '',str_replace(' ', '-', $clanak->naslov))?>/{{$clanak->id}}">{{$clanak->naslov}}</a>
						</li>
					</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Welcome Area End -->


<!-- Main Blog Sectin Srart -->
<section id="blogDetails" class="blogDetails">
	<div class="container">
		<div class="row">
			<div class="col-lg-8">
				<div class="blog-details-left">
					<div class="details-sulf">
						<div class="details-img">
							<img src="/images/clanci/{{$clanak->id}}/{{$clanak->naslov}}.jpg" alt="" />
						</div>
						<div class="details-text">
							<h3>{{$clanak->naslov}}</h3>
							{!! $clanak->tekst !!}
							<ul>
								<li><img src="{{asset('img/in.png')}}" alt="" /> Essence Of Beauty</li>
								<li>|</li>
								<li><i class="far fa-calendar-alt"></i> {{$clanak->created_at->format('d-m-Y')}}</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-4 ">
				<div class="blog-details-right">
					<div class="relative">
						<h3>Istaknuti članci:</h3>
						@foreach($najnoviji as $cl)
						<a href="/clanak/<?=str_replace('?', '',str_replace(' ', '-', $cl->naslov))?>/{{$cl->id}}">
							<div class="relative-item">
								<img height="100" width="120" src="/images/clanci/{{$cl->id}}/{{$cl->naslov}}.jpg" alt="<?=str_replace('?', '',str_replace(' ', '-', $cl->naslov))?>" />
								<p>
									<a href="/clanak/<?=str_replace('?', '',str_replace(' ', '-', $cl->naslov))?>/{{$cl->id}}">
										{{$cl->naslov}}
									</a>
								</p>
								<span><i class="far fa-calendar-alt"></i> {{$cl->created_at->format('d-m-Y')}}</span>
								
							</div>
						</a>
						@endforeach
					</div>
					
				</div>
			</div>
		</div>
	</div>
</section>

@stop