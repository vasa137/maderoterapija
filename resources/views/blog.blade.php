@extends('layout')

@section('title')
Novosti - 
@stop

@section('scriptsTop')
<script>
    	fbq('track', 'Blog');
</script>
@stop

@section('sekcije')
<section id="spabreadcrumb" class="spabreadcrumb extrapadding">
	<div class="bcoverlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="Content">
					<h2>
						Novosti
					</h2>
					<div class="links">
					<ul>
						<li>
							<a href="/">Naslovna</a>
						</li>
						<li>
							<span>/</span>
						</li>
						<li>
							<a class="active" href="/blog">Novosti</a>
						</li>
					</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Welcome Area End -->

<!-- Main Blog Sectin Srart -->
<section id="mainBlog" class="mainBlog">
	<div class="container">
		<div class="row">
			@foreach($blog as $clanak)
			<div class="col-md-6 col-lg-4">
				<div class="blog-box">
					<div class="topImg">
						<a class="read-more-link" href="/clanak/<?=str_replace('?', '',str_replace(' ', '-', $clanak->naslov))?>/{{$clanak->id}}">
							<img height="400" class="img-fluid" src="/images/clanci/{{$clanak->id}}/{{$clanak->naslov}}.jpg" alt="">
						</a>
					</div>
					<div class="blog-item-description">
						<h5 class="post-date">{{$clanak->created_at->format('d-m-Y')}}</h5>
						<h3 class="title"><a href="/clanak/<?=str_replace('?', '',str_replace(' ', '-', $clanak->naslov))?>/{{$clanak->id}}" title="New Yoga Trainer is here">{{$clanak->naslov}}</a></h3>
						<div>
							<p>{{$clanak->uvod}}</p>
							<div class="read-more-link-wrapper">
								<a class="read-more-link" href="/clanak/<?=str_replace('?', '',str_replace(' ', '-', $clanak->naslov))?>/{{$clanak->id}}">Detaljnije<i class="fas fa-angle-right"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
			@endforeach		
		</div>
	</div>
</section>
<!-- Main Blog Sectin End -->
@stop