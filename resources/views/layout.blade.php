<!DOCTYPE html>
<html lang="zxx">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0">
	<meta name="keywords" content="">
	<meta name="description" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
	<title>@yield ('title') ESSENCE OF BEAUTY Maderoterapija, BB Glow, Obuke, Kursevi, Edukacija, Salon lepote</title>
	<!--Favicon-->
	<link rel="shortcut icon"  href="{{asset('img/favicon.ico')}}" />
	<!--Bootstrap Stylesheet-->
	<link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.css')}}">
	<!--Slick Slider-->
	<link rel="stylesheet" type="text/css" href="{{asset('css/slick.css')}}">
	<!--Font Awesome Stylesheet-->
	<link rel="stylesheet" type="text/css" href="{{asset('css/fontawesome.all.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('css/pe-icon-7-stroke.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('css/helper.css')}}">
	<!--Animate Stylesheet-->
	<link rel="stylesheet" type="text/css" href="{{asset('css/animate.css')}}">
	<!--Venobox Stylesheet-->
	<link rel="stylesheet" type="text/css" href="{{asset('css/venobox.css')}}">
	<!--Owl carosul-->
	<link rel="stylesheet" type="text/css" href="{{asset('css/owl.carousel.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('css/owl.theme.default.css')}}">
	<!--Main Stylesheet-->
	<link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
	<!--Responsive Stylesheet-->
	<link rel="stylesheet" type="text/css" href="{{asset('css/responsive.css')}}">

	<link rel="stylesheet" type="text/css" href="{{asset('css/klijentHoverKorpa.css')}}"/>

	<link  rel="stylesheet"  href="{{asset('assets/js/plugins/slick/slick-theme.css')}}"/>

	@yield('scriptsTop')
	<!-- Facebook Pixel Code -->
	<script>
	  !function(f,b,e,v,n,t,s)
	  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	  n.queue=[];t=b.createElement(e);t.async=!0;
	  t.src=v;s=b.getElementsByTagName(e)[0];
	  s.parentNode.insertBefore(t,s)}(window, document,'script',
	  'https://connect.facebook.net/en_US/fbevents.js');
	  fbq('init', '423174104938911');
	  fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	  src="https://www.facebook.com/tr?id=423174104938911&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-117964354-6"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-117964354-6');
	</script>

</head>

<body>
	<!--Start Preloader-->
	<div class="site-preloader">
		<div class="spinner">
			<div class="double-bounce1"></div>
			<div class="double-bounce2"></div>
		</div>
	</div>
	<!--End Preloader-->

	<!-- Main Menu Area Start -->
	<header id="mainHeader" class="header">
		<!-- Start Navigation -->
		<nav class="navbar navbar-expand-lg navbar-light p-0">
			<div class="container">
				<a class="navbar-brand" href="/">
					<img style="height: 70px;" src="{{asset('img/logo.png')}}" alt="">

				</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
				 aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>

				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav ml-auto">

						<li class="nav-item">
							<a class="nav-link" href="/prodavnica">Prodavnica</a>
						</li>
						<!--
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" aria-expanded="true" href="/prodavnica"  id="shope1" >Prodavnica</a>
							
							<div class="dropdown-menu" aria-labelledby="shope1">
								<a class="dropdown-item" href="/kategorija/17">Dezinfekcija i zaštitna oprema</a>

								<a class="dropdown-item" href="/kategorija/21">Kozmetički aparati  </a>

								<a class="dropdown-item" href="/kategorija/1">Maderoterapija</a>

								<a class="dropdown-item" href="/kategorija/5">BB Glow </a>

								<a class="dropdown-item" href="/kategorija/22">Sheet maske </a>
							</div>           
 

						</li>
					-->
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="/edukacija" id="shope"  
							 aria-expanded="true">
								Edukacija
							</a>
							<div class="dropdown-menu" aria-labelledby="shope">
								<a class="dropdown-item" href="/edukacija/bb-glow">BB Glow</a>
<!--
								<a class="dropdown-item" href="/edukacija/plazma-pen">Plazma pen</a>-->
								<a class="dropdown-item" href="/edukacija/hijaluron-pen">Hijaluron pen</a>
								<a class="dropdown-item" href="/edukacija/aparaturno-ciscenje-lica-dijamantskom-mikrodermoabrazijom">Aparaturno čišćenje lica dijamantskom mikrodermoabrazijom</a>
								<a class="dropdown-item" href="/edukacija/aparaturno-ciscenje-lica-ultrazvucnom-spatulom">Aparaturno čišćenje lica ultrazvučnom špatulom</a>
								<a class="dropdown-item" href="/edukacija/brazilska-maderoterapija">Brazilska maderoterapija</a>
								<a class="dropdown-item" href="/edukacija/maderoterapija">Maderoterapija</a>
								<a class="dropdown-item" href="/edukacija/maderoterapija-lica">Maderoterapija lica</a>
								<a class="dropdown-item" href="/edukacija/relaks-masaza">Relaks masaža</a>
								<a class="dropdown-item" href="/edukacija/svedska-masaza">Švedska masaža</a>
								<a class="dropdown-item" href="/edukacija/refleksoloska-masaza">Refleksološka masaža</a>
								<a class="dropdown-item" href="/edukacija/masaza-svecom">Masaža svećom</a>
								<a class="dropdown-item" href="/edukacija/anticelulit-masaza">Anticelulit masaža</a>
								<a class="dropdown-item" href="/edukacija/depilacija">Depilacija</a>
								
								

								
								<a class="dropdown-item" href="/vauceri">Vaučeri</a>
							</div>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="/blog">Novosti</a>
						</li>
						
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="/salon" id="shope"  
							 aria-expanded="true">
								Salon lepote
							</a>
							<div class="dropdown-menu" aria-labelledby="shope">
								<a class="dropdown-item" href="/salon/bb-glow">BB Glow</a>
								<a class="dropdown-item" href="/salon/epilacija">Epilacija</a>
								<a class="dropdown-item" href="/salon/fraktora-lifting">Fraktora lifting</a>
								
								<a class="dropdown-item" href="/salon/tretmani-lica">Tretmani lica</a>
								<a class="dropdown-item" href="/salon/kavitacija">Kavitacija</a>
								<a class="dropdown-item" href="/salon/tretmani-tela">Tretmani tela</a>
								<a class="dropdown-item" href="/salon/kozmeticke-usluge">Kozmetičke usluge</a>
								<a class="dropdown-item" href="/salon/cenovnik">Cenovnik</a>
								<a class="dropdown-item" href="/salon/promocije">Promocije</a>
								<a class="dropdown-item" href="/salon/karijera">Karijera</a>
								
								<a class="dropdown-item" href="/vauceri">Vaučeri</a>
							</div>
						</li>
						<!--
						<li class="nav-item">
							<a class="nav-link" href="/karijera">Karijera</a>
						</li>
						-->
						<li class="nav-item">
							<a class="nav-link" href="/kontakt">Kontakt</a>
						</li>
					</ul>
					<div class="call_btn" title="Korpa">
						<a id="cart-button" style="cursor:pointer;">
							<i class="fa fa-shopping-cart"></i>
						</a>
						<div id="korpaInclude" class="container">
							@include('include.korpaNavbar')
						</div>

					</div>
					<div class="nalog" title="Korisnički nalog">
						<a href="/nalog">
							<i class="fa fa-user"></i>
						</a>
					</div>
					

					<!--
					<div class="" title="">
						<a href="http://en.essenceofbeauty.rs/">
							<i class="">
							<img  style="height: 20px;" src="img/eng.png">
							</i>
						</a>
					</div>
				-->
					@if(Auth::check())
					<div class="logout" title="Odjava">
						<a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
							<i class="fa fa-sign-out"></i>
						</a>

						<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
							@csrf
						</form>
					</div>
					@endif
					<div class="" title="Korisnički nalog">
						<a class="nav-link" href="http://en.essenceofbeauty.rs"><img style="margin-left: 10px;" src="{{asset('/img/eng.png')}}"></a>
					</div>
				</div>
			</div>
		</nav>
	</header>
	<!-- Main Menu Area End -->


	@yield('sekcije')

	<!--  Footer Section Start -->
	<footer class="footer">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-lg-4">
					<div class="widget1">
						<div class="logo">
							<a href="/">
								<img style="height: 120px;" src="{{asset('img/logo-footer.png')}}" height="10" class="img-fluid" alt="">
							</a>
						</div>
						<p>
							 Već punih 5 godina Essence of Beauty Academy uspešno vrši edukaciju budućih profesionalaca iz oblasti nege lica i tela, kao i ostalih kozmetičkih usluga. Nalazimo se u Bulevaru Zorana Đinđića 6 u Beogradu. 
						</p>
						
					</div>
				</div>
				<div class="col-md-6 col-lg-4">
					<div class="widget2">
						<h5>
							Izdvajamo
						</h5>
						<div class="media">
							<a href="/edukacija/bb-glow">
							<img style="height: 100px;" class="img-fluid" src="{{asset('img/edukacija/bb-glow.png')}}" alt="">
							</a>
							<div class="media-body d-flex align-self-center">
								<div class="content">
									<a href="/edukacija/bb-glow">
										<p>
											BB Glow edukacija
										</p>
									</a>
									<span>
										Obuka, Sertifikat, Popusti
									</span>
								</div>
							</div>
						</div>
						<div class="media">
							<a href="/edukacija/maderoterapija">
							<img style="height: 100px;" class="img-fluid" src="{{asset('img/edukacija/maderoterapija.png')}}" alt="">
							</a>
							<div class="media-body d-flex align-self-center">
								<div class="content">
									<a href="/edukacija/maderoterapija">
										<p>
											Maderoterapija edukacija
										</p>
									</a>
									<span>
										Obuka, Sertifikat, Popusti
									</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-lg-2">
					<div class="widget3">
						<h5>
							INFORMACIJE
						</h5>
						<ul>
							<li>
								<a href="/kontakt">
									Kontakt
								</a>
							</li>
							<li>
								<a href="/kako-poruciti">
									Kako poručiti?
								</a>
							</li>
							<li>
								<a href="/nalog">
									Moje porudžbine
								</a>
							</li>
							<li>
								<a href="uslovi-kupovine">
									Uslovi kupovine
								</a>
							</li>
							<li>
								<a href="/registracija">
									Registracija
								</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="col-sm-6 col-lg-2">
					<div class="widget4">
						<h5>
							EDUKACIJE
						</h5>
						<ul>
							<li>
								<a href="/edukacija/bb-glow">BB Glow</a>
							</li>
							<li>
								<a href="/edukacija/maderoterapija">Maderoterapija</a>
							</li>
							<li>
								<a href="/edukacija/maderoterapija-lica">Maderoterapija lica</a>
							</li>
							<li>
								<a  href="/edukacija/relaks-masaza">Relaks masaža</a>
							</li>
							<li>
								<a href="/edukacija/anticelulit-masaza">Anticelulit masaža</a>
							</li>
						</ul>
					</div>
					<div class="socialLinks">
							<ul>
								<li>
									<a target="blanc" rel="nofollow" href="https://www.facebook.com/Essence-of-Beauty-530591970442539/">
										<i class="fab fa-facebook-f"></i>
									</a>
								</li>
								<li>
									<a target="blanc" rel="nofollow" href="https://www.instagram.com/essenceofbeauty.rs/">
										<i class="fab fa-instagram"></i>
									</a>
								</li>
							</ul>
						</div>
				</div>
			</div>
		</div>
		<div class="copyRightArea">
			<div class="container">
				<div class="row">
					<div class="col-12 text-center">
						<p>&copy; Copyright 2020. Sva prava zadržana. Essence Of Beauty</p>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<!--  Footer Section End -->

	<!--Start ClickToTop-->
	<div class="totop">
		<a href="#top">
			<p>
				<i class="fas fa-angle-up"></i>
			</p>
		</a>
	</div>
	<!--End ClickToTop-->

	<!--jQuery JS-->
	<script src="{{asset('js/jquery.min.js')}}"></script>
	<!--Bootstrap JS-->
	<script src="{{asset('js/bootstrap.min.js')}}"></script>
	<script src="{{asset('js/popper.js')}}"></script>
	<!-- Way Point js -->
	<script src="{{asset('js/waypoints.min.js')}}"></script>
	<script src="{{asset('js/counter.js')}}"></script>
	<!-- Contact js -->
	<script src="{{asset('js/contact.js')}}"></script>
	<!-- Owl carosul js -->
	<script src="{{asset('js/owl.carousel.min.js')}}"></script>
	<!-- Venobox JS-->
	<script src="{{asset('js/venobox.min.js')}}"></script>
	<!--Main-->
	<script src="{{asset('js/main.js')}}"></script>
	<script src="{{asset('js/slick.min.js')}}"></script>

	<script src="{{asset('js/klijentHoverKorpa.js')}}"></script>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>

    <script type="text/javascript">
                  $( '#dodajUkorpu' ).click(function() {
					  fbq('track', 'Kliknuli DODAJ U KORPU');
				  });
                  </script>
	@yield('scriptsBottom')

</body>

</html>