@extends('admin.adminLayout')

@section('title')
Korisnik - {{$korisnik->ime_prezime}}
@stop

@section('breadcrumbs')
<a class="breadcrumb-item" href="/admin">Admin</a>
<a class="breadcrumb-item" href="/admin/korisnici">Korisnici</a>
<span class="breadcrumb-item active">{{$korisnik->ime_prezime}}</span>
@stop




@section('main')
<!-- Overview -->
<div class="row gutters-tiny">
    <!-- Orders -->
    <div class="col-md-6 col-xl-3">
        <a class="block block-rounded block-link-shadow">
            <div class="block-content block-content-full block-sticky-options">
                <div class="block-options">
                    <div class="block-options-item">
                        <i class="fa fa-money fa-2x text-body-bg-dark"></i>
                    </div>
                </div>
                <div class="py-20 text-center">
                    <div class="font-size-h2 font-w700 mb-0" data-toggle="countTo" data-to="{{$korisnik->promet}}">0</div>
                    <div class="font-size-sm font-w600 text-uppercase text-muted">Promet za korisnika</div>
                </div>
            </div>
        </a>
    </div>
    <!-- END Orders -->

    <!-- In Cart -->
    <div class="col-md-6 col-xl-3">

        <a class="block block-rounded block-link-shadow">
            <div class="block-content block-content-full block-sticky-options">
                <div class="block-options">
                    <div class="block-options-item">
                        <i class="fa fa-shopping-cart fa-2x text-body-bg-dark"></i>
                    </div>
                </div>
                <div class="py-20 text-center">
                    <div class="font-size-h2 font-w700 mb-0" data-toggle="countTo" data-to="{{$korisnik->broj_porudzbina}}">0</div>
                    <div class="font-size-sm font-w600 text-uppercase text-muted">Porudžbina</div>
                </div>
            </div>
        </a>
    </div>
    <!-- END In Cart -->

    <!-- Edit Customer -->
    <div class="col-md-6 col-xl-3">

        <a class="block block-rounded block-link-shadow" href="javascript:$('#forma-korisnik-submit-button').click();">
            <div class="block-content block-content-full block-sticky-options">
                <div class="block-options">
                    <div class="block-options-item">
                        <i class="fa fa-user fa-2x text-success-light"></i>
                    </div>
                </div>
                <div class="py-20 text-center">
                    <div class="font-size-h2 font-w700 mb-0 text-success">
                        <i class="fa fa-check"></i>
                    </div>
                    <div class="font-size-sm font-w600 text-uppercase text-muted">Sačuvaj</div>
                </div>
            </div>
        </a>
    </div>
    <!-- END Edit Customer -->

    @if(!$korisnik->blokiran)
    <!-- Remove Customer -->
    <div class="col-md-6 col-xl-3">
        <form id="forma-blokiraj" action="/admin/blokirajKorisnika/{{$korisnik->id}}" method="POST">
            {{csrf_field()}}
        <a class="block block-rounded block-link-shadow" href="javascript:document.getElementById('forma-blokiraj').submit()">
            <div class="block-content block-content-full block-sticky-options">
                <div class="block-options">
                    <div class="block-options-item">
                        <i class="fa fa-user fa-2x text-danger-light"></i>
                    </div>
                </div>
                <div class="py-20 text-center">
                    <div class="font-size-h2 font-w700 mb-0 text-danger">
                        <i class="fa fa-ban"></i>
                    </div>

                    <div class="font-size-sm font-w600 text-uppercase text-muted">Blokiraj korisnika</div>
                </div>
            </div>
        </a>
        </form>
    </div>
    @else
    <div class="col-md-6 col-xl-3">
        <form id="forma-odblokiraj" action="/admin/odblokirajKorisnika/{{$korisnik->id}}" method="POST">
            {{csrf_field()}}
        <a class="block block-rounded block-link-shadow" href="javascript:document.getElementById('forma-odblokiraj').submit()">
            <div class="block-content block-content-full block-sticky-options">
                <div class="block-options">
                    <div class="block-options-item">
                        <i class="fa fa-user fa-2x text-primary-light"></i>
                    </div>
                </div>
                <div class="py-20 text-center">
                    <div class="font-size-h2 font-w700 mb-0 text-primary">
                        <i class="fa fa-check-circle"></i>
                    </div>

                    <div class="font-size-sm font-w600 text-uppercase text-primary">Odblokiraj korisnika</div>
                </div>
            </div>
        </a>
        </form>
    </div>
    @endif
    <!-- END Remove Customer -->
</div>
<!-- END Overview -->
<form method="POST" action="/admin/sacuvajKorisnika/{{$korisnik->id}}">
{{csrf_field()}}
<!-- Addresses -->
<h2 class="content-heading">Informacije</h2>
<div class="row row-deck gutters-tiny">
    <!-- Billing Address -->
    <div class="col-md-5">
        <div class="block block-rounded">
            <div class="block-header block-header-default">
                <h3 class="block-title">Informacije o korisniku</h3>
            </div>
            <div class="block-content">
                <div class="font-size-lg text-black mb-5"><i class="fa fa-user mr-5"></i> {{$korisnik->ime_prezime}}</div>
                <address>
                    @if($korisnik->adresa != '')
                    <i class="fa fa-home mr-5"></i> {{$korisnik->adresa}},&nbsp;{{$korisnik->zip}}&nbsp;{{$korisnik->grad}}<br>
                    <br>
                    @endif
                    @if($korisnik->telefon != '')
                    <i class="fa fa-phone mr-5"></i> {{$korisnik->telefon}}<br>
                    @endif
                    @if($korisnik->telefon2 != '')
                    <i class="fa fa-phone mr-5"></i> {{$korisnik->telefon2}}<br>
                    @endif
                    <i class="fa fa-envelope-o mr-5"></i> <a href="mailto:{{$korisnik->email}}">{{$korisnik->email}}</a>
                </address>
            </div>
            
            <label class="col-12">Vrsta korisnika</label>
            <div class="col-12">
                <div class="custom-control custom-radio custom-control-inline mb-5">
                    <input class="custom-control-input" type="radio" name="vrstaKorisnika" id="vrsta-korisnika-obican" value="" @if($korisnik->id_vrsta_korisnika == null) checked @endif>
                    <label class="custom-control-label"  for="vrsta-korisnika-obican">Običan</label>
                </div>
                @foreach($vrsteKorisnika as $vrstaKorisnika)
                    <div class="custom-control custom-radio custom-control-inline mb-5">
                        <input class="custom-control-input" type="radio" name="vrstaKorisnika" id="vrsta-korisnika-{{$vrstaKorisnika->id}}" value="{{$vrstaKorisnika->id}}" @if($korisnik->id_vrsta_korisnika != null and $korisnik->vrsta->id == $vrstaKorisnika->id) checked @endif>
                        <label class="custom-control-label" for="vrsta-korisnika-{{$vrstaKorisnika->id}}">{{$vrstaKorisnika->naziv}}</label>
                    </div>
                @endforeach
                <br/>
            </div>
            
        </div>
    </div>
    <!-- END Billing Address -->

    <!-- Shipping Address -->
    <div class="col-md-7">
        <div class="block block-rounded">
            <div class="block-header block-header-default">
                <h3 class="block-title">Porudžbine</h3>
            </div>
            <div class="block-content">
		        <!-- Orders Table -->
                @if(count($korisnik->porudzbine) > 0)
		        <table class="table table-borderless table-sm table-striped">
		            <thead>
		                <tr>
		                    <th style="width: 15%;">Broj</th>
		                    <th style="width: 20%;">Status</th>
		                    <th class="text-center d-none d-sm-table-cell" style="width: 20%;">Datum</th>
		                    <th class="text-center d-none d-sm-table-cell" style="width: 25%;">Način plaćanja</th>
		                    <th class="text-right" style="width: 20%;">Cena</th>
		                </tr>
		            </thead>
		            <tbody>
                        @foreach($korisnik->porudzbine as $kupacPorudzbina)
                            <tr>
                                <td>
                                    <a class="font-w600" href="/admin/porudzbina/{{$kupacPorudzbina->id}}">{{$kupacPorudzbina->id}}</a>
                                </td>
                                <td>
                                    @if($kupacPorudzbina->status == 'nova')
                                        <span class="badge badge-warning">Nova</span>
                                    @elseif($kupacPorudzbina->status == 'poslata')
                                        <span class="badge badge-primary">Poslata</span>
                                    @elseif($kupacPorudzbina->status == 'stornirana')
                                        <span class="badge badge-danger">Stornirana</span>
                                    @elseif($kupacPorudzbina->status == 'kompletirana')
                                        <span class="badge badge-success">Kompletirana</span>
                                    @endif
                                </td>
                                <td class="d-none d-sm-table-cell">
                                    {{date_format($kupacPorudzbina->created_at, "d.m.Y.")}}
                                </td>
                                <td class="d-none d-sm-table-cell">
                                    @if($kupacPorudzbina->nacin_placanja == 'pouzecem')
                                        Pouzećem
                                    @elseif($kupacPorudzbina->nacin_placanja == 'karticom')
                                        Karticom
                                    @elseif($kupacPorudzbina->nacin_placanja == 'preko_racuna')
                                        Preko računa
                                    @endif
                                </td>
                                <td class="text-right">
                                    <span class="text-black">{{number_format($kupacPorudzbina->iznos_popust, 2, ',', '.')}}&nbsp;rsd</span>
                                </td>
                            </tr>
                        @endforeach

		            </tbody>
		        </table>
                @else
                <p>Nema porudžbina.</p>
                @endif
		        <!-- END Orders Table -->
		    </div>

        </div>
    </div>
    <!-- END Shipping Address -->
</div>
<!-- END Addresses -->

<!-- Private Notes -->
<h2 class="content-heading">Napomene o korisniku</h2>
<div class="block block-rounded">
    <div class="block-content">
        <div class="alert alert-info alert-dismissable" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <p class="mb-0"><i class="fa fa-info-circle mr-5"></i> Ove napomene su vidljive isključivo u admin panelu. Korisnik NE može da ih vidi u svom nalogu.</p>
        </div>
        
        <div class="form-group row mb-10">
            <div class="col-12">
                <textarea class="form-control" name="admin_napomena" placeholder="Npr. Korektan kupac. ili 3 puta tražio reklamaciju...." rows="4">{{$korisnik->admin_napomena}}</textarea>
            </div>
        </div>
        
    </div>
</div>
    <input type="submit" style="display:none;" id="forma-korisnik-submit-button"/>
</form>
<!-- END Private Notes -->
@stop