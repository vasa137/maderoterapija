@extends('admin.adminLayout')

@section('title')
Fakture
@stop

@section('breadcrumbs')
<a class="breadcrumb-item" href="/admin">Admin</a>
<span class="breadcrumb-item active">Fakture</span>
@stop

@section('heder-h1')
Fakture
@stop


@section('scriptsBottom')
    <!-- Page JS Code -->
    <script src="{{asset('/js/tabelaFakture.js')}}"></script>
@endsection


@section('main')
<!-- Dynamic Table Full Pagination -->
<div class="block">
    <div class="block-header block-header-default">
        <h3 class="block-title">Fakture</h3>
    </div>
    <div class="block-content block-content-full">
        <!-- DataTables init on table by adding .js-dataTable-full-pagination class, functionality initialized in js/pages/be_tables_datatables.js -->
        <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
            <thead>
                <tr>
                    <th class="text-center">Broj</th>
                    <th class="text-center">Datum</th>
                    <th class="d-none d-sm-table-cell">Ime i Prezime</th>
                    <th class="d-none d-sm-table-cell" style="width: 15%;">Iznos</th>
                    <th class="text-center" style="width: 15%;">Akcija</th>
                </tr>
            </thead>
            <tbody>
                @foreach($porudzbine as $porudzbina)
                <tr>
                    <td class="text-center">{{$porudzbina->id}}</td>
                    <td class="text-center">{{date_format($porudzbina->created_at, "Y-m-d H:i")}}</td>
                    <td class="font-w600">{{$porudzbina->kupac}}</td>
                    <td class="font-w600">{{number_format($porudzbina->iznos_popust, 2, ',', '.')}}</td>
                    <td class="text-center">
                        <a href="/admin/faktura/{{$porudzbina->id}}" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Pregled / štampa fakture">
                            <i class="fa fa-file"></i>
                        </a>
                        <a href="/admin/porudzbina/{{$porudzbina->id}}" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Pregled porudžbine" >
                            <i class="si si-basket-loaded"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<!-- END Dynamic Table Full Pagination -->
@stop