@extends('layout')

@section('title')
Prodavnica - Ampule Za BB Glow, Oklagije Za Maderoterapiju, Iglice za BB Glow,  - 
@stop

@section('scriptsTop')
	<link rel="stylesheet" href="{{asset('css/klijentProizvodi.css')}}"/>
	<link rel="stylesheet" href="{{asset('css/popupDialog.css')}}"/>
	<script>
	fbq('track', 'PRODAVNICA');
	</script>
@endsection

@section('scriptsBottom')
	<script src="{{asset('js/popupDialog.js')}}"></script>
	<script src="{{asset('js/klijentKorpa.js')}}"></script>
	<script src="{{asset('js/prodavnica.js')}}"></script>
@endsection

@section('sekcije')
@include('include.popupDialog', ['poruka' => 'Uspešno ste dodali proizvod u korpu. <br/> Da li želite da pregledate korpu?', 'linkUspesno' => '/korpa'])
<section id="prodavnicaheder" class="prodavnicaheder extrapadding">
	<div class="bcoverlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="Content">
					<h2>
						Prodavnica
					</h2>
					<div class="links">
						<ul>
							<li>
								<a href="/">Naslovna</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a class="active" href="/prodavnica">Prodavnica</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Welcome Area End -->

<!-- Shope Catagori Sectin Srart -->
<div class="category-content-area search-page">
	<div class="container">
		<div class="row">

			<div class="col-lg-3">
				<form id="filterForma">
					@foreach($stabloKategorija as $kategorija)
					@if($kategorija->id == $id || $id==-1)
					<div class="category-sidebar">
						<div class="category-filter-widget">
							<h4 class="title">{{$kategorija->naziv}}</h4>
							@if(!empty( $kategorija->children))
								@include('include.podkategorija', ['kategorije' => $kategorija->children, 'nad_kategorija' => $kategorija])
							@endif
						</div>
					</div>
					@endif
					@endforeach

					

					<div class="category-sidebar">
						<div class="category-filter-widget">
							<h4 class="title">Brend</h4>
							<ul class="cancel-off-pngcat-list">
							@foreach($brendovi as $brend)
								<li>
									<input onchange="filtriraj();" style="cursor:pointer;" id="checkbox-brend-{{$brend->id}}" type="checkbox" name="brendovi[]" value="{{$brend->id}}"/>
									<label style="cursor:pointer;" for="checkbox-brend-{{$brend->id}}">{{$brend->naziv}}</label>
								</li>
							@endforeach
							</ul>
						</div>
					</div>
					<!--
					<div class="banner-img">
						<a href="#">
							<img class="img-fluid" src="img/shope/catagoripage/discunt.jpg" alt="banner images">
						</a>
					</div>
					-->
				</form>
			</div>

			<div class="col-lg-9">
				<div class="row">
					<div class="col-12">
						<div class="topElement d-flex justify-content-between">
							<div class="left">
								<ul>
									<li>

										<form id="sortirajForma">
											<select onchange="filtriraj()" name="sort">
												<option hidden value="null">Sortiraj po</option>
												<option value="cena-asc">Ceni rastuće</option>
												<option value="cena-desc">Ceni opadajuće</option>
												<option value="naziv-asc">Naziv rastuće</option>
												<option value="naziv-desc">Naziv opadajuće</option>
											</select>
										</form>
									</li>
								</ul>
							</div>
							
						</div>
					</div>
				</div>
				<div class="bottom-content">
					<div class="row" id="proizvodiInclude">
						@include('include.listaProizvoda')
					</div>
					<div class="ajax-load" ></div>
				</div><!-- //.top content -->
				
			</div>
			
		</div>
	</div>
</div>
<!-- Shope catagori Sectin End -->
@stop